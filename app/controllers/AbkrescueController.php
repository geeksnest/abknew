<?php
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Tag as Tag,
    Phalcon\Validation\Validator\Regex as Regex,
    Phalcon\Validation\Validator\Email;
    use Phalcon\Mvc\Controller;
class AbkrescueController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->validateLoginVolunteer();
    }

    private function _getdb($phql){
      $db1 = \Phalcon\DI::getDefault()->get('db');
      $stmt = $db1->prepare($phql);
      $stmt->execute();
      $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      return $searchresult;
    }
    public function indexAction()
    {
      $find = Abkresdetails::findFirst("id = 1")->toArray();
      $this->view->hello = $find;

      $this->view->socialmedia = Socialmedia::find(array("order by"=>"sortable"))->toArray();

      $this->view->events = Events::find(array("limit"=>"3", "order" => "startdate desc"))->toArray();

      $this->view->aboutus = Aboutus::findFirstById(1)->toArray();

      $this->view->featured = $this->_getdb('SELECT * FROM activities where featured = "1" order by sort asc');
      $this->view->activities = $this->_getdb('SELECT * FROM activities where featured is null and status = 1 order by date_created DESC LIMIT 4');

      //volunteer
      $form = new CreateuserForm();
      $request=new Phalcon\Http\Request();
      $this->view->passError = "";
        if($this->request->isPost() && $this->request->getPost('join_team')){
            $team = new Rescueteam();
            $team->assign(
              array(
                "fname"=>$request->getPost("fname"),
                "lname"=>$request->getPost("lname"),
                "email" => $request->getPost("email"),
                "address" => $request->getPost("address"),
                "city"=>$request->getPost("city"),
                "country"=>$request->getPost("country"),
                "state"=>$request->getPost("state"),
                "zip"=>$request->getPost("zip"),
                "contact"=>$request->getPost("contact"),
                "date_created"=>date('Y-m-d H:i:s'),
                "date_updated"=>date('Y-m-d H:i:s')
                ));
            $validation = new Phalcon\Validation();
            $validation->add('email', new Regex(array(
              'message'    => 'Not a valid email format',
              'pattern'    => '/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i',
              'allowEmpty' => false
              )));
            if(count($validation->validate($_POST))){
              foreach ($validation->getMessages()->filter('email') as $message) {
                $this->view->passError .= "<div class='label label-danger'>".$message."</div> ";
              }
            }else{
              $this->view->passError = "";
               if($team->save())
                {
                  $this->view->successmsg = "<div class='col-sm-6 col-sm-offset-3 alert alert-success'>
                  <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                  <b> Success! </b> Thank you for Joining the team </div>";
                  //$this->flash->success("Thank you for Joining the team");
                  Tag::resetInput();
                }else{
                    foreach ($team->getMessages() as $message) {
                      if ($message->getField() == 'fname') {
                        $m = "<br> <span style='color:#a94442;'> firstname is required </span>";
                      }elseif ($message->getField() == 'lname') {
                        $m = "<br> <span style='color:#a94442;'> lastname is required </span>";
                      }else{
                        $m = "<br> <span style='color:#a94442;'>".$message->getMessage()."</span>";
                      }
                      $messages[$message->getField()] = $m;
                    }
                    // $this->view->msg = $messages;
                    // var_dump($messages);
                    // die();
                  // $this->flash->error($team->getMessages());
                }
            }

        }
        $this->view->country = $this->_getcountries();
        $this->view->form = $form;
    }

    private function _getcountries(){
     $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

     return $countries;
   }
}
