<?php
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email;
class AboutController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->validateLoginVolunteer();
    }

    public function indexAction()
    {
      $about=Tblother::findfirst("title='About ABK'");
      $this->view->about=$about;

        $contact= Tblcontact::find();
       $this->view->contacts=$contact;
        
    }
}