<?php

use Phalcon\Tag as Tag,
Phalcon\Mvc\View,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\StringLength,
Phalcon\Validation\Validator\Confirmation,
Phalcon\Mvc\Model\Criteria;

class AdminController extends ControllerBase
{
	public function initialize()
	{
		parent::initialize();
		$role = $this->session->get('roles');
		$this->view->userpages = $role['roles'];
		$this->view->programMenu = Tblprograms::find();
		//Check if the variable is defined
		if ($this->session->has("auth")) {
			$abk_user = $this->session->get('auth');
			//Retrieve its value
			$this->view->abk_user = $abk_user['abk_fullname'];
		}
	}
	public function indexAction()
	{
		\Phalcon\Tag::prependTitle('Dashboard | ');
		$this->view->menu = $this->_menuActive('dashboard');
		//echo "<h1> This is an example</h1>";
	}
	public function loginAction()
	{
		$auth = $this->session->get('auth');
		if ($auth){
			$this->response->redirect('admin');
		}

		$this->view->error = null;
		if ($this->request->isPost()) {
			$username = $this->request->getPost('username');
			$password = $this->request->getPost('password');
			//$hashpass = $this->security->hash($password);
			$user = Tblusers::findFirst("userName='$username' AND userStatus='active'");
			if($user){
				if(sha1($password) == $user->userPassword){
					$this->_registerSession($user);
					$this->response->redirect('admin');
				}
			}
			$this->flash->warning('Wrong Username/Password');
		}
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
	}
	/**
	* Create User Function
	*/
	public function createuserAction(){
		\Phalcon\Tag::prependTitle('Create User | ');
		$this->view->menu = $this->_menuActive('umrole');
		$tblroles = Tblroles::find();
		$this->view->tblroles = $tblroles;
		$form = new CreateuserForm();
		$this->view->roleError = null;
		$numberPage = 1;
		if ($this->request->isPost()) {
			if($this->security->getSessionToken() == $this->request->getPost('csrf')){
				$roles = array();
				$roles[] = $umrole = $this->request->getPost('umrole');
				$roles[] = $annrole = $this->request->getPost('annrole');
				$roles[] = $pmrole = $this->request->getPost('pmrole');
				$roles[] = $partrole = $this->request->getPost('partrole');
				$roles[] = $townrole = $this->request->getPost('townrole');
				$roles[] = $conrole = $this->request->getPost('conrole');
				$roles[] = $pagerole = $this->request->getPost('pagerole');
				$roles[] = $extrole = $this->request->getPost('extrole');
				$roles[] = $postrole = $this->request->getPost('postrole');
				if(empty($umrole) && empty($annrole) && empty($pmrole) && empty($partrole) && empty($townrole) && empty($conrole) && empty($pagerole) && empty($extrole) && empty($postrole)){
					$this->view->roleError = "<div class='label label-danger'>At least one role should be selected.</div>";
				}
				if ($form->isValid($this->request->getPost()) != false) {
					$userName = Tblusers::findFirst("userName='".$this->request->getPost('username', 'striptags')."'");
					$userEmail = Tblusers::findFirst("userEmail='".$this->request->getPost('email', 'striptags')."'");
					if($userName==true){
						$this->flash->error("Username already taken");
					}elseif($userEmail==true){
						$this->flash->error("Email Address already taken");
					}else{
						$user = new Tblusers();
						$password = sha1($this->request->getPost('password'));
						$user->assign(array(
							'userName' => $this->request->getPost('username', 'striptags'),
							'userEmail' => $this->request->getPost('email', 'striptags'),
							'userPassword' => $password,
							'userFirstname' => $this->request->getPost('firstname', 'striptags'),
							'userLastname' => $this->request->getPost('lastname', 'striptags'),
							'userMiddlename' => $this->request->getPost('middlename', 'striptags'),
							'userAddress' => $this->request->getPost('address', 'striptags'),
							'userCompany' => $this->request->getPost('company', 'striptags'),
							'userContact' => $this->request->getPost('contact', 'striptags'),
							'userPosition' => $this->request->getPost('position', 'striptags'),
							'userStatus' => $this->request->getPost('status', 'striptags'),
							'userLevel' => 0,
							'dateCreated' => time()
						));
						if (!$user->save()) {
							$this->flash->error($user->getMessages());
						} else {
							$this->_insertRoles($user->userID, $roles);

							$this->flash->success("User was created successfully");
							$this->view->roleError = null;
							Tag::resetInput();
						}
					}
				}
			}else{
				//$this->flash->warning("You cannot resubmit this page");
				Tag::resetInput();
			}
		}
		$this->view->form = $form;
	}
	/*
	* AJAX Action User View
	*/
	public function ajaxUserViewAction($userID){
		if ($this->request->isAjax()){
			$user = Tblusers::findFirst($userID);
			echo "<label>Username: </label> ".$user->userName."<br>";
			echo "<label>Email Address: </label> ".$user->userEmail."<br>";
			echo "<label>Name: </label> ".$user->userFirstname." ".$user->userMiddlename." ".$user->userLastname."<br>";
			echo "<label>Address: </label> ".$user->userAddress."<br>";
			echo "<label>Company: </label> ".$user->userCompany."<br>";
			echo "<label>Position: </label> ".$user->userPosition."<br>";
			echo "<label>Contact: </label> ".$user->userContact."<br>";
			echo "<label>Status: </label> ".$user->userStatus."<br>";
			echo "<label>Date Created: </label> ".date('F j, Y',$user->dateCreated)."<br>";

			$phql = 'SELECT Tblroles.roleDescription FROM Tbluserroles ' .
			' INNER JOIN Tblroles ON tblroles.roleCode = Tbluserroles.userRoles WHERE Tbluserroles.userID = '.$userID;
			$result = $this->modelsManager->executeQuery($phql);
			$rr = null;
			foreach($result as $r){
				$rr .= $r->roleDescription . ', ';
			}
			echo "<label>Roles:</label> <br/>";
			if(empty($rr)){
				echo 'None';
			}else{
				echo substr($rr, 0, -2);
			}
		}else{
			echo "This Page is not for you.";
		}
		$this->view->disableLevel(View::LEVEL_LAYOUT);
	}
	/*
	User Table
	*/
	public function usersAction(){
		$this->view->menu = $this->_menuActive('umrole');
		$numberPage = 1;
		if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
			Tag::resetInput();
		}elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
			$id = $this->request->getPost('recordID');
			if(!empty($id)){
				$this->_deleteUser($id);
			}
		}elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
			$id = $this->request->getPost('tbl_id');
			if(!empty($id)){
				$this->_deleteUser($id);
			}
		}elseif($this->request->isPost() && $this->request->getPost('action') == "edit"){
			$id = $this->request->getPost('recordID');
			if(!empty($id)){
				return $this->dispatcher->forward(array(
					'controller' => 'admin',
					'action' => 'edituser',
					'params' => array($id)
				));
			}
		}elseif($this->request->isPost() && $this->request->getPost('clear_search')){
			$this->session->remove("search_text");
			$this->persistent->searchParams = null;
			unset($_POST);
		}else{
			if ($this->request->isPost()) {
				$query = Criteria::fromInput($this->di, 'Tblusers', array('userName' => $this->request->getPost('search_text'), 'userFirstname' => $this->request->getPost('search_text')));
				$this->persistent->searchParams = $query->getParams();
				$this->session->set("search_text", $this->request->getPost('search_text'));
			} else {
				$numberPage = $this->request->getQuery("page", "int");
			}
		}
		$parameters = array();
		if ($this->persistent->searchParams) {
			$parameters = $this->persistent->searchParams;
		}
		$builder = $this->modelsManager->createBuilder()
		->columns('userID, userName, userEmail, userLastname, userFirstname, userMiddlename, userAddress, userPosition, userContact,userStatus,dateCreated')
		->from('Tblusers')
		->where('userLevel = 0');

		if(!empty($parameters)){
			$this->flash->notice('Search results for "<strong>' . $this->session->get("search_text") .'</strong>"');
			$builder->andWhere($parameters['conditions'], $parameters['bind']);
		}

		$paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
			"builder" => $builder,
			"limit"=> 10,
			"page" => $numberPage
		));
		// Get the paginated results
		$this->view->page = $paginator->getPaginate();
	}
	/**
	* Edit User Action
	*/
	public function edituserAction($userID){
		$this->view->menu = $this->_menuActive('umrole');
		$this->view->roleError = null;
		$user = Tblusers::findFirst($userID);
		$role = Tbluserroles::find('userID ='.$userID);
		$tblroles = Tblroles::find();
		$this->view->tblroles = $tblroles;
		$this->view->user = $user;
		$this->view->passError = null;
		$this->view->repassError = null;
		$form = new CreateuserForm($user, array(
			'edit' => true
		));
		if ($this->request->isPost()) {
			if($this->request->getPost('update_password')){
				$validation = new Phalcon\Validation();
				$validation->add('password', new PresenceOf(array(
					'message' => 'The password is required',
					'cancelOnFail' => true
				)));
				$validation->add('password', new StringLength(array(
					'max' => 20,
					'min' => 8,
					'messageMaximum' => 'Thats an exagerated password.',
					'messageMinimum' => 'Password should be Minimum of 8 characters.'
				)));

				$validation->add('repassword', new PresenceOf(array(
					'message' => 'Retyping your password is required'
				)));
				$validation->add('repassword', new Confirmation(array(
					'message' => 'Password doesn\'t match confirmation',
					'with' => 'password'
				)));

				if(count($validation->validate($_POST))){
					foreach ($validation->getMessages()->filter('password') as $message) {
						$this->view->passError .= "<div class='label label-danger'>".$message."</div> ";
					}
					foreach ($validation->getMessages()->filter('repassword') as $message) {
						$this->view->repassError .= "<div class='label label-danger'>".$message."</div> ";
					}
				}else{
					$user->assign(array(
						'password' => sha1($this->request->getPost('password'))
					));

					if (!$user->save()) {
						$this->flash->error($user->getMessages());

					} else {

						$this->flash->success("User was updated successfully");

						Tag::resetInput();
					}
				}
			}elseif($this->request->getPost('update_button')){
				$roles = array();
				$roles[] = $umrole = $this->request->getPost('umrole');
				$roles[] = $annrole = $this->request->getPost('annrole');
				$roles[] = $pmrole = $this->request->getPost('pmrole');
				$roles[] = $partrole = $this->request->getPost('partrole');
				$roles[] = $townrole = $this->request->getPost('townrole');
				$roles[] = $conrole = $this->request->getPost('conrole');
				$roles[] = $pagerole = $this->request->getPost('pagerole');
				$roles[] = $extrole = $this->request->getPost('extrole');
				$roles[] = $postrole = $this->request->getPost('postrole');
				if(empty($umrole) && empty($annrole) && empty($pmrole) && empty($partrole) && empty($townrole) && empty($conrole) && empty($pagerole) && empty($extrole) && empty($postrole)){
					$this->view->roleError = "<div class='label label-danger'>At least one role should be selected.</div>";
				}
				if (!$form->isValid($_POST)) {
					$form->getMessages();
				}else{
					$userName = Tblusers::findFirst("userName='".$this->request->getPost('username', 'striptags')."' AND userID != ".$userID);
					$userEmail = Tblusers::findFirst("userEmail='".$this->request->getPost('email', 'striptags')."' AND userID != ".$userID);
					if($userName==true){
						$this->flash->error("Username already taken");
					}elseif($userEmail==true){
						$this->flash->error("Email Address already taken");
					}else{
						$user->assign(array(
							'userName' => $this->request->getPost('username', 'striptags'),
							'userEmail' => $this->request->getPost('email', 'striptags'),
							'userFirstname' => $this->request->getPost('firstname', 'striptags'),
							'userLastname' => $this->request->getPost('lastname', 'striptags'),
							'userMiddlename' => $this->request->getPost('middlename', 'striptags'),
							'userAddress' => $this->request->getPost('address', 'striptags'),
							'userCompany' => $this->request->getPost('company', 'striptags'),
							'userContact' => $this->request->getPost('contact', 'striptags'),
							'userPosition' => $this->request->getPost('position', 'striptags'),
							'userStatus' => $this->request->getPost('status', 'striptags'),
							'dateCreated' => time()
						));
						if (!$user->save()) {
							$this->flash->error($user->getMessages());
						} else {
							$this->_insertRoles($user->userID, $roles);
							$role = Tbluserroles::find('userID ='.$userID);
							$this->flash->success("User was updated successfully");
							$this->view->roleError = null;
						}
					}
				}

			}
		}
		foreach($role as $r){
			$this->tag->setDefault($r->userRoles, $r->userRoles);
		}
		$this->tag->setDefault("status", $user->userStatus);
		$this->view->form = $form;

	}
	/**
	* Finishes the active session redirecting to the index
	*
	* @return unknown
	*/
	public function logoutAction()
	{
		$this->session->remove('auth');
		$this->flash->success('Goodbye!');
		$this->dispatcher->forward(
		array(
			'controller' => 'admin',
			'action' => 'index'
		)
	);
}
/*
For PROGRAM MANAGEMENT
*/
public function programsAction($id=null){
	$this->view->menu = $this->_menuActive('pmrole');
	$programs = Tblprograms::find();
	$form = new ProgramsForm();
	$this->view->titletaken = null;
	$this->view->edit = false;
	$this->view->pictureerror = null;

	if(!empty($id)){
		$this->view->edit = true;
		$eprogs = Tblprograms::findFirst("programID=".$id);
		$this->view->eprogs = $eprogs;
		$eprogsi = Tblprogramsimg::find("programID=".$id);
		$this->view->eprogsi = $eprogsi;
	}

	if ($this->request->isPost() && $this->request->getPost('action') == "delete") {
		$id = $this->request->getPost('recordID');
		if(!empty($id)){
			$this->_deleteProgram($id);
			$programs = Tblprograms::find();
			$this->view->programMenu = $programs;
		}
	}elseif($this->request->isPost() && $this->request->getPost('update-program')){
		if (!$form->isValid($_POST)) {
			$form->getMessages();
		}else{
			$pictures = $this->request->getPost('program_picture');
			$programname = Tblprograms::findFirst("programName='".$this->request->getPost('title', 'striptags')."' AND programID != ".$id);
			if($programname){
				$this->flash->error("Program Name already taken");
			}elseif(empty($pictures)){
				$this->flash->error("Please add pictures.");
			}else{
				$eprogs->assign(array(
					'programName' => $this->request->getPost('title', 'striptags'),
					'programTooltip' => $this->request->getPost('tooltip', 'striptags'),
					'programTagline' => $this->request->getPost('tagline', 'striptags'),
					'programPage' => $this->request->getPost('programurl', 'striptags'),
					'programLastUpdated' => time()
				));

				if (!$eprogs->save()) {
					$this->flash->error($user->getMessages());
				} else {

					//Clean other files not included
					$tpi = Tblprogramsimg::find('programID='.$id);
					foreach ($tpi as $t) {
						$ch = false;
						foreach($pictures as $pic){
							if($t->imgname == $pic){
								$ch=true;
							}
						}
						if(!$ch){
							if(is_file('../public/img/programs/'.$t->imgname)){
								unlink('../public/img/programs/'.$t->imgname);
							}
							if(is_file('../public/img/programs/thumbnail/'.$t->imgname)){
								unlink('../public/img/programs/thumbnail/'.$t->imgname);
							}
						}
					}

					$this->modelsManager->executeQuery("DELETE FROM tblprogramsimg WHERE tblprogramsimg.programID = ".$id);

					$phql = "INSERT INTO tblprogramsimg ( imgname, imgpath, programID) "
					. "VALUES (:imgname:, :imgpath:, :programID:)";
					foreach($pictures as $pic){
						if(!is_file('../public/img/programs/'.$pic)){
							$newpicname = md5(uniqid(rand(),true)).$pic;
							$res = $this->modelsManager->executeQuery($phql,
							array(
								'imgname' => $newpicname,
								'imgpath' => 'public/programs/',
								'programID' => $id,
							)
						);
						rename('../public/server/php/files/'.$pic, '../public/img/programs/'.$newpicname);
						rename('../public/server/php/files/thumbnail/'.$pic, '../public/img/programs/thumbnail/'.$newpicname);
					}else{
						$res = $this->modelsManager->executeQuery($phql,
						array(
							'imgname' => $pic,
							'imgpath' => 'public/programs/',
							'programID' => $id,
						)
					);
				}
			}


			$programs = Tblprograms::find();
			$this->view->programMenu = $programs;
			$eprogs = Tblprograms::findFirst("programID=".$id);
			$this->view->eprogs = $eprogs;
			$eprogsi = Tblprogramsimg::find("programID=".$id);
			$this->view->eprogsi = $eprogsi;
			$this->flash->success("Program was updated successfully");
		}
	}
}
}elseif ($this->request->isPost()) {

	if($this->security->getSessionToken() == $this->request->getPost('csrf')){
		$progs = Tblprograms::findFirst("programName='".$this->request->getPost('title', 'striptags')."'");

		if ($form->isValid($this->request->getPost()) != false) {

			if($progs){
				$this->view->titletaken = "<div class='label label-danger'>Program Name already exist.</div>";
			}else{
				$tbp = new Tblprograms();

				$tbp->assign(array(
					'programName' => $this->request->getPost('title', 'striptags'),
					'programTooltip' => $this->request->getPost('tooltip', 'striptags'),
					'programTagline' => $this->request->getPost('tagline', 'striptags'),
					'programPage' => $this->request->getPost('programurl', 'striptags'),
					'programLastUpdated' => time()
				));
				$pictures = $this->request->getPost('program_picture');

				if(empty($pictures)){
					$this->view->pictureerror = "<div class='label label-danger'>Please add pictures.</div>";
				}else{
					if (!$tbp->save()) {
						$this->flash->error($tbp->getMessages());
					} else {
						$phql = "INSERT INTO tblprogramsimg ( imgname, imgpath, programID) "
						. "VALUES (:imgname:, :imgpath:, :programID:)";
						foreach($pictures as $pic){
							$newpicname = md5(uniqid(rand(),true)).$pic;
							$res = $this->modelsManager->executeQuery($phql,
							array(
								'imgname' => $newpicname,
								'imgpath' => 'public/programs/',
								'programID' => $tbp->programID,
							)
						);
						rename('../public/server/php/files/'.$pic, '../public/img/programs/'.$newpicname);
						rename('../public/server/php/files/thumbnail/'.$pic, '../public/img/programs/thumbnail/'.$newpicname);
					}
					$this->_createProgramFolder($tbp->programID);
					$programs = Tblprograms::find();
					$this->view->programMenu = $programs;
					$this->flash->success("Program was created successfully");
					Tag::resetInput();
				}
			}
		}
	}
}else{
	Tag::resetInput();
}
}

$this->view->form = $form;
$this->view->prog = $programs;
}
/*
*  Program Pages
*/
public function programpagesAction($programID){
	$this->view->menu = $this->_menuActive('pmrole');
	$programs = Tblprograms::findFirst('programID='.$programID);
	$this->view->script = '<script> var CURRENT_PROGRAM_FOLDER = '.$programID.'; var CURRENT_FOLDER_CAT = "program"; </script>';
	$this->view->prog = $programs;
	$this->view->titleError = null;
	$this->view->pagetitleError = null;
	$this->_createProgramFolder($programID);
	if($this->request->isPost() && $this->request->getPost('program_page_add')){
		if($this->security->getSessionToken() == $this->request->getPost('csrf')){
			$validation = new Phalcon\Validation();
			$validation->add('program_page_title', new PresenceOf(array(
				'message' => 'Page Title is required.'
			)));

			if(count($validation->validate($_POST))){
				foreach ($validation->getMessages()->filter('program_page_title') as $message) {
					$this->view->titleError .= "<div class='label label-danger'>".$message."</div> ";
				}
			}else{
				$pages = Tblpages::findFirst('pageTitle="'.$this->request->getPost('program_page_title').'" AND pageParent = '.$programID);
				if($pages){
					$this->flash->titleError = "Page title already taken.";
				}else{
					$page = new Tblpages();
					$page->assign(array(
						'pageTitle' => $this->request->getPost('program_page_title', 'striptags'),
						'pageParent' => $programID,
						'pageSlug' => trim($this->request->getPost('program_page_slug', 'striptags'),'-'),
						'pageType' => 'program',
						'pageOrder' => 0,
						'pageLastUpdated' => time()
					));
					if (!$page->save()) {
						$this->flash->error($page->getMessages());
					} else {
						$this->flash->success("Page was created successfully.");
						Tag::resetInput();
					}
				}
			}
		}else{
			Tag::resetInput();
		}
	}elseif($this->request->isPost() && $this->request->getPost('updatebanner')){
		if($this->security->getSessionToken() == $this->request->getPost('csrf')){
			$programs = Tblprograms::findFirst('programID='.$programID);
			$programs->assign(array(
				'programBanner' => $this->request->getPost('program_banner_url', 'striptags')
			));
			if (!$programs->save()) {
				$this->flash->error($programs->getMessages());
			} else {
				$this->flash->success($programs->programName." Banner update successfull.");
				Tag::resetInput();
			}
		}else{
			Tag::resetInput();
		}
		$programs = Tblprograms::findFirst('programID='.$programID);
		$this->view->prog = $programs;
	}elseif($this->request->isPost() && $this->request->getPost('program_page_update')){
		if($this->security->getSessionToken() == $this->request->getPost('csrf')){
			$validation = new Phalcon\Validation();
			$validation->add('program_page_title', new PresenceOf(array(
				'message' => 'Page Title is required.'
			)));
			if(count($validation->validate($_POST))){
				foreach ($validation->getMessages()->filter('program_page_title') as $message) {
					$this->flash->error($message);
				}
			}else{
				$pages = Tblpages::findFirst('pageID='.$this->request->getPost('program_pageID'));
				$pages->assign(array(
					'pageTitle' => $this->request->getPost('program_page_title', 'striptags'),
					'pageContent' => $this->request->getPost('programPageText'),
					'pageKeywords' => $this->request->getPost('program_page_keyword', 'striptags'),
					'pageSlug' => trim($this->request->getPost('program_page_slug', 'striptags'),'-'),
					'pageActive' => $this->request->getPost('program_page_active'),
					'pageLastUpdated' => time()
				));
				if (!$pages->save()) {
					$this->flash->error($pages->getMessages());
				} else {
					$this->flash->success($pages->pageTitle." update successfull.");
					Tag::resetInput();
				}
			}
		}else{
			Tag::resetInput();
		}

	}elseif($this->request->isPost() && $this->request->getPost('updatesort')){
		$result = $this->modelsManager->executeQuery('SELECT tblpages.pageID FROM tblpages WHERE tblpages.pageParent='.$programID.' AND tblpages.pageType="program"');
		$phql = "UPDATE tblpages SET tblpages.pageOrder = :pageorder: WHERE tblpages.pageID=:pageid:";

		foreach($result as $p){

			$this->modelsManager->executeQuery($phql, array(
				'pageorder' => $this->request->getPost('sort_field'.$p->pageID),
				'pageid' => $p->pageID
			));
		}
		$this->flash->success("Page sorting updated.");
		Tag::resetInput();
	}elseif($this->request->isPost() && $this->request->getPost('program_page_delete_yes')){
		if($this->security->getSessionToken() == $this->request->getPost('csrf')){
			$pageID = $this->request->getPost('page_program_delete_id');
			$pages = Tblpages::findFirst("pageID=".$pageID);
			if (!$pages) {
				$this->flash->error("User was not found");
			}

			if (!$pages->delete()) {
				foreach ($pages->getMessages() as $message) {
					$this->flash->error((string) $message);
				}
			} else {
				$this->flash->success("Page was deleted");
			}
		}
	}

	$image_files = array();
	$files = glob('../public/img/programs/'.$programID.'/*.*');
	usort($files, create_function('$a,$b', 'return filemtime($a)<filemtime($b);'));
	foreach($files as $filename){
		$image_files[] = basename($filename);
	}
	$this->view->digital_assets = $image_files;

	$pages = Tblpages::find('pageParent='.$programID.' AND pageType="program" ORDER BY pageOrder ');
	if($pages){
		$this->view->pages = $pages;
	}
}
/*
* Move File Upload of Programs
*/
public function ajaxmoveuploadprogramAction($filename, $folder){
	$newpicname = $filename;
	if(is_file('../public/server/php/files/'.$filename)){
		rename('../public/server/php/files/'.$filename, '../public/img/programs/'.$folder.'/'.$newpicname);
	}
	if(is_file('../public/server/php/files/thumbnail/'.$filename)){
		rename('../public/server/php/files/thumbnail/'.$filename, '../public/img/programs/'.$folder.'/thumbnail/'.$newpicname);
	}
	echo '<div class="program-digital-assets-library pull-left" style="position: relative">
	<a href="'.$this->url->get().'img/programs/'.$folder.'/'.$newpicname.'" class="prettyPhoto[pp_gal]"><img src="'.$this->url->get().'img/programs/'.$folder.'/'.$newpicname.'" alt=""></a>
	<input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="'.$this->url->get().'img/programs/'.$folder.'/'.$newpicname.'">
	<button class="btn btn-xs btn-danger digital-assets-delete" data-filename="'.$filename.'" data-folder="'.$folder.'" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
	</div>';
	$this->view->disableLevel(View::LEVEL_LAYOUT);
}

/*
* Delete File Uploaded
*/
public function ajaxdeleteuploadAction($filename, $folder, $type='program'){
	if($type=='program'){
		if(is_file('../public/img/programs/'.$folder.'/'.$filename)){
			unlink('../public/img/programs/'.$folder.'/'.$filename);
		}
		if(is_file('../public/img/programs/'.$folder.'/thumbnail/'.$filename)){
			unlink('../public/img/programs/'.$folder.'/thumbnail/'.$filename);
		}
	}elseif($type=='post'){
		$folder = explode('-', $folder);
		if(is_file('../public/img/assets/'.$folder[0].'/'.$folder[1].'/'.$filename)){
			unlink('../public/img/assets/'.$folder[0].'/'.$folder[1].'/'.$filename);
		}
	}
	$this->view->disableLevel(View::LEVEL_LAYOUT);
}
/*
* Ajax View Page
*/
public function ajaxviewpageAction($pagID){

	$pages = Tblpages::findFirst('pageID='.$pagID);
	if (!$pages) {
		echo "Page was not found";
	}else{
		echo $pages->pageContent;
	}

	$this->view->disableLevel(View::LEVEL_LAYOUT);
}
/*
* =======================================================================================================================
* NEWS POST
* =======================================================================================================================
*/
/*
* Create a News Post
*/
public function createpostAction(){
	$this->view->menu = $this->_menuActive('postrole');
	$programs = Tblprograms::find();
	$this->view->script = '<script> var CURRENT_FOLDER_CAT = "post"; </script>';
	$form = new CreatepostForm();
	$this->view->titleError = null;
	$this->view->contentError = null;
	$this->view->folders = $this->_getFolderAssets();

	$f = $this->_getFolderAssets();
	$toparray = array();
	foreach ($f as $fkey => $fvalue) {
		$toparray[] = $fkey;
		foreach($fvalue as $ff){
			$toparray[] = $ff;
			break;
		}
		break;
	}
	$this->view->dayear = $toparray[0];
	$this->view->damonth = $toparray[1];

	if($this->request->isPost() && $this->request->getPost('submit_post')){
		if($this->security->getSessionToken() == $this->request->getPost('csrf')){
			if ($form->isValid($this->request->getPost()) != false) {
				$postcheck = Tblpost::findFirst('postTitle="'.$this->request->getPost('post_title', 'striptags').'"');
				if($postcheck==true){
					$this->flash->error('Page title already taken.');
				}else{
					$post = new Tblpost();
					$post->assign(array(
						'postTitle' => $this->request->getPost('post_title', 'striptags'),
						'postSlug' =>  trim($this->request->getPost('post_slug', 'striptags'),'-'),
						'postContent' => $this->request->getPost('post_content', 'striptags'),
						'postStatus' => ($this->request->getPost('post_status') ? $this->request->getPost('post_status') : 'publish'),
						'postDate' => time(),
						'postPublishDate' => ($this->request->getPost('postDatePublish', 'striptags') ? strtotime($this->request->getPost('postDatePublish', 'striptags')):time()),
						'postKeyword' => $this->request->getPost('post_keywords', 'striptags'),
						'postFeatureImage' => $this->request->getPost('post_feature_image', 'striptags')
					));
					if (!$post->save()) {
						$this->flash->error($post->getMessages());
					} else {
						$this->_insertPostCat($post->postID, $programs, "programs");
						$this->flash->success("Post was created successfully.");
						Tag::resetInput();
					}
				}
			}
		}else{
			Tag::resetInput();
		}
	}
	$this->view->form = $form;
	$this->view->programs = $programs;
}
/*
* All Post
*/
public function allnewspostAction(){
	$this->view->menu = $this->_menuActive('postrole');
	$numberPage = 1;
	if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
		Tag::resetInput();
	}elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
		$id = $this->request->getPost('recordID');
		if(!empty($id)){
			$this->_deletePost($id);
		}
	}elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
		$id = $this->request->getPost('tbl_id');
		if(!empty($id)){
			$this->_deletePost($id);
		}
	}elseif($this->request->isPost() && $this->request->getPost('action') == "edit"){
		$id = $this->request->getPost('recordID');
		if(!empty($id)){
			return $this->dispatcher->forward(array(
				'controller' => 'admin',
				'action' => 'editPost',
				'params' => array($id)
			));
		}
	}elseif($this->request->isPost() && $this->request->getPost('clear_search')){
		$this->session->remove("search_text");
		$this->persistent->searchParams = null;
		unset($_POST);
	}else{
		if ($this->request->isPost()) {
			$query = Criteria::fromInput($this->di, 'Tblpost', array('postTitle' => $this->request->getPost('search_text')));
			$this->persistent->searchParams = $query->getParams();
			$this->session->set("search_text", $this->request->getPost('search_text'));
		} else {
			$numberPage = $this->request->getQuery("page", "int");
		}
	}


	$parameters = array();
	if ($this->persistent->searchParams) {
		$parameters = $this->persistent->searchParams;
	}
	$builder = $this->modelsManager->createBuilder()
	->columns('postID, postTitle, postDate, postPublishDate, postStatus, postKeyword')
	->from('Tblpost');

	if(!empty($parameters)){
		$this->flash->notice('Search results for "<strong>' . $this->session->get("search_text") .'</strong>"');
		$builder->andWhere($parameters['conditions'], $parameters['bind']);
	}

	$paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
		"builder" => $builder,
		"order" => "postDate DESC",
		"limit"=> 10,
		"page" => $numberPage
	));

	// Get the paginated results
	$this->view->page = $paginator->getPaginate();
}
/*
* Edit Post
*/
public function editnewsAction($id){
	$this->view->menu = $this->_menuActive('postrole');
	$programs = Tblprograms::find();
	$post = Tblpost::findFirst('postID='.$id);
	$this->view->script = '<script> var CURRENT_FOLDER_CAT = "post"; </script>';
	$form = new CreatepostForm();
	$this->view->titleError = null;
	$this->view->contentError = null;
	$this->view->folders = $this->_getFolderAssets();

	$f = $this->_getFolderAssets();
	$toparray = array();
	foreach ($f as $fkey => $fvalue) {
		$toparray[] = $fkey;
		foreach($fvalue as $ff){
			$toparray[] = $ff;
			break;
		}
		break;
	}
	$this->view->dayear = $toparray[0];
	$this->view->damonth = $toparray[1];

	if($this->request->isPost() && $this->request->getPost('update_post')){
		if($this->security->getSessionToken() == $this->request->getPost('csrf')){
			if ($form->isValid($this->request->getPost()) != false) {
				$postcheck = Tblpost::findFirst('postTitle="'.$this->request->getPost('post_title', 'striptags').'" AND  postID != '.$id);
				if($postcheck==true){
					$this->flash->error('Page title already taken.');
				}else{
					$post->assign(array(
						'postTitle' => $this->request->getPost('post_title', 'striptags'),
						'postSlug' =>  trim($this->request->getPost('post_slug', 'striptags'),'-'),
						'postContent' => $this->request->getPost('post_content'),
						'postStatus' => ($this->request->getPost('post_status') ? $this->request->getPost('post_status') : 'publish'),
						'postDate' => time(),
						'postPublishDate' => ($this->request->getPost('postDatePublish', 'striptags') ? strtotime($this->request->getPost('postDatePublish', 'striptags')):time()),
						'postKeyword' => $this->request->getPost('post_keywords', 'striptags'),
						'postFeatureImage' => $this->request->getPost('post_feature_image', 'striptags')
					));
					if (!$post->save()) {
						$this->flash->error($post->getMessages());
					} else {
						$this->_insertPostCat($post->postID, $programs, "programs");
						$this->flash->success("Post was updated successfully.");
						Tag::resetInput();
					}
				}
			}
		}else{
			Tag::resetInput();
		}
	}

	$postcat = Tblpostcat::find('postID='.$id);
	foreach($postcat as $p){
		$this->tag->setDefault('chkProg'.$p->relatedID, $p->relatedID);

	}
	if($post->postStatus=='draft'){
		$this->tag->setDefault('post_status', $post->postStatus);
	}
	$this->view->form = $form;
	$this->view->programs = $programs;
	$this->view->post = $post;
}
/*
* Edit Profile
*/
/**
* Edit User Action
*/
public function editprofileAction(){
	$userid = $this->session->get('auth');
	$this->view->menu = null;
	$this->view->roleError = null;
	$userID = $userid['abk_id'];
	$user = Tblusers::findFirst($userID);
	$this->view->user = $user;
	$this->view->passError = null;
	$this->view->repassError = null;
	$this->view->oldpassError = null;
	$form = new CreateuserForm($user, array(
		'editown' => true,
		'edit' => true
	));
	if ($this->request->isPost()) {
		if($this->request->getPost('update_password')){
			$validation = new Phalcon\Validation();
			$validation->add('password', new PresenceOf(array(
				'message' => 'The password is required',
				'cancelOnFail' => true
			)));
			$validation->add('password', new StringLength(array(
				'max' => 20,
				'min' => 8,
				'messageMaximum' => 'Thats an exagerated password.',
				'messageMinimum' => 'Password should be Minimum of 8 characters.'
			)));

			$validation->add('repassword', new PresenceOf(array(
				'message' => 'Retyping your password is required'
			)));
			$validation->add('repassword', new Confirmation(array(
				'message' => 'Password doesn\'t match confirmation',
				'with' => 'password'
			)));

			if(count($validation->validate($_POST))){
				foreach ($validation->getMessages()->filter('password') as $message) {
					$this->view->passError .= "<div class='label label-danger'>".$message."</div> ";
				}
				foreach ($validation->getMessages()->filter('repassword') as $message) {
					$this->view->repassError .= "<div class='label label-danger'>".$message."</div> ";
				}
			}else{
				if(sha1($this->request->getPost('oldpassword')) == $user->userPassword){
					$user->assign(array(
						'password' => sha1($this->request->getPost('password'))
					));

					if (!$user->save()) {
						$this->flash->error($user->getMessages());

					} else {

						$this->flash->success("Account Password was updated successfully");

						Tag::resetInput();
					}
				}else{
					$this->view->oldpassError = "<div class='label label-danger'>Wrong Password</div> ";
				}


			}
		}elseif($this->request->getPost('update_button')){
			if (!$form->isValid($_POST)) {
				$form->getMessages();
			}else{
				$userName = Tblusers::findFirst("userName='".$this->request->getPost('username', 'striptags')."' AND userID != ".$userID);
				$userEmail = Tblusers::findFirst("userEmail='".$this->request->getPost('email', 'striptags')."' AND userID != ".$userID);
				if($userName==true){
					$this->flash->error("Username already taken");
				}elseif($userEmail==true){
					$this->flash->error("Email Address already taken");
				}else{
					$user->assign(array(
						'userName' => $this->request->getPost('username', 'striptags'),
						'userEmail' => $this->request->getPost('email', 'striptags'),
						'userFirstname' => $this->request->getPost('firstname', 'striptags'),
						'userLastname' => $this->request->getPost('lastname', 'striptags'),
						'userMiddlename' => $this->request->getPost('middlename', 'striptags'),
						'userAddress' => $this->request->getPost('address', 'striptags'),
						'userCompany' => $this->request->getPost('company', 'striptags'),
						'userContact' => $this->request->getPost('contact', 'striptags'),
						'userPosition' => $this->request->getPost('position', 'striptags'),
						'userStatus' => $user->userStatus,
						'dateCreated' => time()
					));
					if (!$user->save()) {
						$this->flash->error($user->getMessages());
					} else {
						$role = Tbluserroles::find('userID ='.$userID);
						$this->flash->success("You account was updated successfully");
						$this->view->roleError = null;
					}
				}
			}

		}
	}
	$this->view->form = $form;

}
/*
* Move Upload for Post Digital Assets
*/
public function ajaxmoveuploadpostAction($filename){
	$path = null;
	if(is_file('../public/server/php/files/'.$filename)){
		$path = '../public/img/assets/'.date('Y').'/'.date('m');
		if(!is_dir('../public/img/assets/'.date('Y'))){
			mkdir('../public/img/assets/'.date('Y'));
		}
		if(!is_dir('../public/img/assets/'.date('Y').'/'.date('m'))){
			mkdir($path);
			mkdir($path.'/thumbnail');
		}
		rename('../public/server/php/files/'.$filename, $path.'/'.$filename);
	}
	if(is_file('../public/server/php/files/thumbnail/'.$filename)){
		rename('../public/server/php/files/thumbnail/'.$filename, $path.'/thumbnail/'.$filename);
	}
	$ext = pathinfo($filename, PATHINFO_EXTENSION);
	$imgpath = $this->url->get().'img/assets/'.date('Y').'/'.date('m').'/'.$filename;
	$newfilename = strlen($filename) > 15 ? substr($filename,0,15)."...".$ext : $filename;
	echo '<div class="program-digital-assets-library pull-left" style="position: relative">
	<div style="padding-left: 25px; width: 100%; word-wrap:break-word;">'.$newfilename.'</div>
	<a href="'.$imgpath.'" class="prettyPhoto[pp_gal]"><img src="'.$imgpath.'" alt=""></a>
	<input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="'.$imgpath.'">
	<button class="btn btn-xs btn-danger digital-assets-post-delete" data-filename="'.$filename.'" data-folder="'.date('Y').'-'.date('m').'" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
	<div class="clearfix"></div>
	</div>';
	$this->view->disableLevel(View::LEVEL_LAYOUT);
}
/*
* Ajax View Digital Assets
*/
public function ajaxviewdigitalassetsAction($year, $month){
	if(is_dir('../public/img/assets/'.$year.'/'.$month)){
		$path = '../public/img/assets/'.$year.'/'.$month;
		$files = glob($path.'/*.*');
		usort($files, create_function('$a,$b', 'return filemtime($a)<filemtime($b);'));
		foreach($files as $filename){

			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			//$imgpath = $this->url->get().'public/img/assets/'.$year.'/'.$month.'/'.$filename;
			$exts = array('jpg','jpeg','png','gif');

			$imgpath = $this->url->get().'img/assets/'.$year.'/'.$month.'/'.basename($filename);
			if(in_array($ext, $exts)){
				$img = '<a href="'.$imgpath.'" class="prettyPhoto[pp_gal]"><img src="'.$imgpath.'" alt=""></a>';
			}else{
				$img = '<a href="'.$imgpath.'" target="_blank"><img src="'.$this->url->get().'public/img/file.png" alt="" style="height: 100px;"></a>';
			}

			$name = basename($filename);
			$newfilename = strlen($name) > 15 ? substr($name,0,15)."...".$ext : $name;
			echo '<div class="program-digital-assets-library pull-left" style="position: relative">
			<div style="padding-left: 25px; width: 100%; word-wrap:break-word;">'.$newfilename.'</div>
			<a href="'.$filename.'" class="prettyPhoto[pp_gal]">'.$img.'</a>
			<input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="'.$imgpath.'">
			<button class="btn btn-xs btn-danger digital-assets-post-delete" data-filename="'.basename($filename).'" data-folder="'.$year.'-'.$month.'" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
			<div class="clearfix"></div>
			</div>';
		}
	}
	$this->view->disableLevel(View::LEVEL_LAYOUT);
}
private function _getFolderAssets(){
	$path = '../public/img/assets/';
	$assets = scandir($path);
	$folders = array();
	foreach ($assets as $a) {
		if ($a === '.' or $a === '..') continue;

		if (is_dir($path . '/' . $a)) {
			$subassets = scandir($path . '/' . $a);
			$subfolders = array();
			foreach ($subassets as $sa){
				if ($sa === '.' or $sa === '..') continue;

				if (is_dir($path . '/' . $a . '/'. $sa)) {
					$subfolders[] = $sa;
				}
			}
			arsort($subfolders);
			$folders[$a] = $subfolders;
		}
	}
	arsort($folders);
	return $folders;
}
private function _createProgramFolder($programID){
	$dir = '../public/img/programs';
	if(is_dir($dir)){
		$path = $dir.'/'.$programID;
		if(!is_dir($path)){
			mkdir($path);
			mkdir($path.'/thumbnail');
		}
		return $path;
	}else{
		die('directorynoexist');
	}
}
private function _registerSession($user)
{
	$this->session->set('auth', array(
		'abk_id' => $user->userID,
		'abk_fullname' => $user->userFirstname .' '.$user->userLastname
	));
	$phql = 'SELECT Tblroles.rolePage,Tblroles.roleDescription, Tblroles.roleCode FROM Tbluserroles ' .
	' INNER JOIN Tblroles ON Tblroles.roleCode = Tbluserroles.userRoles WHERE Tbluserroles.userID = '.$user->userID;
	$result = $this->modelsManager->executeQuery($phql);
	$sessrole = array();
	$sesspage = array();

	if($user->userLevel == 1){
		$phql = 'SELECT Tblroles.rolePage, Tblroles.roleCode FROM Tblroles ';
		$result = $this->modelsManager->executeQuery($phql);
		foreach($result as $r){
			$sessrole[] = $r->roleCode;
			$sesspage[] = explode(',', $r->rolePage);
		}
	}else{
		foreach($result as $r){
			$sessrole[] = $r->roleCode;
			$sesspage[] = explode(',', $r->rolePage);
		}
	}
	$roles = array('roles' => $sessrole, 'page' => array_filter(call_user_func_array('array_merge', $sesspage)));

	$this->session->set('roles', $roles );

	//Set SuperAdmin
	if($user->userLevel){
		$this->session->set('SuperAdmin', true );
	}
}
private function _menuActive($menu)
{
	$phql = 'SELECT Tblroles.rolePage, Tblroles.roleCode FROM Tblroles ';
	$result = $this->modelsManager->executeQuery($phql);
	$menus = array();
	$menus['dashboard'] = false;
	foreach($result as $r){
		$menus[$r->roleCode] = false;
	}
	$menus[$menu] = true;
	return $menus;
}
private function _deleteUser($id){
	$param=null;
	if(is_array($id)){
		$newid = array();
		foreach ($id as $i) {
			$param .= 'userID = '. $this->filter->sanitize($i, array("int")) . ' OR ';
		}
		$param = substr($param, 0, -4);
	}else{
		$id = $this->filter->sanitize($id, array("int"));
		$param = 'userID=' . $id;
	}

	$products = Tblusers::find($param);
	if (!$products) {
		$this->flash->error("User was not found");
	}

	if (!$products->delete()) {
		foreach ($products->getMessages() as $message) {
			$this->flash->error((string) $message);
		}
		return $this->forward("admin/users");
	} else {
		$this->flash->success("User was deleted");
	}
}
private function _deleteProgram($id){
	$id = $this->filter->sanitize($id, array("int"));
	$param = 'programID=' . $id;

	$tbp = Tblprograms::find($param);
	if(!$tbp){
		$this->flash->error("Program was not found");
	}

	$tbpage = Tblpages::find('pageParent='.$id.' AND pageType="program"');
	if(!$tbpage){
		$this->flash->error("Page was not found");
	}

	$tbpi = Tblprogramsimg::find($param);
	if(count($tbpi)){
		foreach($tbpi as $t){
			if(is_file('../public/img/programs/thumbnail/'.$t->imgname)){
				unlink('../public/img/programs/thumbnail/'.$t->imgname);
			}
			if(is_file('../public/img/programs/'.$t->imgname)){
				unlink('../public/img/programs/'.$t->imgname);
			}
		}
	}
	if(is_dir('../public/img/programs/'.$id)){
		$this->_deletefolder('../public/img/programs/'.$id);
	}
	if(!$tbp->delete() || !$tbpi->delete() || !$tbpage->delete()){
		foreach ($tbp->getMessages() as $message) {
			$this->flash->error((string) $message);
		}
		foreach ($tbpi->getMessages() as $message) {
			$this->flash->error((string) $message);
		}
		foreach ($tbpage->getMessages() as $message) {
			$this->flash->error((string) $message);
		}
	}else{
		$this->flash->success('Program was deleted.');
	}

}
private function _deletefolder($path)
{
	if (is_dir($path) === true)
	{
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::CHILD_FIRST);

		foreach ($files as $file)
		{
			if (in_array($file->getBasename(), array('.', '..')) !== true)
			{
				if ($file->isDir() === true)
				{
					rmdir($file->getPathName());
				}

				else if (($file->isFile() === true) || ($file->isLink() === true))
				{
					unlink($file->getPathname());
				}
			}
		}

		return rmdir($path);
	}

	else if ((is_file($path) === true) || (is_link($path) === true))
	{
		return unlink($path);
	}

	return false;
}
private function _insertRoles($id, $roles){
	//Delete if there is existing
	$this->modelsManager->executeQuery('DELETE FROM Tbluserroles WHERE Tbluserroles.userID = '.$id);
	// Inserting using placeholders
	$phql = "INSERT INTO Tbluserroles ( userID, userRoles) "
	. "VALUES (:userid:, :role:)";
	foreach($roles as $r){
		if(!empty($r)){
			$this->modelsManager->executeQuery($phql,
			array(
				'userid'     => $id,
				'role' => $r,
			)
		);
	}
}
}
private function _insertPostCat($id, $programs, $type){
	//Delete if there is existing
	$this->modelsManager->executeQuery('DELETE FROM Tblpostcat WHERE Tblpostcat.postID = '.$id);
	// Inserting using placeholders
	$phql = "INSERT INTO Tblpostcat ( postID, relatedID, relatedtype) "
	. "VALUES (:id:, :prog:, :type:)";
	foreach($programs as $p){
		if($this->request->getPost('chkProg'.$p->programID)){
			$this->modelsManager->executeQuery($phql,
			array(
				'id'     => $id,
				'prog' => $p->programID,
				'type' => $type
			)
		);
	}

}

}
private function _deletePost($id){
	$param=null;
	if(is_array($id)){
		$newid = array();
		foreach ($id as $i) {
			$param .= 'postID = '. $this->filter->sanitize($i, array("int")) . ' OR ';
		}
		$param = substr($param, 0, -4);
	}else{
		$id = $this->filter->sanitize($id, array("int"));
		$param = 'postID=' . $id;
	}

	$post = Tblpost::find($param);
	if (!$post) {
		$this->flash->error("Post was not found");
	}
	$postcat = Tblpostcat::find($param);
	if (!$post) {
		$this->flash->error("Post was not found");
	}

	if (!$post->delete() && !$postcat->delete()) {
		foreach ($post->getMessages() as $message) {
			$this->flash->error((string) $message);
		}
		foreach ($postcat->getMessages() as $message) {
			$this->flash->error((string) $message);
		}
		return $this->forward("admin/allnewspost");
	} else {
		$this->flash->success("Post was deleted");
	}
}

}
