<?php

class AnnouncementsController extends ControllerBase
{

    protected $breadCrumbs = "<a href='/'>Home</a> > Announcements";
    public function initialize()
    {
        parent::initialize();
        $this->view->bread_crumbs = $this->breadCrumbs;
        $this->validateLoginVolunteer();
    }

    public function indexAction()
    {
        $numberPage = $this->request->getQuery("page", "int");
        $numberPage = !empty($numberPage)?$numberPage:1;

        $phql = 'SELECT * FROM Tblannouncements WHERE UNIX_TIMESTAMP() between annStart AND annEnd ORDER BY annID DESC';
        $result = $this->modelsManager->executeQuery($phql);

        $dataArray = array();
        foreach ($result as $key => $value) {            
            $dataArray[] = array(
                'annID'=>$value->annID,
                'annTitle'=>$value->annTitle,
                'annDesc'=>$this->_truncateHtml($value->annDesc),
                'annDate'=>$value->annStart
                );
        }

        $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
            "data" => $dataArray,
            "limit"=> 10,
            "page" => $numberPage
            ));
        $this->view->page = $paginator->getPaginate();
    }

    public function viewAction($id){
        $phql = 'SELECT * FROM Tblannouncements WHERE (UNIX_TIMESTAMP() between annStart AND annEnd) AND annID ='. $id;
        $this->view->ann = $result = $this->modelsManager->executeQuery($phql);

        $about=Tblother::findfirst("title='Main Tagline'");
       $this->view->about=$about;
       $contact= Tblcontact::find();
       $this->view->contacts=$contact;
    }
}