<?php
use Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\StringLength,
Phalcon\Validation\Validator\Email;
class ContactusController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->validateLoginVolunteer();
    }

    public function indexAction()
    {
       $about=Tblother::findfirst("title='Main Tagline'");
       $this->view->about=$about;
       $contact= Tblcontact::find();
       $this->view->contacts=$contact;

        $progs = Tblprograms::find();

        if($this->request->isPost() && $this->request->getPost('send')){

            

            if($this->request->isPost() && $this->security->getSessionToken() == $this->request->getPost('csrf')){
            
                $name = trim($this->request->getPost('name', 'striptags'));
                $email = trim($this->request->getPost('email', 'striptags'));
                $message = trim($this->request->getPost('message', 'striptags'));
                $subject = trim($this->request->getPost('subject', 'striptags'));

                $validation = new Phalcon\Validation();
                $validation->setFilters('name', 'trim');
                $validation->setFilters('message', 'trim');
                $validation->setFilters('subject', 'trim');
                $validation->add('name', new PresenceOf(array(
                    'message' => 'Your name is required.'
                    )));
                $validation->add('name', new StringLength(array(
                      'min' => 2,
                      'messageMinimum' => 'Full name should contain minimum of 2 characters.'            
                    )));

                $validation->add('subject', new PresenceOf(array(
                    'message' => 'The subject is required'
                    )));
                
                $validation->add('email', new PresenceOf(array(
                    'message' => 'Your e-mail is required'
                    )));

                $validation->add('message', new PresenceOf(array(
                    'message' => 'Your message is required',
                    'cancelOnFail' => true
                    )));

                $validation->add('email', new Email(array(
                    'message' => 'Your e-mail is not valid'
                    )));

                $messages = $validation->validate($_POST);
                if (count($messages)) {
                    $errorMessage = null;
                    foreach ($messages as $message) {
                        $errorMessage .= '<li>'.$message. '</li>';
                    }
                    echo '<div class="alert alert-danger"><strong>Please fix the following:</strong><ul>'.$errorMessage.'</ul></div>';
                }else{
                    $inquiry = new Tblinquiries();
                    $inquiry->assign(array(
                        'inqSender'=> $name,
                        'inqSubject' => $subject,
                        'inqEmail'=>$email,
                        'inqMessage'=>$message,
                        'inqDate'=>time(),
                        'inqStatus'=>0
                        ));

                    $mailObjects = array(
                    'From'=> 'angbayanko.org@no-reply.com',
                    'FromName' => $email,
                    'AddAddress'=> Tblusers::findFirst("userID = 1")->userEmail,
                    'Subject' => $subject,
                    'Body' => '
                    <p>'.$message.'</p>
                    [DO NOT REPLY ON THIS EMAIL].<br/><p>This email has been sent to notify that an inquiry has been sent on the AngBayanKo Administrator.</p>'
                    );
                    
                    if($inquiry->save())
                        $this->_sendmail($mailObjects);
                    
                    unset($_POST);
                     // Using session flash
                    $this->flashSession->success('<div class="alert alert-success">Your inquiry has been successfully sent and we appreciate you contacting us. We\'ll be in touch soon. Thank you. </div>');

                    // Make a full HTTP redirection
                    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    return $this->response->redirect($actual_link);
                    
                }
            }else{
                
                $name = $this->request->getPost('name', 'striptags');
                $email = $this->request->getPost('email', 'striptags');
                $message = $this->request->getPost('message', 'striptags');
                $subject = $this->request->getPost('subject', 'striptags');

                $validation = new Phalcon\Validation();

                $validation->add('name', new PresenceOf(array(
                    'message' => 'Your name is required'
                    )));

                $validation->add('subject', new PresenceOf(array(
                    'message' => 'The subject is required'
                    )));
                
                $validation->add('email', new PresenceOf(array(
                    'message' => 'Your e-mail is required'
                    )));

                $validation->add('message', new PresenceOf(array(
                    'message' => 'Your message is required',
                    'cancelOnFail' => true
                    )));

                $validation->add('email', new Email(array(
                    'message' => 'Your e-mail is not valid'
                    )));

                $messages = $validation->validate($_POST);
                if (count($messages)) {
                    $errorMessage = null;
                    foreach ($messages as $message) {
                        $errorMessage .= '<li>'.$message. '</li>';
                    }
                    echo '<div class="alert alert-danger"><strong>Please fix the following:</strong><ul>'.$errorMessage.'</ul></div>';
                }else{
                    $inquiry = new Tblinquiries();
                    $inquiry->assign(array(
                        'inqSender'=> $name,
                        'inqSubject' => $subject,
                        'inqEmail'=>$email,
                        'inqMessage'=>$message,
                        'inqDate'=>time(),
                        'inqStatus'=>0
                        ));

                    $mailObjects = array(
                    'From'=> 'angbayanko.org@no-reply.com',
                    'FromName' => $email,
                    'AddAddress'=> Tblusers::findFirst("id = 1")->userEmail,
                    'Subject' => $subject,
                    'Body' => '
                    <p>'.$message.'</p>
                    <br><br>
                    [DO NOT REPLY ON THIS EMAIL].<br/><p>This email has been sent to notify that an inquiry has been sent on the AngBayanKo Administrator.</p>'
                    );
                    
                    if($inquiry->save())
                        $this->_sendmail($mailObjects);
                    
                    unset($_POST);
                     // Using session flash
                    $this->flashSession->success('<div class="alert alert-success">Your inquiry has been successfully sent and we appreciate you contacting us. We\'ll be in touch soon. Thank you. </div>');

                    // Make a full HTTP redirection
                    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    return $this->response->redirect($actual_link);
                    
                }
            }
        }
    }

    public function submitEmailNewsLetterAction(){
        $this->view->disable();
        $response = array();
        $name = $this->request->getPost('name', 'striptags');
        $email = $this->request->getPost('email', 'striptags');

        $validation = new Phalcon\Validation();

        $validation->add('name', new PresenceOf(array(
            'message' => 'The name is required'
            )));
        
        $validation->add('email', new PresenceOf(array(
            'message' => 'The e-mail is required',
            'cancelOnFail' => true
            )));

        $validation->add('email', new Email(array(
            'message' => 'The e-mail is not valid'
            )));

        $messages = $validation->validate($_POST);
        if (count($messages)) {
            $errorMessage = null;
            foreach ($messages as $message) {
                $errorMessage .= $message. '<br>';
            }
            $response['success'] = false;
            $response['message'] = '<div class="alert alert-warning">'.$errorMessage.'</div>';
        }else{
            $enews = Tblnewsletteremails::findFirst('email="'.$email.'"');
            if($enews){
                $response['success'] = false;
                $response['message'] = '<div class="alert alert-warning">Your email is already registered to our e-newsletter.</div>';
            }else{
                $enews = new Tblnewsletteremails();
                $enews->assign(array('name'=>$name, 'email'=>$email, 'dateAdded'=>time()));
                $enews->save();
                $response['success'] = true;
                $response['message'] = '<div class="alert alert-success">Thank you for subscribing to our e-newsletter.</div>';
            }
        }

        

        echo json_encode($response);
    }
}