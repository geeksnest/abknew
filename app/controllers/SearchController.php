<?php

class SearchController extends ControllerBase
{

    protected $breadCrumbs = "<a href='/'>Home</a> > Search";
    public function initialize()
    {
        parent::initialize();
        $this->view->bread_crumbs = $this->breadCrumbs;
        $this->validateLoginVolunteer();
    }

    public function indexAction(){
         $contact= Tblcontact::find();
       $this->view->contacts=$contact;
       
        $searchString = $this->request->getPost('keyword', 'striptags');
        $searchString = empty($searchString) && isset($_GET['s'])?urldecode($_GET['s']):$searchString;
        if(!empty($searchString)){
            echo '<h3 class="fontNormal">Search result(s) for "<strong>'.$searchString.'</strong>"</h3><br />';
            $this->view->searchString = urlencode($searchString);

            $numberPage = $this->request->getQuery("page", "int");
            $numberPage = !empty($numberPage)?$numberPage:1;

            $dataArray = array();

            $phql = '
                SELECT
                Tblannouncements.annID AS urlKey,
                Tblannouncements.annTitle AS searchTitle,
                Tblannouncements.annDesc AS searchDesc
                FROM Tblannouncements
                WHERE
                (Tblannouncements.annTitle LIKE "%'.$searchString.'%" OR Tblannouncements.annDesc LIKE "%'.$searchString.'%") AND
                UNIX_TIMESTAMP() between Tblannouncements.annStart AND Tblannouncements.annEnd
            ';
            $dataArray = array_merge($dataArray, $this->_querySearch($phql, 'announcements/view/'));

            $dataArray = array_merge($dataArray, $this->_querySearch($this->_prepareSql('Tblpost', 'postTitle', 'postContent', $searchString, 'postSlug'), 'post/news/'));
            $dataArray = array_merge($dataArray, $this->_querySearch($this->_prepareSql('Tbltowns', 'townName', 'townInfo', $searchString, 'townID'), 'towns/information/'));
            $dataArray = array_merge($dataArray, $this->_querySearch($this->_prepareSql('Tblpartnerevents', 'eventTitle', 'eventDetails', $searchString, 'eventID'), 'partners/currentevents/'));
            $dataArray = array_merge($dataArray, $this->_querySearch($this->_prepareSql('Tbltownevents', 'eventTitle', 'eventDetails', $searchString, 'eventID'), 'towns/events/'));
            $dataArray = array_merge($dataArray, $this->_querySearch($this->_prepareSql('Tblpartners', 'partnerName', 'partnerInfo', $searchString, 'partnerID'), 'partners/view/'));
            //$dataArray = array_merge($dataArray, $this->_querySearch($this->_prepareSql('Tblpages', 'pageTitle', 'pageContent', $searchString, 'pageSlug'), 'site/pages/'));
            

            $phql = '
                SELECT
                Tblpages.pageSlug AS urlKey,
                Tblpages.pageTitle AS searchTitle,
                Tblpages.pageContent AS searchDesc
                FROM Tblpages
                WHERE
                (Tblpages.pageTitle LIKE "%'.$searchString.'%" OR Tblpages.pageSlug LIKE "%'.$searchString.'%" OR Tblpages.pageContent LIKE "%'.$searchString.'%") AND
                Tblpages.pageActive = 1
            ';
            $dataArray = array_merge($dataArray, $this->_querySearch($phql, 'site/pages/'));
            $dataArray = array_merge($dataArray, $this->_querySearch($this->_prepareSql('Tblprograms', 'programName', 'programTagline', $searchString, 'programPage'), 'programs/page/'));

            $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
                "data" => $dataArray,
                "limit"=> 10,
                "page" => $numberPage
                ));
            $this->view->page = $paginator->getPaginate();
        }else{
            echo '<h3>No search result found.</h3>Please enter your serach keyword.';
        }


        
    }

    private function _prepareSql($table, $searchTitle, $searchDesc, $searchString, $urlKey){
        return $phql = '
                SELECT
                '.$urlKey.' AS urlKey,
                '.$table.'.'.$searchTitle.' AS searchTitle,
                '.$table.'.'.$searchDesc.' AS searchDesc
                FROM '.$table.'
                WHERE
                '.$table.'.'.$searchTitle.' LIKE "%'.$searchString.'%" OR '.$table.'.'.$searchDesc.' LIKE "%'.$searchString.'%"
            ';
    }

    private function _querySearch($phql, $url){
        $dataArray = array();
        $result = $this->modelsManager->executeQuery($phql);

        foreach ($result as $key => $value) {            
            $dataArray[] = array(
                'searchTitle'=>$value->searchTitle,
                'searchDesc'=>$this->_truncateHtml($value->searchDesc),
                'searchUrl'=>$url.$value->urlKey
                );
        }

        return $dataArray;
    }
}