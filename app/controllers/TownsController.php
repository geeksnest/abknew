<?php
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email,
    Phalcon\Validation\Validator\StringLength as StringLength;
class TownsController extends ControllerBase
{

    protected $breadCrumbs = "<a href='/'>Home</a> > ";
    public function initialize()
    {
        parent::initialize();
        $this->view->bread_crumbs = $this->breadCrumbs;
        $this->validateLoginVolunteer();

    }

    public function indexAction(){

       $about=Tblother::findfirst("title='Main Tagline'");
       $this->view->about=$about;
       $contact= Tblcontact::find();
       $this->view->contacts=$contact;

    	$this->view->bread_crumbs = $this->breadCrumbs.'<a href="">Towns</a>';
    	$this->view->townTitle = 'Towns';
    	$phql = 'SELECT * FROM Tbltowns';
        $result = $this->modelsManager->executeQuery($phql);

        $townArray = array();
        foreach ($result as $key => $value) {
            $townArray[] = array(
                'townID'=>$value->townID,
                'townName'=>$value->townName,
                'townLat'=>$value->townLat,
                'townLong'=>$value->townLong,
                'townInfo'=>$this->_truncateHtml(trim($value->townInfo), 150),
                'townLink'=>'/towns/information/'.$value->townID,
                'townEventLink'=>'/towns/events/'.$value->townID,
                'townGalleryLink'=>'/towns/gallery/'.$value->townID,
            );
        }
        $this->view->townMarkers = $townArray;
    }

    private function _townTab($selected = 1, $townID){
    	$tabHtml = '<ul class="nav nav-tabs">';
    	$tabHtml .= $selected == 1?'<li class="active">':'<li>';
    	$tabHtml .= '<a href="/towns/information/'.$townID.'">Information</a></li>';
    	$tabHtml .= $selected == 2?'<li class="active">':'<li>';
	    $tabHtml .= '<a href="/towns/events/'.$townID.'">Current Events</a></li>';
	    $tabHtml .= $selected == 3?'<li class="active">':'<li>';
	    $tabHtml .= '<a href="/towns/gallery/'.$townID.'">Gallery</a></li>';
	    $tabHtml .= $selected == 4?'<li class="active">':'<li>';
	    $tabHtml .= '<a href="/towns/forums/'.$townID.'">Town Forums</a></li>';
	    $tabHtml .= $selected == 5?'<li class="active">':'<li>';
	    $tabHtml .= '<a href="/towns/partners/'.$townID.'">ABK Partners</a></li>';
    	$tabHtml .= '</ul>';
    	return $tabHtml;
    }

    public function informationAction($townID){
    	$this->view->bread_crumbs = $this->breadCrumbs.'<a href="/towns">Towns</a> > Information';
    	$this->view->townTab = $this->_townTab(1, $townID);
    	$town = Tbltowns::findFirst('townID = '.$townID);
    	$this->view->townTitle = $town->townName;
    	$this->view->townInfo = $town->townInfo;
    }

    public function eventsAction($townID, $eventID = NULL){
    	$this->view->bread_crumbs = $this->breadCrumbs.'<a href="/towns">Towns</a> > Events';
    	$this->view->townTab = $this->_townTab(2, $townID);
    	$town = Tbltowns::findFirst('townID = '.$townID);
    	$this->view->townTitle = $town->townName;
        $this->view->aydi=$townID;

    	if(!empty($eventID)){
    		$this->view->list = false;
    		$this->view->event = Tbltownevents::findFirst('eventID = '.$eventID);
    	}else{
    		$this->view->list = true;
	        $numberPage = $this->request->getQuery("page", "int");
			$numberPage = empty($numberPage)?1:$numberPage;

			$phql = 'SELECT * FROM Tbltownevents WHERE townID='.$townID.' ORDER BY eventDate DESC';
			$result = $this->modelsManager->executeQuery($phql);

			$dataArray = array();
			foreach ($result as $key => $value) {            
			    $dataArray[] = array(
			        'townID'=>$value->townID,
			        'eventID'=>$value->eventID,
			        'eventTitle'=>$value->eventTitle,
			        'eventDate'=>date("F j, Y", $value->eventDate),
			        'eventVenue'=>$value->eventVenue,
			        'eventDetails'=>$this->_truncateHtml($value->eventDetails)
			        );
			}


			$paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
			    "data" => $dataArray,
			    "limit"=> 10,
			    "page" => $numberPage
			    ));
			$this->view->page = $paginator->getPaginate();
		}
    }

    public function galleryAction($townID, $galleryID = NULL){
        $this->view->townID = $townID;
    	
    	$this->view->townTab = $this->_townTab(3, $townID);
    	$town = Tbltowns::findFirst('townID = '.$townID);
    	$this->view->townTitle = $town->townName;

        if(is_null($galleryID)){
            $this->view->bread_crumbs = $this->breadCrumbs.'<a href="'.$this->url->get().'towns">Towns</a> > Gallery';
        }else{
            $this->view->albumInfo = $albumInfo = Tbltownalbums::findFirst('albumID='.$galleryID);
            $this->view->bread_crumbs = $this->breadCrumbs.'<a href="'.$this->url->get().'towns">Towns</a> > <a href="'.$this->url->get().'towns/'.$townID.'">Gallery</a> > '.$albumInfo->albumName;
        }

        if(!is_null($galleryID)){
            $this->view->galleryID = $galleryID;
            
            $this->view->list = false;

            $builder = $this->modelsManager->createBuilder()
            ->columns('pictureFilename,pictureCaption')
            ->from('Tbltownpictures')->where('albumID = :galleryID:', array('galleryID' => $galleryID));

            $paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
                "builder" => $builder,
                "limit"=> 15,
                "page" => $numberPage
                ));
            $this->view->page = $paginator->getPaginate();

        }else{
            $this->view->list = true;
            $phql = 'SELECT Tbltownalbums.albumID,
            Tbltownalbums.albumName,
            Tbltownalbums.dateCreated,
            Tbltownpictures.pictureFilename
            FROM
            Tbltownalbums
            LEFT JOIN Tbltownpictures ON Tbltownpictures.pictureID = Tbltownalbums.coverID
            WHERE Tbltownalbums.townID = '.$townID;
            $result = $this->modelsManager->executeQuery($phql);

            $dataArray = array();
            foreach ($result as $key => $value) {  
                $cover = null;          
                if(!empty($value->pictureFilename)){
                    $cover = $value->pictureFilename;
                }else{
                    $lastPic = $this->modelsManager->executeQuery("SELECT pictureFilename FROM Tbltownpictures WHERE albumID = ".$value->albumID." ORDER BY dateUploaded DESC LIMIT 1");
                    $cover = !empty($lastPic[0])?$lastPic[0]->pictureFilename:NULL;
                }

                if(!is_null($cover)){
                    $dataArray[] = array(
                        'albumID'=>$value->albumID,
                        'albumName'=>$value->albumName,
                        'dateCreated'=>date("m-d-Y", $value->dateCreated),
                        'coverFileName'=>$this->url->get().'img/towns/'.$townID.'/'.$value->albumName.'/'.$cover,
                        'albumURL' => $this->url->get().'towns/gallery/'.$townID.'/'.$value->albumID
                    );
                }
            }


            $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
            "data" => $dataArray,
            "limit"=> 15,
            "page" => $numberPage
            ));
            $this->view->page = $paginator->getPaginate();
        }
        
    }

    public function partnersAction($townID){
    	$this->view->bread_crumbs = $this->breadCrumbs.'<a href="/towns">Towns</a> > ABK Partners';
    	$this->view->townTab = $this->_townTab(5, $townID);
    	$town = Tbltowns::findFirst('townID = '.$townID);
    	$this->view->townTitle = $town->townName;

        $phql = 'SELECT * FROM Tblpartners ORDER BY partnerName';
            $result = $this->modelsManager->executeQuery($phql);

            $dataArray = array();
            foreach ($result as $key => $value) {            
                $dataArray[] = array(
                    'partnerID'=>$value->partnerID,
                    'partnerName'=>$value->partnerName,
                    'partnerInfo'=>$this->_truncateHtml($value->partnerInfo)
                    );
            }


            $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
                "data" => $dataArray,
                "limit"=> 10,
                "page" => $numberPage
                ));
            $this->view->page = $paginator->getPaginate();
    }
}