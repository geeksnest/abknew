<?php

use Phalcon\Forms\Form,
Phalcon\Forms\Element\TextArea,
Phalcon\Forms\Element\Text,
Phalcon\Forms\Element\Hidden,
Phalcon\Forms\Element\Password,
Phalcon\Forms\Element\Submit,
Phalcon\Forms\Element\Check,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\Email,
Phalcon\Validation\Validator\Identical,
Phalcon\Validation\Validator\StringLength,
Phalcon\Validation\Validator\Confirmation;

class TownForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        $townName = new Text('townName', array('class' => 'form-control', 'placeholder' => 'Town Name'));
        $townName->setLabel('Name');
        $townName->addFilter('trim');
        $townName->addValidators(array(
            new PresenceOf(array(
                'message' => 'The town name is required'
                ))
            ));
        $this->add($townName);

        $townLat = new Text('townLat', array('id'=>'us2-lat', 'class' => 'form-control', 'placeholder' => 'Latitude', 'readonly'=>'readonly', 'value'=>'14.594461297930993'));
        $townLat->setLabel('Latitude');
        $townLat->addFilter('trim');
        $townLat->addValidators(array(
            new PresenceOf(array(
                'message' => 'The town latitude is required'
                ))
            ));
        $this->add($townLat);

        $townLong = new Text('townLong', array('id'=>'us2-lon', 'class' => 'form-control', 'placeholder' => 'Longtitude', 'readonly'=>'readonly', 'value'=>'120.99428721289064'));
        $townLong->setLabel('Longtitude');
        $townLong->addFilter('trim');
        $townLong->addValidators(array(
            new PresenceOf(array(
                'message' => 'The town longtitude is required'
                ))
            ));
        $this->add($townLong);

        //Content
        $townInfo = new TextArea('townInfo', array('class' => 'form-control newsPostContent','id'=>'townInfo'));
        $townInfo->setLabel('Town Information');
        $townInfo->addFilter('trim');
        $townInfo->addValidators(array(
            new PresenceOf(array(
                'message' => 'The town information is required'
                ))
            ));
        $this->add($townInfo);

        //CSRF
        $csrf = new Hidden('csrf');

        /*$csrf->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
            )));*/
    
        $csrf->addValidator(new Identical(array(
            $this->security->checkToken() => 1,
            'message' => 'CSRF-token validation failed'
        )));

        $this->add($csrf);        

    }
    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }    
}