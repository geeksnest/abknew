<?php

use Phalcon\Events\Event,
	Phalcon\Mvc\User\Plugin,
	Phalcon\Mvc\Dispatcher,
	Phalcon\Acl;

/**
 * Security
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class Security extends Plugin
{

	public function __construct($dependencyInjector)
	{
		$this->_dependencyInjector = $dependencyInjector;
	}

	public function getAcl($resources)
	{
		//if (!isset($this->persistent->acl)) {

			$acl = new Phalcon\Acl\Adapter\Memory();

			$acl->setDefaultAction(Phalcon\Acl::DENY);

			//Register roles
			$roles = array(
				'users' => new Phalcon\Acl\Role('Users'),
				'guests' => new Phalcon\Acl\Role('Guests')
			);
			foreach ($roles as $role) {
				$acl->addRole($role);
			}

			$resources[] = 'index';
			$resources[] = 'editprofile';
			//Private area resources
			$privateResources = array(
				'admin' => $resources
			);
			foreach ($privateResources as $resource => $actions) {
				$acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
			}

			//Public area resources
			$publicResources = array(
				'admin' => array('login', 'logout', 'forgotpassword', 'resetpassword', 'settings','other','editother','editcontact','help','abkrescuesettings','abkrescreateevents','abkresevents',
					'abkreseditevents','abkresaboutus','abkresactivities','abkreseditactivity','abkrescueteam'),
				'index' => array('index', 'submitEmailNewsLetter', 'logout'),
				'site' => array('index', 'pages', 'acivateAccount', 'signup', 'finalsignup'),
				'towns'=> array('index', 'information', 'events', 'gallery', 'forums', 'partners'),
				'programs' => array('index','page'),
				'post' => array('index', 'news'),
				'announcements' => array('index', 'view'),
				'partners' => array('index', 'view', 'currentevents', 'viewevent', 'pictures', 'showpictures'),
				'contactus' => array('index'),
				'search' => array('index'),
				'donate' => array('index', 'paypalipn', 'canceled', 'success'),
				'about' => array('index'),
				'myaccount'=>array('index', 'editprofile', 'changepass', 'forgotpassword', 'resetpassword'),
				'abkrescue' => array('index'),
			);
			foreach ($publicResources as $resource => $actions) {
				$acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
			}

			//Grant access to public areas to both users and guests
			foreach ($roles as $role) {
				foreach ($publicResources as $resource => $actions) {
					$acl->allow($role->getName(), $resource, $actions);
				}
			}

			//Grant acess to private area to role Users
			foreach ($privateResources as $resource => $actions) {
				foreach ($actions as $action){
					$acl->allow('Users', $resource, $action);
				}
			}

			//The acl is stored in session, APC would be useful here too
			$this->persistent->acl = $acl;
		//}

		return $this->persistent->acl;
	}

	/**
	 * This action is executed before execute any action in the application
	 */
	public function beforeDispatch(Event $event, Dispatcher $dispatcher)
	{

		$auth = $this->session->get('auth');
		$roles = $this->session->get('roles');
		if (!$auth){
			$role = 'Guests';
		} else {
			$role = 'Users';
		}
		$controller = $dispatcher->getControllerName();
		$action = $dispatcher->getActionName();

		$acl = $this->getAcl($roles['page']);

		$allowed = $acl->isAllowed($role, $controller, $action);

		if ($allowed != Acl::ALLOW) {
			if($dispatcher->getControllerName() == "admin"){
				$dispatcher->forward(
					array(
						'controller' => 'admin',
						'action' => 'login'
					)
				);
			} else if($dispatcher->getControllerName() == "forum"){
				// do nothing
			} else {
				$this->response->redirect('/');
			}
			return false;
		}

	}

}
