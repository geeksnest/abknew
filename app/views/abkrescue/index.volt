<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Title and other stuffs -->
  <title> ABK RESCUE </title>

  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

  <!-- <link rel="stylesheet" href="css/normalize.css"> -->
  <!-- <link rel="stylesheet" href="css/main.css"> -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">

  <!--  <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/2.6.1/less.min.js"></script> -->
  <link rel="stylesheet" type="text/less" href="/css/bootstrap-less/less/style.less">
  <script src="/js/vendor/less.min1.js"></script>
  <style type="text/css">
  #myModalEvent{
    top:50px;
  }
  </style>
</head>
<body data-spy="scroll" data-offset="50">
  <!--[if lt IE 8]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

  <!-- Add your site or application content here -->
  <!--  <p>Hello world! This is HTML5 Boilerplate.</p> -->

  <!-- Header Starts -->
  <header class="top-bg open-sans">
    <div class="head-container">

      <div class="col-sm-3" id="header-copyright">
        <span class="size12 text-justify">
          A Program of AngBayanKo Foundation, <br>
          a US 501(c)(3) approved non-profit organization
        </span>
      </div>

      <div class="col-sm-4" id="header-details">
        <div class="head-details">
          <i class="ic-world size20"> </i>
          <span class="dropdown-toggle size16 open-sans" type="button" data-toggle="dropdown">U.S
            <label class=""></label></span>
            <!-- <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">HTML</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">CSS</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">JavaScript</a></li>
              <li role="presentation" class="divider"></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#aboutus">About Us</a></li>
            </ul> -->
            <div id="detail-email">
              <i class="ic-telephone size18"> </i>
              <span class=""> <?php echo $hello['phone']; ?> <!-- 626.205.3838 --> </span>
            </div>
            <div id="detail-email">
              <i class="ic-envelope size18"> </i>
              <span> <?php echo $hello['email']; ?> <!-- cert@abkrescue.com  --> </span>
            </div>

          </div>
        </div>

        <div class="col-sm-3" id="header-search">
          <div class="form-group has-feedback">
            <input type="text" class="form-control head-search" placeholder="search">
            <span class="glyphicon glyphicon-search form-control-feedback search-icon size18"></span>
          </div>
        </div>

        <div class="col-sm-2 text-center" id="header-icons">
          <div class="header-icons">
            <?php foreach($socialmedia as $soc){
              ?>
              <a href="<?php echo $soc['link']; ?>" class="btn btn-rounded btn btn-icon">
                <i class="ic-<?php echo $soc['icon']; ?> size25 social-icon"></i></a>
                <!--  <a href="#" class="btn btn-rounded btn btn-icon">
                <i class="ic-twitter size25 social-icon"></i></a>
                <a href="#" class="btn btn-rounded btn btn-icon">
                <i class="ic-instagram size25 social-icon"></i></a>
                <a href="#" class="btn btn-rounded btn btn-icon">
                <i class="ic-linkedin size25 social-icon"></i></a> -->
                <?php } ?>
              </div>
            </div>
          </div>
        </header>
        <!-- Header Ends -->

        <!-- START NAV auth.AC -->
        <!-- Use class navbar-static-top for sticky navigation -->
        <nav class="navbar navbar-default nav-container navbar-static-top" role="navigation" id="nav" data-spy="affix" data-offset-top="200">
          <div class="container-fluid">
            <div class="navbar-header col-sm-4" id="nav-logo">
              <a href="#" class="brand pull-left">
                <div id="nav-abklogo"><img src="/img/resource/abk-foundation-logo.png" class="square" alt="abk-foundation-logo"></div>
                <div id="nav-abkreslogo">
                  <img src="/img/resource/abkrescue-logo.png" alt="abkrescue-logo"></div>
                  <div id="nav-abkcertlogo">
                    <img src="/img/resource/cert-logo.png" alt="abkrescue-logo"></div>
                  </a>
                </div>
                <!-- Put this for the toggle navigation on responsive -->
                <div class="navbar-header container" id="nav-toggle">
                  <button type="button" data-target="#navbarCollapse" data-toggle="offcanvas" class="navbar-toggle
                  offcanvas-toggle pull-right">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand size18">ABKRESCUE</a>
              </div>
              <!-- Put this for the toggle navigation on responsive -->

              <div class="col-sm-7" id="nav-text">
                <div id="navbarCollapse" class="navbar-offcanvas navbar-offcanvas-touch nopadleft">
                  <ul class="nav navbar-nav open-sans">
                    <li><a href="#" class="active">TAKE ACTION</a></li>
                    <li><a href="#aboutus">ABOUT US</a></li>
                    <li><a href="#">BLOG</a></li>
                    <li><a href="#volunteer">VOLUNTEER</a></li>
                    <li><a href="#donate">DONATE</a></li>
                  </ul>
                </div>
              </div>

              <div class="navbar-header col-sm-1" id="nav-cert">
                <a href="#" class="brand pull-right">
                  <span><img src="/img/resource/cert-logo.png" alt="cert-logo"></span>
                </a>
              </div>
            </div>
          </nav>
          <hr class="nav-hr"/>
          <!-- END NAV auth.AC -->

          <!-- START HEADLINE auth.AC -->
          <div class="headline-container">
            <!-- <a href="#" class="content-box"> -->
            <div class="col-sm-4 height100 nopad" id="frame-lg">
              <?php foreach($featured as $featured){
                if($featured['featured'] == '1'){
                  ?>
                  <div class="frame-lg" style="background-image: url('<?php echo $featured['banner']; ?>');">
                    <div class="content-container featured-container">
                      <a href="">
                        <div class="content-title"><strong><?php echo $featured['title']; ?></strong></div>
                        <div class="content">
                          <span>
                            <?php
                            if (strlen($featured['description']) > 70)
                            {
                              ?>
                              <a href="#" data-toggle="modal" data-target="#myModalEvent"
                              onclick="loadModalEvent(
                              '<?=trimTtxt($featured['description'])?>',
                              '<?=trimTtxt($featured['title'])?>' ,
                              '<?=$featured['banner']?>')"> <?=substr($featured['description'], 0 , 70); ?> ... </a>
                              <?php } else {
                                echo $featured['description'];
                              }
                              ?>
                            </span>
                          </div>
                        </a>
                      </div>
                    </div>
                    <?php break; } }?>
                  </div>


                  <!-- </a> -->
                  <div class="col-sm-8 height100 nopad" id="frame-md">
                    <?php $i = 0; foreach($activities as $activities){
                      ?>
                      <!-- <a href="#" class="content-box"> -->
                      <div class="col-sm-6 height50 activitypad <?php echo ($i == 2 || $i == 3 ? 'nobot' : '') ;?> ">
                        <div class="frame-md" style="background-image: url('<?php echo $activities['banner']; ?>');">
                          <div class="content-container <?php echo ($i == 0 || $i == 1 ? 'absolutebot' : '') ;?> other-container">
                            <a>
                              <div class="content-title"><strong><?php echo $activities['title']; ?></strong></div>
                              <div class="content">
                                <span>
                                <a href="#" data-toggle="modal" data-target="#myModalEvent"
                                    onclick="loadModalEvent(
                                    '<?=trimTtxt($activities['description'])?>',
                                    '<?=trimTtxt($activities['title'])?>' ,
                                    '<?=$activities['banner']?>')">
                                  <?php
                                  if (strlen($activities['description']) > 70)
                                  { ?>
                                     <?=substr($activities['description'], 0 , 70)?> ... 
                                    <?php } else {
                                      echo $activities['description'];
                                    }
                                    ?>
                                 </a>
                                  </span>
                                </div>

                              </a>
                            </div>
                          </div>
                        </div>
                        <?php $i++; if($i==4) break; }?>
                        <!-- </a> -->
                        <!-- <a href="#" class="content-box"> -->
                        <!--  <div class="col-sm-6 height50 nopad">
                        <div class="frame-md" style="background-image: url('/img/resource/img-3-425x200.png');">
                        <div class="content-container">
                        <a href="">
                        <div class="content-title"><strong>LOREM IPSUM</strong></div>
                        <div class="content"><span>dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
                      </a>
                    </div>
                  </div>
                </div> -->
                <!-- </a> -->
                <!--  <a href="#" class="content-box"> -->
                <!-- <div class="col-sm-6 height50 nopad">
                <div class="frame-md" style="background-image: url('/img/resource/img-2-425x200.png');">
                <div class="content-container">
                <a href="">
                <div class="content-title"><strong>LOREM IPSUM</strong></div>
                <div class="content"><span>dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
              </a>
            </div>
          </div>
        </div> -->
        <!-- </a> -->
        <!-- <a href="#" class="content-box"> -->
        <!-- <div class="col-sm-6 height50 nopad">
        <div class="frame-md" style="background-image: url('/img/resource/img-3-425x200.png');">
        <div class="content-container">
        <a href="">
        <div class="content-title"><strong>LOREM IPSUM</strong></div>
        <div class="content"><span>dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span></div>
      </a>
    </div>
  </div>
</div> -->
<!-- </a> -->
<div id="donate"></div>
</div>
</div>
<!-- END HEADLINE auth.AC -->
<!-- START CALL-TO-ACTION auth.AC -->
<div id="aboutus"></div>
<div class="cta-container center-block" style="background-image: url('/img/resource/bg-cta.png');">
  <div class="cta-box">
    <div class="text-container col-sm-8">
      <div class="text-title"><p>Lorem Ipsum</p></div>
      <div class="text"><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</span></div>
    </div>
    <div class="button-container col-sm-4">
      <div class="col-sm-12 open-sans">
        <div class="form-group col-sm-6">
          <button class="form-control donate-submit">
            <span class="size20"> DONATE </span>
          </button>
        </div>

        <div class="form-group col-sm-6">
          <button class="form-control volunteer-submit">
            <span class="size20"> VOLUNTEER </span>
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END CALL-TO-ACTION auth.AC -->

<!-- START ABOUT US auth.AC -->
<div class="about-container">
  <div class="about-body">
    <article class="container">
      <hr class="about-hr"/>
      <h1 class="open-sans-bold text-center"> ABOUT US
      </h1>
      <?php echo $aboutus['description'];  ?>
      <!-- <p class="size16 open-sans">
      Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur.
    </p>
    <p class="size16 open-sans">
    adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur
  </p> -->
<div id="volunteer"></div>
</article>
</div>
</div>
<!-- END ABOUT US auth.AC -->

<!-- Join the team Starts -->
<div class="join-container">
  <div class="join-bg" style="background-image: url('/img/resource/bg-join.png');">
  </div>
  <div class="join-body">
    <article class="col-md-12 text-center height100">
      <hr class="join-hr"/>
      <h1 class="open-sans-bold"> JOIN THE TEAM </h1>
      <p class="col-sm-10 col-sm-offset-1 open-sans size18 mgtop20">
        There are many ways to contribute to our mission in helping to serve to the community. Fill out the form below and your volunteer coordinator will be in touch.
      </p>
      <div class="col-sm-12"><?php echo $this->getContent(); echo $passError; echo $successmsg;?></div>
      <div class="join-form center-block">
        {{ form('abkrescue/', 'class': 'form-inline height100', 'novalidate', "id": "joinForm") }}
        <div class="col-sm-12 join-bot">
        <div class="join-block">
          <div class="form-group">
            {{ text_field('fname', 'name':'fname' , 'placeholder':'Firstname *', 'class':'form-control') }}
           <?php echo $msg['fname']; ?>
          </div>
        </div>
          <div class="join-block">
          <div class="form-group">
            {{ text_field('lname', 'name':'lname' , 'placeholder':'Lastname *', 'class':'form-control') }}
            <?php echo $msg['lname']; ?>
          </div>
          </div>
        <div class="join-block">
          <div class="form-group">
          <input id="email" name="email" type="email" class="form-control" placeholder="Email Address *"novalidate />
          <?php echo $msg['email']; ?>
          </div>
        </div>
        </div>

        <div class="col-sm-12 join-bot">
        <div class="join-block">
            <div class="form-group">
            <?php echo $form->render("address", array('class' => 'form-control', 'placeholder' => 'Address *')); ?>
            <?php echo $msg['address']; ?>
          </div>
        </div>
        <div class="join-block">
            <div class="form-group">
                {{ text_field('city', 'name':'city' , 'placeholder':'City *', 'class':'form-control') }}
                <?php echo $msg['city']; ?>
            </div>
        </div>

          <div class="join-block">
            <div class="form-group">
            <select name="country" class="form-control" id="country">
               <option value=""> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Country * </option>
                <?php foreach($country as $country){ ?>
                    <option value="<?php echo $country; ?>"> <?php echo $country; ?></option>
                <?php } ?>
            </select>
            <?php echo $msg['country']; ?>
          </div>
          </div>
        </div>

        <div class="col-sm-12 join-bot">
          <div class="join-block">
          <div class="form-group">
            {{ text_field('state', 'name':'state' , 'placeholder':'State *', 'class':'form-control') }}
            <?php echo $msg['state']; ?>
          </div>
          </div>
          <div class="join-block">
          <div class="form-group">
            {{ text_field('zip', 'name':'zip' , 'placeholder':'Zip *', 'class':'form-control', 'onkeypress':'return event.charCode >= 48 && event.charCode <= 57', 'maxlength':'5') }}
            <?php echo $msg['zip']; ?>
          </div>
          </div>
          <div class="join-block">
          <div class="form-group">
          <input id="" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  name="contact" type="text" class="form-control" maxlength="16" placeholder="Contact # *" />
          <?php echo $msg['contact']; ?>
          </div>
          </div>
        </div>

        <div class="col-sm-12 open-sans">
          <div class="form-group">
            {{ submit_button("join_team", 'class':'form-control join-submit size20', 'value':'GET INVOLVE', 'name':'join_team') }}
            <!-- <button class="form-control join-submit">
            <span class="size20">GET INVOLVE</span>
          </button> -->
        </div>
      </div>
      {{ end_form() }}

  </div>
</article>
</div>

</div>
<!-- Join the team Ends -->

<!-- Count Starts -->
<div class="count-container">
  <div class="count-bg" style="background-image: url('/img/resource/bg-s.png');"></div>
  <div class="count-body">
    <div class="container mgtop20 open-sans">
      <div class="col-sm-4 text-left">
        <span> 5,648 </span>
        <p class="size20"> GLOBAL MEMBERS </p>
      </div>
      <div class="col-sm-4 text-center">
        <span> 301 </span>
        <p class="size20"> ACCOMPLISH MISSIONS </p>
      </div>
      <div class="col-sm-4 text-right">
        <span> 431,123 </span>
        <p class="size20"> WORTH OF DONATIONS </p>
      </div>
    </div>
  </div>
</div>
<!-- Count Starts -->

<!-- Article Starts -->
<div class="article-container">
  <article class="container height100">
    <h1 class="open-sans"> AN EXPERT IN EMERGENCY RESPONSE </h1>
    <p class="size16 open-sans">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    </p>
    <hr align="left" />

    <section class="text-justify size14 open-sans">
      <div class="sect-body">
        <span class="open-sans">
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
        </span>
        <span>
          sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat.
        </span>
        <span>
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit. sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.
        </span>
      </div>
      <div class="sect-button col-sm-12 text-right">
        <a href=""> <i class="fa fa-play text-success"></i>
          <span class="open-sans-bold size16">Read More</span>
        </a>

      </div>

    </section>

  </article>
</div>
<!-- Article Ends -->

<!-- Event Starts -->
<div class="event-container">
  <section class="event-left">
    <div class="event-bg" style="background-image:url('/img/resource/bg-ue-fill.png');">
      <div class="event-content">
        <hr class="event-hr" />
        <h1 class="open-sans-bold text-center"> UPCOMING EVENTS </h1>

        <div class="event-desc">
          <?php foreach($events as $events){
            ?>
            <div class="col-sm-12 nopadleft">
              <div class="col-sm-5 nopadleft">
                <label class="event-num open-sans"> <?php echo date('d', strtotime($events['startdate'])); ?> </label>
                <label class="event-date open-sans"> <b class="open-sans-bold"> <?php echo date('F', strtotime($events['startdate'])); ?> </b> <span class="size21"> <?php echo date('l', strtotime($events['startdate'])); ?> </span> </label>
              </div>
              <div class="col-sm-7 event-text open-sans">
                <h3 class="open-sans-bold" style="word-break:break-all"> <?php echo $events['eventname']; ?>  </h3>
                <p class="size14">
                  <?php if (strlen($events['shortdesc']) > 30) {
                    echo substr($events['shortdesc'], 0 , 30)."...";
                  }  else {
                    echo $events['shortdesc'];
                  }?>
                </p>
                <span class="pull-left size10">Location: <i> <?php echo $events['location']; ?> </i> </span>
                <span class="pull-right size10">Time: <i><?php echo date('h:i a', strtotime($events['starttime'])); ?></i> </span>
              </div>
              <div class="col-sm-12 mgtop5 mgbot5">
                <hr />
                <a class="sect-button" data-toggle="modal" data-target="#myModalActivities"
                onclick="loadModalActivities(
                '<?=trimTtxt($events['shortdesc'])?>',
                '<?=trimTtxt($events['eventname'])?>',
                '<?=date('h:i a', strtotime($events['starttime']))?>',
                '<?=date('l d, F Y', strtotime($events['startdate']))?>',
                '<?=$events['location']?>',
                '<?=date('h:i a', strtotime($events['endtime']))?>',
                '<?=date('l d, F Y', strtotime($events['enddate']))?>')">
                <i class="fa fa-play text-success"></i>
                <span class="open-sans-bold size16">Details</span>
              </a>
            </div>
          </div>
          <?php } ?>

          <!--                             <div class="col-sm-12 nopadleft">
          <div class="col-sm-5 nopadleft">
          <label class="event-num open-sans"> 12 </label>
          <label class="event-date open-sans"> <b class="open-sans-bold"> JUNE </b> <span class="size21"> SUNDAY </span> </label>
        </div>
        <div class="col-sm-7 event-text open-sans">
        <h3 class="open-sans-bold"> Lorem Ipsum  </h3>
        <p class="size14">dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
        <span class="pull-left size10">Location: <i> Some location</i> </span>
        <span class="pull-right size10">Time: <i>Some time</i> </span>
      </div>
      <div class="col-sm-12 mgtop5 mgbot5">
      <hr />
      <a href="" class="sect-button"> <i class="fa fa-play text-success"></i>
      <span class="open-sans-bold size16">Details</span>
    </a>
  </div>
</div>

<div class="col-sm-12 nopadleft">
<div class="col-sm-5 nopadleft">
<label class="event-num open-sans"> 12 </label>
<label class="event-date open-sans"> <b class="open-sans-bold"> JUNE </b> <span class="size21"> SUNDAY </span> </label>
</div>
<div class="col-sm-7 event-text open-sans">
<h3 class="open-sans-bold"> Lorem Ipsum  </h3>
<p class="size14">dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
<span class="pull-left size10">Location: <i> Some location</i> </span>
<span class="pull-right size10">Time: <i>Some time</i> </span>
</div>
<div class="col-sm-12 mgtop5 mgbot5">
<hr />
<a href="" class="sect-button"> <i class="fa fa-play text-success"></i>
<span class="open-sans-bold size16">Details</span>
</a>
</div>
</div> -->

</div>

</div>
</div>

</section>
<section class="event-right">
  <div class="event-right-bg"
  style="background-image:url('/img/resource/img-1-545-800.png');">
</div>
</section>
</div>
<!-- Event Ends -->


<!-- Footer Starts -->
<footer>
  <div class="container footer-container">

    <div class="col-sm-12 nopadleft footer-logo">
      <div class="col-sm-1 abklogo">
        <span class="helper"></span>
        <img src="/img/resource/abk-foundation-logo.png" class="foot-img" />
      </div>
      <div class="col-sm-3 nopadleft height100" id="abkrescue-logo">
        <span class="helper"></span>
        <img src="/img/resource/abkrescue-logo.png" class="foot-img" />
      </div>
      <div class="col-sm-2 nopadleft height100 text-center" id="cert-logo">
        <span class="helper"></span>
        <img src="/img/resource/cert-logo.png" />
      </div>
      <div class="col-sm-5 height100 nopadright" id="subscribe-logo">
        <div class="footer-subscribe">
          <span class="helper"></span>
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Email" aria-describedby="basic-addon2">
            <span class="input-group-btn lato-regular">
              <button class="btn btn-lg btn-secondary" type="button">SUBSCRIBE</button>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-12 footer-text">
      <div class="col-sm-3">
        <h5 class="open-sans-bold"> get to know us </h5>
        <ul class="open-sans size15">
          <li> read our blog </li>
          <li> field stories </li>
          <li> meet our stories </li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5 class="open-sans-bold"> get to know us </h5>
        <ul class="open-sans size15">
          <li> read our blog </li>
          <li> field stories </li>
          <li> meet our stories </li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5 class="open-sans-bold"> get to know us </h5>
        <ul class="open-sans size15">
          <li> read our blog </li>
          <li> field stories </li>
          <li> meet our stories </li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5 class="open-sans-bold"> get to know us </h5>
        <ul class="open-sans size15">
          <li> read our blog </li>
          <li> field stories </li>
          <li> meet our stories </li>
        </ul>
      </div>
    </div>
    <div class="col-sm-12 text-center open-sans">
      <hr class="footer-hr" />
      <div class="footer-icon-container ">
        <p> A Program of AngBayanKo Foundation, a US 501(c)(3) approved non-profit organization</p>
        <p>privacy policy   •   © 2016   •  <a href=""> get our awesome emails </a> </p>
        <div class="footer-icons">
        <?php foreach($socialmedia as $soc){
              ?>
              <a href="<?php echo $soc['link']; ?>" class="btn btn-icon">
                <i class="ic-<?php echo $soc['icon']; ?>"></i></a>
        <?php } ?>
          <!-- <a href="#" class="btn btn-icon">
            <i class="ic-facebook"></i></a>
            <a href="#" class="btn btn-icon">
              <i class="ic-twitter"></i></a>
              <a href="#" class="btn btn-icon">
                <i class="ic-instagram"></i></a>
                <a href="#" class="btn btn-icon">
                  <i class="ic-linkedin"></i></a> -->
                </div>
              </div>
            </div>

          </div>
        </footer>


        <!-- Modal Event-->
        <div class="modal fade" id="myModalEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span id="titleTxt" class="size20" style="color: #10a800"></span></h4>
              </div>

              <div class="modal-body">
                <div class="row">
                  <div class="col-sm-12">
                    <center><div id="imgEvt"></div></center>
                  </div>
                  <div class="col-sm-12">
                    <span id="descTxt" style="color: #1d1d1d"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <!-- Modal Activities-->
        <div class="modal fade" id="myModalActivities" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span id="titleTxtac" style="color: #10a800; font-weight:bolder;"></span></h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-sm-12">
                   <label> Date Start: </label>  <span id="dateac"></span> <br>
                   <label> Date End: </label> &nbsp; <span id="enddate"></span>
                  </div>
                  <div class="col-sm-12">
                   <label> Time Start: </label>  <span id="timeac"></span>
                    <br>
                   <label> Time End: </label> &nbsp; <span id="endtime"></span>
                  </div>
                  <div class="col-sm-12">
                  <br>
                    <span id="descTxtac" style="color: #1d1d1d"></span>
                  </div>
                  <div class="col-sm-12" style="margin-top: 10px">
                    location:  <i><span id="locac" style="color: #1d1d1d"></span></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Footer Ends -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <!--<script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="js/vendor/jquery-1.12.0.min.js"></script>-->
        {{ javascript_include('js/vendor/jquery-1.12.0.min.js') }}
        {{ javascript_include('js/bootstrap.js') }}
        {{ javascript_include('js/join-validation.js') }}
        {{ javascript_include('js/vendor/bootstrap.offcanvas.min.js') }}

        <script type="text/javascript">
        $('#phone-number')
        .keydown(function (e) {
          var key = e.charCode || e.keyCode || 0;
          $phone = $(this);

          // Auto-format- do not expose the mask as the user begins to type
          if (key !== 8 && key !== 9) {
            if ($phone.val().length === 4) {
              $phone.val($phone.val() + ')');
            }
            if ($phone.val().length === 5) {
              $phone.val($phone.val() + ' ');
            }
            if ($phone.val().length === 9) {
              $phone.val($phone.val() + '-');
            }
          }

          // Allow numeric (and tab, backspace, delete) keys only
          return (key == 8 ||
            key == 9 ||
            key == 46 ||
            (key >= 48 && key <= 57) ||
            (key >= 96 && key <= 105));
          })

          .bind('focus click', function () {
            $phone = $(this);

            if ($phone.val().length === 0) {
              $phone.val('(');
            }
            else {
              var val = $phone.val();
              $phone.val('').val(val); // Ensure cursor remains at the end
            }
          })

        .blur(function () {
            $phone = $(this);

            if ($phone.val() === '(') {
                $phone.val('');
            }
        });

        function loadModalEvent(desc, title, img){
          $("#imgEvt").html('<img class="img-responsive" src= "'+ img +'" alt="" />');
          $("#descTxt").html(desc);
          $('#titleTxt').html(title);
          $('#titleTxt').css({"color":"#1d1d1d","font-size":"20"});
          $('#myModalEvent').modal('show');
        }

        function loadModalActivities(desc, title,timeac,dateac, locac, endtime, enddate){
          $("#descTxtac").html(desc);
          $("#locac").html(locac);
          $('#titleTxtac').html(title);
          $('#timeac').html(timeac);
          $('#dateac').html(dateac);
          $('#endtime').html(endtime);
          $('#enddate').html(enddate);
          $('#myModalActivities').modal('show');
        }
        </script>

        <script type="text/javascript">
        $(window).scroll(function() {
          if ($(document).scrollTop() > 200) {
            $('nav').addClass('shrink');
          } else {
            $('nav').removeClass('shrink');
          }
        });

       $( window ).resize(function() {
        //console.log($(window).width());
        if($(window).width() < 753){
          // console.log("shing!")
          $('.join-block').addClass('join-block-add');
          $('.join-block').removeClass('join-block');
        }else{
          $('.join-block-add').addClass('join-block');
        }
        if($(window).innerWidth() <= 767){
          // console.log("shing!")
          $('.join-block').addClass('join-block-add');
          $('.join-block').removeClass('join-block');
        }else{
          $('.join-block-add').addClass('join-block');
        }
        
      })
       $(document).ready(function(){
           if($(window).width() < 753 || $(window).innerWidth() <= 767){
              console.log("shing!")
              $('.join-block').addClass('join-block-add');
              $('.join-block').removeClass('join-block');
          }else{
              $('.join-block-add').addClass('join-block');
          }
       });
        </script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
          function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
          e=o.createElement(i);r=o.getElementsByTagName(i)[0];
          e.src='https://www.google-analytics.com/analytics.js';
          r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
          ga('create','UA-XXXXX-X','auto');ga('send','pageview');
          </script>
          <?php
          function trimTtxt($data){
            return rtrim(str_replace("'", "", $data));
          }
          ?>
        </body>
        </html>
