<!--Modal View-->
<div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Alert!</h4>
      </div>
      <div class="modal-body">
        <p class="modal-message"></p>
        <span class="modal-list-names"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
      </div>
    </div>
  </div>
</div>

<?php $token = $this->security->getToken(); ?>
<div class="page-head">

  <h2 class="pull-left">
    <span class="page-meta">CREATE EVENT</span>
  </h2>

  <div class="bread-crumb pull-right">
    <a href="/admin"><i class="icon-home"></i> Home</a>
    <span class="divider">/</span>
    <a href="" class="bread-current">Create</a>
  </div>
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
    {{ content() }}
    {{ form('admin/abkrescreateevents', 'class': 'form-horizontal') }}
    <!--start row-->
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            Event Information
            <div class="widget-icons pull-right">
            </div>
          </div>

          <div class="widget-content">
            <div class="padd">


              <div class="form-group">
                <div class="col-lg-3">
                  <label class="">{{ form.label('eventname') }}</label>
                  <span class="asterisk">*</span>
                </div>
                <div class="col-lg-7">
                  {{ form.render('eventname') }}
                  <!--  {{ form.messages('eventname') }} -->
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-3">
                  <label class="">{{ form.label('eventslugs') }}</label>
                  {#<span class="asterisk">*</span>#}
                </div>
                <div class="col-lg-7">
                  {{ form.render('eventslugs') }}
                  <!-- {{ form.messages('eventname') }} -->
                </div>
              </div>

              <div class="form-group">
                <div class="col-lg-3">
                  <label class="">{{ form.label('desc') }}</label>
                  <span class="asterisk">*</span>
                </div>
                <div class="col-lg-7">
                  {{ form.render('desc') }}
                  <!-- {{ form.messages('eventname') }} -->
                </div>
              </div>


              <div class="form-group">
                <div class="col-lg-3">
                  <label class="">{{ form.label('ldesc') }}</label>
                  <span class="asterisk">*</span>
                </div>
                <div class="col-lg-7">
                  {{ form.render('ldesc') }}
                  <!-- {{ form.messages('eventname') }} -->
                </div>
              </div>

              <div class="form-group">
                <div class="col-lg-3">
                  <label class="">{{ form.label('location') }}</label>
                  <span class="asterisk">*</span>
                </div>
                <div class="col-lg-7">
                  {{ form.render('location') }}
                  <!-- {{ form.messages('eventname') }} -->
                </div>
              </div>

              <div class="form-group">
                <div class="col-lg-3">
                  <label class="">{{ form.label('date') }}</label>
                  <span class="asterisk">*</span>
                </div>
              </div>

              <div class="row">
                <div class="col-md-3">
                  <span class="control-label">Starts</span>
                  <div class="input-group w-md2">
                    <span class="input-group-btn">
                      <div class="input-append pull-left datetimepicker1">
                        {{ text_field('startdate', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time','placeholder':'Start date') }}
                        <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg glyphicon glyphicon-time"></i>
                        </span>
                        <br/>
                      </div>
                    </span>
                  </div>
                </div>
                {#<div class="col-md-3">
                <br>
                <div class="input-append pull-left datetimepicker2">
                {{ text_field('starttime', 'data-format':'HH:mm PP','class':'form-control dtpicker', 'data-time-icon':'icon-time','placeholder':'Start time') }}
                <span class="add-on">
                <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg glyphicon glyphicon-time"></i>
              </span>
              <br/>
            </div>
          </div>#}

          <div class="col-md-3">
            <span class="control-label">Ends</span>
            <div class="input-group w-md2">
              <span class="input-group-btn">
                <div class="input-append pull-left datetimepicker1">
                  {{ text_field('enddate', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time','placeholder':'End date') }}
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg glyphicon glyphicon-time"></i>
                  </span>
                  <br/>
                </div>
              </span>
            </div>
          </div>
          {#<div class="col-md-3">
          <br>
          <div class="input-append pull-left datetimepicker2">
          {{ text_field('endtime', 'data-format':'HH:mm PP','class':'form-control dtpicker', 'data-time-icon':'icon-time','placeholder':'End time') }}
          <span class="add-on">
          <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg glyphicon glyphicon-time"></i>
        </span>
        <br/>
      </div>
    </div>#}

  </div>


  <br /><br />
  {{ submit_button("save_event", 'class':'btn btn-primary', 'value':'Save Event', 'name':'save_event','id':'savepassBtn' ) }}
  <input type="hidden" name="csrf"value="<?php echo $token ?>"/>


</div>
</div>
</div>
</div>


</div><!--end row-->


</form>
</div><!--end container-->
</div><!--end matter-->
