            <!-- Modal View-->
            <div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">View Record</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span>Name: </span> <label id="name"></label> <br>
                    <span>Email: </span> <label id="email"></label> <br>
                    <span>Address: </span> <label id="address"></label> <br>
                    <span>City: </span> <label id="city"></label> <br>
                    <span>Country: </span> <label id="country"></label> <br>
                    <span>State: </span> <label id="state"></label> <br>
                    <span>Zip: </span> <label id="zip"></label> <br>
                    <span>Contact: </span> <label id="contact"></label> <br>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>

             <div id="delActConf" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="ddModalTitle"></h4>
                  </div>
                  <div class="modal-body">
                    Are you sure you want to <span id="ddActionSpan"></span> Page "<strong id="activityLabelConf"></strong>" ?
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel
                    </button>
                    <button type="button" class="btn btn-danger" id="delActivitybtn">Delete</button>
                  </div>
                </div>
              </div>
            </div>

            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Abk Rescue Team </h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="/admin"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <span>Activities</span>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->

            <div class="matter">
              {{ form('admin/abkrescueteam', 'id':'main-table-form') }}
              <div class="container">

                <div class="row">
                <div class="col-sm-12">
                  <div class="col-md-2">
                    <label>Search by Text</label>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class="col-md-12">
                      {{ text_field('search_text' , 'class':'form-control') }}
                    </div>
                  </div>
                  <div class="col-md-2 form-group">
                    {{ submit_button('Search', 'class':'btn btn-default') }}
                    <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                  </div>
                </div>
                  
                </div>

                <!-- Table -->
                <div class="row">
                <div class="col-sm-12">
                  <div class="col-sm-2">
                    <label>Filter by Date</label>
                  </div>
                  <div class="col-sm-4 form-group" id="fromDatepicker" class="input-append">
                    <label class="col-sm-2 control-label">From</label>
                    <div class="col-sm-10">
                      {{ text_field('fromDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-sm-4 form-group" id="toDatepicker" class="input-append">
                    <label class="col-sm-2 control-label">To</label>
                    <div class="col-sm-10">
                      {{ text_field('toDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    {{ submit_button('Filter', 'class':'btn btn-default') }}
                  </div>
                  </div>
                </div>

                <!-- Table -->
                  {{ content() }}
                <div class="row">

                  <!-- <div class="form-group">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="control-label col-lg-3">Search</label>
                        <div class="col-lg-9"> 
                          {#{ text_field('search_text' , 'class':'form-control') }#}
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      {#{ submit_button('Search', 'class':'btn btn-default') }#}
                     <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                    </div>
                  </div>  -->

                  <div class="col-md-12">
                   
                    <div class="widget">

                      <div class="widget-head">
                        <div class="pull-left">Volunteers</div>
                        <div class="clearfix"></div>
                      </div>


                      <div class="widget-content">
                        {{ hidden_field('csrf', 'value': security.getToken())}}
                        <input type="hidden" class="tbl-action" name="action" value=""/>
                        <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                        <input type="hidden" class="tbl-edit-url" name="editurl" value="abkreseditactivity/"/>
                        <table class="table table-striped table-bordered table-hover tblusers">
                          <thead>
                            <tr>
                              <!-- <th>{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th> -->
                              <th><a href="?sort={{ titleHref }}">Name <i class="{{ titleIndicator ? titleIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ slugHref }}">Email <i class="{{ slugIndicator ? slugIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ orderHref }}"> Contact <i class="{{ orderIndicator ? orderIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ lastupdateHref }}">Date Registered <i class="{{ lastupdateIndicator ? lastupdateIndicator : "" }}"></i></a></th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                        
                          <?php foreach ($page->items as $rescue) {
                            $deleteLink = '<a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="'.$events['id'].'"><i class="icon-ok"></i> </a>';
                           ?> 
                          <tr>
                              <!-- <td>
                                '.$pageCheck.'
                              </td> -->
                              <td class="name"> <?php echo $rescue['fname'].' '.$rescue['lname']; ?> </td>
                              <td><?php echo $rescue['email']; ?></td>
                              <td><?php echo $rescue['contact']; ?></td>
                              <td><?php echo $rescue['date_created']; ?></td>
                              <td>
                               <a href="#modalView" data-toggle="modal" type="button" class="btn btn-xs btn-info view-page-sample" data-recorID="'.$rescue['id'].'" onclick="loadModalActivities('<?php echo $rescue['fname']." ". $rescue['lname']; ?>',
                               '<?php echo $rescue['email'] ?>','<?php echo $rescue['address'] ?>','<?php echo $rescue['city'] ?>','<?php echo $rescue['country'] ?>','<?php echo $rescue['state'] ?>','<?php echo $rescue['zip'] ?>','<?php echo $rescue['contact'] ?>')"><i class="icon-ok"></i></a>
                               <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $rescue['id']; ?>"><i class="icon-remove"></i> </a>
                              </td>
                            </tr>
                          <?php } 
                          if(empty($rescue)){
                            echo '<tr><td colspan="6">No records found</td></tr>';
                          }
                          ?>
                          </tbody>
                          </table>

                          <div class="tblbottomcontrol" style="display:none">
                            <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                            <a href="#" class="tbl_unselect_all"> Unselect </a>
                          </div>

                            <div class="widget-foot">
                              {% if page.total_pages > 1 %}
                             
                              <ul class="pagination pull-right">

                                {% if page.current != 1 %}
                                <li>{{ link_to("admin/abkresevents?page=" ~ page.before, 'Prev') }}</li>
                                {% endif %}

                                {% for index in 1..page.total_pages %}
                                {% if page.current == index %}
                                <li>{{ link_to("admin/abkresevents?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                {% else %}
                                <li>{{ link_to("admin/abkresevents?page=" ~ index, index) }}</li>
                                {% endif %}
                                {% endfor %}         

                                {% if page.current != page.total_pages %}                 
                                <li>{{ link_to("admin/abkresevents?page=" ~ page.next, 'Next') }}</li>
                                {% endif %}
                              </ul>
                              {% endif %}
                              
                              <div class="clearfix"></div> 

                            </div>

                          </div>

                        </div>


                      </div>

                    </div>


                  </div>
                </form>
              </div>
<script type="text/javascript">
  function loadModalActivities(name, email,address, city, country, state, zip, contact){
    console.log(name);
    $("#name").html(name);
    $("#email").html(email);
    $('#address').html(address);
    $('#city').html(city);
    $('#country').html(country);
    $('#state').html(state);
    $('#zip').html(zip);
    $('#contact').html(contact);
    //$('#contact').modal('show');
  }
</script>
<!-- Matter ends -->