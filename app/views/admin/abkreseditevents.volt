<!--Modal View-->
<div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Alert!</h4>
      </div>
      <div class="modal-body">
        <p class="modal-message"></p>
        <span class="modal-list-names"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
      </div>
    </div>
  </div>
</div>

<?php $token = $this->security->getToken(); ?>
<div class="page-head">

  <h2 class="pull-left">
    <span class="page-meta">EDIT EVENT</span>
  </h2>

  <div class="bread-crumb pull-right">
    <a href="/admin"><i class="icon-home"></i> Home</a>
    <span class="divider">/</span>
    <a href="" class="bread-current">Create</a>
  </div>
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
    {{ content() }}
    {{ form('admin/abkreseditevents/'~event.id, 'class': 'form-horizontal') }}
    <!--start row-->
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            Event Information
            <div class="widget-icons pull-right">
            </div>
          </div>

          <div class="widget-content">
            <div class="padd">


              <div class="form-group">
                <div class="col-lg-3">
                  <label class="">{{ form.label('eventname') }}</label>
                  <span class="asterisk">*</span>
                </div>
                <div class="col-lg-7">
                  <?php echo $form->render('eventname', array( 'value' => $event->eventname)); ?>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-3">
                  <label class="">{{ form.label('eventslugs') }}</label>
                  {#<span class="asterisk">*</span>#}
                </div>
                <div class="col-lg-7">
                  <?php echo $form->render('eventslugs', array( 'value' => $event->slugs)); ?>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-3">
                  <label class="">{{ form.label('desc') }}</label>
                  <span class="asterisk">*</span>
                </div>
                <div class="col-lg-7">
                  <?php echo $form->render('desc', array( 'value' => $event->shortdesc)); ?>
                </div>
              </div>

              <div class="form-group">
                <div class="col-lg-3">
                  <label class="">{{ form.label('ldesc') }}</label>
                  <span class="asterisk">*</span>
                </div>
                <div class="col-lg-7">
                  <?php echo $form->render('ldesc', array( 'value' => $event->longdesc)); ?>
                </div>
              </div>


              <div class="form-group">
                <div class="col-lg-3">
                  <label class="">{{ form.label('location') }}</label>
                  <span class="asterisk">*</span>
                </div>
                <div class="col-lg-7">
                  <?php echo $form->render('location', array( 'value' => $event->location)); ?>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-3">
                  <label class="">{{ form.label('date') }}</label>
                  <span class="asterisk">*</span>
                </div>
              </div>

              <div class="row">
                <div class="col-md-3">
                  <span class="control-label">Starts</span>
                  <div class="input-group w-md2">
                    <span class="input-group-btn">
                      <div class="input-append pull-left datetimepicker1">
                        <input type="text" name="startdate" data-format ='yyyy-MM-dd' class="form-control dtpicker" data-time-icon='icon-time' value="<?php echo $event->startdate; ?>">
                        <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg glyphicon glyphicon-time"></i>
                        </span>
                        <br/>
                      </div>
                    </span>
                  </div>
                </div>
                {#<div class="col-md-3">
                  <br>
                  <div class="input-append pull-left datetimepicker2">
                    <input type="text" name="starttime" data-format ='HH:mm PP' class="form-control dtpicker" data-time-icon='icon-time' value="<?php echo date('h:i a',strtotime($event->starttime)); ?>">
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg glyphicon glyphicon-time"></i>
                    </span>
                    <br/>
                  </div>
                </div>#}

                <div class="col-md-3">
                  <span class="control-label">Ends </span>
                  <div class="input-group w-md2">
                    <span class="input-group-btn">
                      <div class="input-append pull-left datetimepicker1">
                        <input type="text" name="enddate" data-format ='yyyy-MM-dd' class="form-control dtpicker" data-time-icon='icon-time' value="<?php echo $event->enddate; ?>">
                        <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg glyphicon glyphicon-time"></i>
                        </span>
                        <br/>
                      </div>
                    </span>
                  </div>
                </div>
                {#<div class="col-md-3">
                  <br>
                  <div class="input-append pull-left datetimepicker2">
                    <input type="text" name="endtime" data-format ='HH:mm PP' class="form-control dtpicker" data-time-icon='icon-time' value="<?php echo date('h:i a',strtotime($event->endtime)); ?>">
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg glyphicon glyphicon-time"></i>
                    </span>
                    <br/>
                  </div>
                </div>#}

              </div>


              <br /><br />
              {{ submit_button("save_event", 'class':'btn btn-primary', 'value':'Update Event', 'name':'save_event','id':'savepassBtn' ) }}
              <input type="hidden" name="csrf"value="<?php echo $token ?>"/>


            </div>
          </div>
        </div>
      </div>


    </div><!--end row-->


  </form>
</div><!--end container-->
</div><!--end matter-->
