        <!-- Page heading -->
        <div class="page-head">
        <!-- Page heading -->
          <h2 class="pull-left"> 
          <!-- page meta -->
          <span class="page-meta">Partners</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="/admin"><i class="icon-home"></i> Home</a> 
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="/admin/partners">Partners</a>
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="#" class="bread-current">Create</a>          
        </div>

        <div class="clearfix"></div>

        </div>
        <!-- Page heading ends -->



        <!-- Matter -->

        <div class="matter">
        <div class="container">

          <div class="row">

            <div class="col-md-12">
              {{ content() }}

              <div class="widget wgreen">
                
                <div class="widget-head">
                  <div class="pull-left">Create Partner</div>
                  <div class="widget-icons pull-right">
                    <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                    <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                    <h6>Partner Account</h6>
                    <hr />
                    <!-- Form starts.  -->
                     {{ form('admin/createpartner', 'class': 'form-horizontal') }}
                                <div class="form-group">
                                  <div class="col-lg-2">
                                    <label>{{ form.label('username') }}</label>
                                    <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                    {{ form.render('username') }}
                                    {{ form.messages('username') }}
                                  </div>
                                </div>
                              
                                <div class="form-group">
                                  <div class="col-lg-2">
                                    <label>{{ form.label('email') }}</label>
                                    <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                    {{ form.render('email') }}
                                    {{ form.messages('email') }}
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                    <label>{{ form.label('password') }}</label>
                                    <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                    {{ form.render('password') }}
                                    {{ form.messages('password') }}
                                    <div class="label label-danger" id="passerror"></div>
                                    <div class="label label-danger" id="spaceerror"></div>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                    <label>{{ form.label('repassword') }}</label>
                                    <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                    {{ form.render('repassword') }}
                                    {{ form.messages('repassword') }}
                                    <div class="label label-danger" id="repasserror"></div>
                                  </div>
                                </div>

                    <h6>Partner Profile</h6>
                    <hr />
                                <div class="form-group">
                                  <div class="col-lg-2">
                                    <label>{{ form.label('firstname') }}</label>
                                    <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                    {{ form.render('firstname') }}
                                    {{ form.messages('firstname') }}
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <div class="col-lg-2">
                                    <label>{{ form.label('lastname') }}</label>
                                    <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                    {{ form.render('lastname') }}
                                    {{ form.messages('lastname') }}
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <div class="col-lg-2">
                                    <label>{{ form.label('middlename') }}</label>
                                  </div>
                                  <div class="col-lg-8">
                                    {{ form.render('middlename') }}
                                    {{ form.messages('middlename') }}
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <div class="col-lg-2">
                                    <label>{{ form.label('address') }}</label>
                                  </div>
                                  <div class="col-lg-8">
                                    {{ form.render('address') }}
                                    {{ form.messages('address') }}
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <div class="col-lg-2">
                                    <label>{{ form.label('company') }}</label>
                                  </div>
                                  <div class="col-lg-8">
                                    {{ form.render('company') }}
                                    {{ form.messages('company') }}
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <div class="col-lg-2">
                                    <label>{{ form.label('contact') }}</label>
                                    <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                    {{ form.render('contact') }}
                                    {{ form.messages('contact') }}
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <div class="col-lg-2">
                                    <label>{{ form.label('position') }}</label>
                                  </div>
                                  <div class="col-lg-8">
                                    {{ form.render('position') }}
                                    {{ form.messages('position') }}
                                  </div>
                                </div>  

                                

                    <h6>Partner Profile</h6>
                    <hr />                                   

                                <div class="form-group">
                                <div class="col-lg-2">
                                    <label>{{ form.label('partnerName') }}</label>
                                    <span class="asterisk">*</span>
                                  </div>
                                <div class="col-lg-8">
                                    {{ form.render('partnerName') }}
                                    {{ form.messages('partnerName') }}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-2">
                                    <label>{{ form.label('partnerInfo') }}</label>
                                  </div>
                                <div class="col-lg-8">
                                    {{ form.render('partnerInfo') }}
                                    {{ form.messages('partnerInfo') }}
                                </div>
                            </div>

                            <div class="form-group">
                                  <div class="col-lg-2">
                                    <label>Status</label>
                                    <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                    <div class="radio">
                                      <label>
                                      {{ radio_field('status', 'value':'active', 'checked':'checked') }}
                                        Active
                                      </label>
                                    </div>
                                    <div class="radio">
                                      <label>
                                        {{ radio_field('status', 'value':'deactivate') }}
                                        Deactivate
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                
                                    <hr />
                                  {#{ form.render('csrf', ['value': security.getToken()]) }}
                                  {{ form.messages('csrf') }#}                                    
                                <div class="form-group">
                                  <div class="col-lg-offset-1 col-lg-9">
                                    {{ submit_button('Save Partner' , 'class':'btn btn-primary','id':'savepassBtn') }}
                                    <button type="reset" onclick="javascript:resetTextarea();" class="btn btn-default">Reset</button>
                                  </div>
                                </div>



                              </form>
                  </div>
                </div>
                  <div class="widget-foot">
                    <!-- <a href="{{ url('admin/partners') }}">Back to partner list</a> -->
                  </div>
              </div>  

            </div>

          </div>

        </div>
          </div>

        <!-- Matter ends -->