<div id="partnerGallery" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" style="width: 900px;">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Partners Pictures</h4>
        </div>
        <div class="modal-body">
          <div id="partnerPreloader"></div>
          <input type="hidden" id="partnerID" value="{{ partnerID }}">
          {{ partnerAlbumSelect }}
          <div class="gallery" style="max-height:400px; overflow-y:auto" id="partnerGalleryWrapper">
          {{ partnerGallery }}
          </div>
        </div>
        <div class="modal-footer">
          <button id="afterUpload" type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </div>
  </div>
</div>

<!-- Page heading -->
<div class="page-head">
    <h2 class="pull-left"><i class="icon-suitcase"></i> ABK Partner</h2>

    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="{{ url('admin') }}"><i class="icon-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="{{ url('admin/partners') }}">Partners</a>
        <span class="divider">/</span>
        <a href="{{ url('admin/partnersinfo/') }}{{ partnerID }}">View</a>
        <span class="divider">/</span>
        <a href="{{ url('admin/partnersinfo/') }}{{ partnerID }}/events">Current Events</a>
        <span class="divider">/</span>
        <span>Create New</span>
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">
    {{ form('name':'postform', 'class': 'form-horizontal', 'id':'main-table-form') }}
    <div class="container">

        <!-- Table -->

        <div class="row">

            <div class="col-md-12">
                {{ content() }}

                <div class="widget">

                    <div class="widget-head">
                        <div class="pull-left">{{ partner.partnerName }}</div>
                        <div class="widget-icons pull-right">
                            <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> -->
                            <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">
                            <?php if($userlevel!=2){ ?>
                            {{ tab }}
                            <?php } ?>
                        </div>

                        <div class="padd">
                            <!--code start here-->
                            <h2>Create New Event</h2>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Name</label>
                                <div class="col-lg-8">
                                    {{ evtForm.render('event_name') }}
                                    {{ evtForm.messages('event_name') }}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Date</label>
                                <div class="col-lg-8">
                                    <span class="datePicker">
                                        {{ evtForm.render('event_date') }}
                                        <span class="add-on">
                                            <i class="btn btn-info btn-lg icon-calendar"></i>
                                        </span>
                                    </span>
                                    {{ evtForm.messages('event_date') }}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Venue</label>
                                <div class="col-lg-8">
                                    {{ evtForm.render('event_venue') }}
                                    {{ evtForm.messages('event_venue') }}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label"></label>
                                <div class="col-lg-8">
                                    <a href="#partnerGallery" data-toggle="modal" class="btn btn-default pull-right post-add-media"><i class="icon-paper-clip"></i> Add Media</a>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Details</label>
                                <div class="col-lg-8">
                                    {{ evtForm.render('event_details') }}
                                    {{ evtForm.messages('event_details') }}
                                </div>
                            </div>

                            <hr>
                            <div class="form-group">
                                <div class="col-lg-offset-1 col-lg-9">
                                    {{ link_to("admin/partnersinfo/"~ partnerID ~"/events", "Back to event list", 'class':'btn btn-default') }}
                                    {{ submit_button('Create Event' , 'name':'savePartnerEvent', 'class':'btn btn-primary') }}
                                    {{ evtForm.render('csrf', ['value': security.getToken()]) }}
                                    {{ evtForm.messages('csrf') }}
                                </div>
                            </div>
                            <!--code ends here-->
                        </div>

                        <div class="widget-foot">

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>


            </div>

        </div>


    </div>
    </form>
</div>

<!-- Matter ends -->