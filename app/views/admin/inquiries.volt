
<!-- Modal Prompt-->
<div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Alert!</h4>
			</div>
			<div class="modal-body">
				<p class="modal-message"></p>
				{% if viewlist==false %} <strong>{{ inquiry.inqSubject }}</strong>{% endif %}
				<span class="modal-list-names"></span>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
			</div>
		</div>
	</div>
</div>
<!-- Page heading -->
<div class="page-head">
	<h2 class="pull-left"><i class="icon-table"></i> Contacts</h2>

	<!-- Breadcrumb -->
	<div class="bread-crumb pull-right">
		<a href="{{ url('admin') }}"><i class="icon-home"></i> Home</a> 
		<!-- Divider -->
		<span class="divider">/</span> 
		<a href="" class="bread-current">Contacts</a>
	</div>

	<div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">

	{% if viewlist == true %}

	{{ form('admin/inquiries', 'id':'main-table-form') }}
	<div class="container">
		<!-- <div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<label class="control-label col-lg-3">Search</label>
					<div class="col-lg-9"> 
						{#{ text_field('search_text' , 'class':'form-control') }#}
					</div>
					 
				</div>
			</div>
				<div class="col-lg-4">
					<div class="form-group">
					<label class="control-label col-lg-3">Filter</label>
                      <div class="col-lg-9"> 
                        <select name="searchstat" class="form-control">
                        	<option value="status">-Status-</option>
                        	<option value='0'>New</option>
                        	<option value='1'>Reviewed</option>
                        </select>
                      </div>
                  </div>
                  </div>
			<div class="col-lg-4">
				{#{ submit_button('Search', 'class':'btn btn-default') }#}
				<button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
			</div>
		</div>  -->
		<div class="row">
          <div class="col-md-1">
            <label>Search</label>
          </div>
          <div class="col-md-3 form-group">
            <div class="col-md-12">
              {{ text_field('search_text' , 'class':'form-control') }}
            </div>
          </div>
          <div class="col-md-1">
            <label>Filter</label>
          </div>
          <div class="col-md-3 form-group">
            <div class="col-md-12">
              <select id="form_frame" class="form-control" name="searchstat" onchange="getData(this);"/>
	            	<option value="status" selected="selected">-Status-</option>
	            	<option value='0'>New</option>
	            	<option value='1'>Reviewed</option>
	            </select>
            </div>
          </div>
          <div class="col-md-2 form-group">
            {{ submit_button('Search', 'class':'btn btn-default') }}
            <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
          </div>
        </div>
		 <div class="row">
          <div class="col-md-2">
            <label>Filter by Date</label>
          </div>
          <div class="col-md-3 form-group" id="fromDatepicker" class="input-append">
            <label class="col-md-2 control-label">From</label>
            <div class="col-md-10">
              {{ text_field('fromDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
              <span class="add-on">
                <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
              </span>
            </div>
          </div>
          <div class="col-md-3 form-group" id="toDatepicker" class="input-append">
            <label class="col-md-2 control-label">To</label>
            <div class="col-md-10">
              {{ text_field('toDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
              <span class="add-on">
                <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
              </span>
            </div>
          </div>
          <div class="col-md-2">
            {{ submit_button('Filter', 'name':'filter-date', 'class':'btn btn-default') }}
          </div>
        </div>

		<!-- Table -->
			{#{ content() }#}
			{{ searchResultStat }}
			{{ deleteinq }}

		<div class="row">
			
			<!-- <div class="form-group">
				<div class="col-lg-4">
					<div class="form-group">
						<label class="control-label col-lg-3">Search</label>
						<div class="col-lg-9"> 
							{#{ text_field('search_text' , 'class':'form-control') }#}
						</div>
						 
					</div>
				</div>
					<div class="col-lg-4">
						<div class="form-group">
						<label class="control-label col-lg-3">Filter</label>
                          <div class="col-lg-9"> 
                            <select id="form_frame" name="searchstat" onchange="getData(this);" class="form-control">
                            	<option value="status" selected="selected">-Status-</option>
                            	<option value='0'>New</option>
                            	<option value='1'>Reviewed</option>
                            </select>
                          </div>
                      </div>
                      </div>
				<div class="col-lg-4">
					{#{ submit_button('Search', 'class':'btn btn-default') }#}
					<button type="submit" id="reset" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
				</div>
			</div>  -->

			<div class="col-md-12">
			
			<!--
				KEEP OPTION SELECTED
			<select id="form_frame" name="frame" onchange="getData(this);"/>
			   <option value="data1" selected="selected">Data 1</option>
			   <option value="data2">Data 2</option>
			</select> -->
				<div class="widget">

					<div class="widget-head">
						<div class="pull-left">Inquiries</div>
						<div class="widget-icons pull-right">
							<!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
							<!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
						</div>  
						<div class="clearfix"></div>
					</div>


					<div class="widget-content">
						{{ hidden_field('csrf', 'value': security.getToken())}}
						<input type="hidden" class="tbl-action" name="action" value=""/>
						<input type="hidden" class="tbl-recordID" name="recordID" value=""/>
						<input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
						<table class="table table-striped table-bordered table-hover tblusers">
							<thead>
								<tr>
									<th>{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th>
									<th><a href="?sort={{ statusHref }}">Status <i class="{{ statusIndicator ? statusIndicator : "" }}"></i></a></th>
									<th><a href="?sort={{ subjectHref }}">Subject <i class="{{ subjectIndicator ? subjectIndicator : "" }}"></i></a></th>
									<th><a href="?sort={{ senderHref }}">Sender Name <i class="{{ senderIndicator ? senderIndicator : "" }}"></i></a></th>
									<th><a href="?sort={{ emailHref }}">Email <i class="{{ emailIndicator ? emailIndicator : "" }}"></i></a></th>
									<th><a href="?sort={{ dateHref }}">Date Received <i class="{{ dateIndicator ? dateIndicator : "" }}"></i></a></th>
									<th><a href="?sort={{ replydateHref }}">Date Replied <i class="{{ replydateIndicator ? replydateIndicator : "" }}"></i></a></th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>

								{% if page.total_pages == 0 %}  
								<tr>
									<td colspan="7">No inquiries found</td>
								</tr>
								{% else %}
								{% for post in page.items %}
								<tr>
									<td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="{{ post.inqID}}"> </td>
									<td>
										{% if post.inqStatus == 0 %}
										<span class="label label-success">New</span>
										{% else %}
										<span class="label label-warning">Reviewed</span>
										{% endif %}
									</td>
									<td class="name">
										
										{{ link_to('admin/inquiries/'~post.inqID, post.inqSubject) }}

									</td>
									<td>{{ post.inqSender }}</td>
									<td>{{ post.inqEmail }}</td>
									<td>{{ date("F j, Y h:ia", post.inqDate) }}</td>
									<td>
										{{ post.inqLatestReplyDate ? date("F j, Y h:ia", post.inqLatestReplyDate) : "Not Yet Replied" }}
										</td>
									<td>
										<a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="{{ post.inqID }}"><i class="icon-remove"></i> </a>
									</td>
								</tr>
								{% endfor %}
								{% endif %}
							</tbody>
						</table>

						<div class="tblbottomcontrol" style="display:none">
							<a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
							<a href="#" class="tbl_unselect_all"> Unselect </a>
						</div>

						<div class="widget-foot">

							{% if page.total_pages > 1 %}

							<ul class="pagination pull-right">

								{% if page.current != 1 %}
								<li>{{ link_to("admin/inquiries?page=" ~ page.before, 'Prev') }}</li>
								{% endif %}

								{% for index in 1..page.total_pages %}
								{% if page.current == index %}
								<li>{{ link_to("admin/inquiries?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
								{% else %}
								<li>{{ link_to("admin/inquiries?page=" ~ index, index) }}</li>
								{% endif %}
								{% endfor %}         

								{% if page.current != page.total_pages %}                 
								<li>{{ link_to("admin/inquiries?page=" ~ page.next, 'Next') }}</li>
								{% endif %}
							</ul>
							{% endif %}

							<div class="clearfix"></div> 

						</div>

					</div>

				</div>


			</div>

		</div>


	</div>
</form>
{% else %}
<div class="container">
	<div class="row">
		<div class="col-md-12">
			{{ content() }}
			<div class="widget">
				{{ form('admin/inquiries', 'class': 'form-horizontal', 'id':'main-table-form') }}
				<div class="widget-head">
					<div class="pull-left">Inquiry</div>
					<div class="widget-icons pull-right">
						<!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
						<!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
					</div>  
					<div class="clearfix"></div>
				</div>

				<div class="widget-content">
					<div class="padd">
						{% if(inquiry == false) %}
						<div class="alert alert-danger">Inquiry not found</div>
						{% else %}
						<span class="pull-right recent-meta">{{ date("F j, Y", inquiry.inqDate) }}</span>
						<h3 class="name">{{ inquiry.inqSubject }}</h3>
						<span class="recent-meta">From: {{ inquiry.inqSender }}</span><span class="recent-meta"> < {{ inquiry.inqEmail }} ></span>
						<div class="clearfix"></div>
						<br />
						<p>
							{{ inquiry.inqMessage|nl2br }}
						</p>
						{% endif %}

						<br />
						<hr>
						<br />
						<?php
						$count = count($inquiryReplies);
						foreach ($inquiryReplies as $key => $value) {
							$count--;
							$hr = $count > 0? '<hr />':'';
							echo '
								<div class="text-right">
									<p>'.date('F j, Y', $value->dateReply).'</p>
									<p>Reply from: '.$value->sender.'</p>
									<br />
									<p>'.nl2br($value->message).'</p>
									<div class="clearfix"></div>
								</div>
							'.$hr;
						}
						?>
						
						<br />
						{{ text_area('replyMessage', 'name':'replyMessage', 'class':'form-control', 'rows':'5', 'placeholder':'Send a reply') }}

					</div>
				</div>

				<div class="widget-foot">
					
					{{ hidden_field('csrf', 'value': security.getToken())}}
					<input type="hidden" class="tbl-action" name="action" value=""/>
					<input type="hidden" class="tbl-recordID" name="recordID" value=""/>
					<input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
					<input type="hidden" name="sendToID" value="{{ inquiry.inqID }}"/>
					{{ link_to('admin/inquiries', 'Back to inquiry list', 'class':'btn btn-default') }}
					<!-- <a href="#modalPrompt" class="btn btn-warning tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="{{ inquiry.inqID }}">Delete </a> -->
					{{ submit_button("sendReply", 'class':'btn btn-primary pull-right', 'value':'Send Reply', 'name':'sendReply', 'id':'sendReply' ) }}
				
				</div>  
				</form>

		</div> 
	</div>
</div>  
</div>
{% endif %}

</div>

<!-- Matter ends -->