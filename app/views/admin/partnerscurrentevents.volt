<div id="deleteEventsConf" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Event</h4>
      </div>
      <div class="modal-body">
        <span id="deleteMessage"></span>
      </div>
      <div class="modal-footer" id="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
        <button type="button" id="deleteEventBtn" class="btn btn-danger">Delete</button>
      </div>
    </div>
  </div>
</div>

<!-- Page heading -->
<div class="page-head">
    <h2 class="pull-left"><i class="icon-suitcase"></i> {{ partner.partnerName }}</h2>

    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="{{ url('admin') }}"><i class="icon-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="{{ url('admin/partners') }}">Partners</a>
        <span class="divider">/</span>
        <a href="{{ url('admin/partnersinfo/') }}{{ partnerID }}">View</a>
        <span class="divider">/</span>
        <span>Current Events</span>
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">
    {{ form('class': 'form-horizontal', 'id':'eventsForm') }}
    <div class="container">

        <!-- Table -->

        <div class="row">



            <div class="col-md-12">

                <div class="widget">

                    <div class="widget-head">
                        <div class="pull-left">ABK Partner Details</div>
                        <div class="widget-icons pull-right">
                            <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> -->
                            <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">
                            <?php if($userlevel!=2){ ?>
                            {{ tab }}
                            <?php } ?>
                            <br>
                            {{ content() }}
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label col-lg-3">Search</label>
                                        <div class="col-lg-9"> 
                                            {{ text_field('search_text' , 'class':'form-control') }}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    {{ submit_button('Search', 'name':'searchBtn', 'class':'btn btn-default') }}
                                    <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                                    <a href="{{ url('admin/createpartnerevent/') }}{{ partnerID }}" type="button" class="btn btn-primary pull-right">+ Create New Event</a>
                                </div>
                            </div> 
                            <hr>


                            {% for event in page.items %}
                            {% if loop.first %}
                            <!--head of table-->
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                  <tr>
                                    <th width="10">
                                      <span class="uni">
                                        <input type="checkbox" name="select_all[]" class="tbl_select_all">
                                    </span>
                                </th>
                                <th><a href="?sort={{ dateHref }}">Date <i class="{{ dateIndicator ? dateIndicator : "" }}"></i></a></th>
                                <th><a href="?sort={{ titleHref }}">Event Title <i class="{{ titleIndicator ? titleIndicator : "" }}"></th>
                                <th><a href="?sort={{ venueHref }}">Venue <i class="{{ venueIndicator ? venueIndicator : "" }}"></th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        {% endif %}
                        <!--body of table-->
                        <tbody>
                            <tr>
                                <td>                            
                                  <span class="uni">
                                    <input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="{{ event.eventID }}">
                                  </span>
                                </td>
                                <td width="150">
                                    {{ date("F j, Y", event.eventDate) }}
                                </td>
                                <td>
                                    {{ event.eventTitle }}
                                </td>
                                <td>
                                    {{ event.eventVenue }}
                                </td>
                                <td width="100">
                                  <a href="/admin/editEvent/{{ event.eventID }}?viewonly" class="btn btn-xs btn-success" data-toggle="modal"><i class="icon-ok"></i></a>
                                  <a href="/admin/editEvent/{{ event.eventID }}" class="btn btn-xs btn-warning" data-toggle="modal"><i class="icon-pencil"></i></a>
                                  <a data-event-id="{{ event.eventID }}" data-eventname = "{{ event.eventTitle }}" href="#deleteEventsConf" class="deleteEventLink btn btn-xs btn-danger" data-toggle="modal"><i class="icon-trash"></i></a>
                                </td>
                            </tr>
                        </tbody>
                        {% if loop.last %}
                    </table>
                    {% endif %}
                    
                    {% else %}
                    <div class="alert">No events found</div>
                    {% endfor %}
                    <div class="tblbottomcontrol" style="display: none;">
                        <a href="#deleteEventsConf" id="deleteAllSelectedEventLink" data-toggle="modal"> Delete all Selected </a> |
                        <a href="#" class="tbl_unselect_all"> Unselect </a>
                    </div>
                    <input type="hidden" name="delEventSingle" id="delEventSingle">
                </div>
            </div>


            <div class="widget-foot">
                {% if showBackToList %} {{ link_to("admin/partners", "Back to Partner List", 'class':'btn btn-default') }} {% endif %}

                {% if page.total_pages > 1 %}   
                <ul class="pagination pull-right">
                    {% if page.current != 1 %}
                    <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ page.before, 'Prev') }}</li>
                    {% endif %}

                    {% for index in 1..page.total_pages %}
                    {% if page.current == index %}
                    <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                    {% else %}
                    <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ index, index) }}</li>
                    {% endif %}
                    {% endfor %}         

                    {% if page.current != page.total_pages %}                 
                    <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ page.next, 'Next') }}</li>
                    {% endif %}
                </ul>
                <div class="clearfix"></div> 
                {% endif %}
            </div>

        </div>


    </div>

</div>


</div>
</form>
</div>

<!-- Matter ends -->