<div class="page-head">
    <h2 class="pull-left"><i class="icon-suitcase"></i> ABK Partner</h2>

    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="{{ url('admin') }}"><i class="icon-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="{{ url('admin/partners') }}">Partners</a>
        <span class="divider">/</span>
        <span>View</span>
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">
    {{ form('class': 'form-horizontal', 'id':'main-table-form') }}
    <div class="container">

        <!-- Table -->

        <h2>{{ partner.partnerName }}</h2>
        {{ content() }}

        <div class="row">
            <div class="col-md-12">

                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">ABK Partner Details</div>
                        <div class="widget-icons pull-right">
                            <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> -->
                            <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">
                            <?php if($userlevel!=2){ ?>
                            {{ tab }}
                            <?php } ?>
                            {{ partner.partnerInfo }}
                        </div>
                    </div>

                    <div class="widget-foot">
                        <!-- Footer goes here -->
                        <?php if($userlevel==2){ ?>
                        {{ link_to("admin/partnersinfo/"~partnerID~"/edit", "Edit Details", 'class':'btn btn-warning') }}
                        <?php } ?>
                        {% if showBackToList %} {{ link_to("admin/partners", "Back to Partner List", 'class':'btn btn-default') }} {% endif %}
                    </div>
                </div>

                
            </div>

        </div>


    </div>



    </form>
</div>

<!-- Matter ends