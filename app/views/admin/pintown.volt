          <?php $token = $this->security->getToken() ?>

          <!-- Page heading -->
          <div class="page-head">
            <h2 class="pull-left"><i class="icon-table"></i> Towns</h2>

            <!-- Breadcrumb -->
            <div class="bread-crumb pull-right">
              <a href="index.html"><i class="icon-home"></i> Home</a> 
              <!-- Divider -->
              <span class="divider">/</span> 
              {{ link_to('admin/towns', 'Towns') }}
              <span class="divider">/</span> 
              <span>{{ head }}</span>
            </div>

            <div class="clearfix"></div>

          </div>
          <!-- Page heading ends -->

          <!-- Matter -->

          <div class="matter">
            {{ form('class': 'form-horizontal', 'id':'main-table-form') }}
            <div class="container">

              <!-- Table -->

              <div class="row">

                <div class="col-md-12">
                  {{ content() }}


                  <div class="widget">

                    <div class="widget-head">
                      <div class="pull-left">{{ head }}</div>
                      <div class="widget-icons pull-right">
                      </div>  
                      <div class="clearfix"></div>
                    </div>


                    <div class="widget-content">

                      <!-- <div id="us2" style="width: 100%; height: 500px;"></div>
                      <input type="hidden" id="us2-address" class="form-control" /> -->

                        <div id="mapPanel">
                          <button onclick="addDefaultMarker();" type="button" class="btn btn-primary"><i class="icon-map-marker"></i> Add Marker</button>
                          <span> &nbsp; or click on the map to add marker.</span>
                          <input class="pull-right" onclick="deleteMarkers();" type=button value="Reset Marker" class="btn btn-default">
                        </div>
                        <div style="padding-left:1%;">
                         <em style="color:##d3d3d3; font-size:12px;">*Drag the marker to pin desired town.</em>
                        </div>
                        <div id="map-canvas" onclick="showMarkers();" style="height:500px; width 100%"></div>

                      <div class="padd">

                          <div class="form-group">
                            <div class="col-lg-2">
                              <label>Latitude</label>
                            </div>
                              <div class="col-lg-8">
                                  {% if editMode %}
                                    <?php echo $form->render('townLat', array( 'value' => $town->townLat)); ?>
                                  {% else %}
                                  {{ form.render('townLat') }}
                                  {% endif %}
                                  
                                  {{ form.messages('townLat') }}
                              </div>

                          </div>
                          <div class="form-group">
                            <div class="col-lg-2">
                              <label>Longtitude</label>
                            </div>
                              <div class="col-lg-8">

                                  {% if editMode %}
                                    <?php echo $form->render('townLong', array( 'value' => $town->townLong)); ?>
                                  {% else %}
                                  {{ form.render('townLong') }}
                                  {% endif %}
                                  
                                  {{ form.messages('townLong') }}
                              </div>
                          </div>

                          <hr />
                          
                          <div class="form-group">
                            <div class="col-lg-2">
                              <label>Town Name <apan class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-8">
                                  {% if editMode %}
                                    <input type="hidden" name="origTownName" value="{{ town.townName }}">
                                    <?php echo $form->render('townName', array( 'value' => $town->townName)); ?>
                                  {% else %}
                                  {{ form.render('townName') }}
                                  {% endif %}
                                  {{ form.messages('townName') }}
                              </div>
                          </div>

                          <!-- <div class="form-group">
                              <label class="col-lg-4 control-label"></label>
                              <div class="col-lg-8">
                                  <a href="#partnerGallery" data-toggle="modal" class="btn btn-default pull-right post-add-media"><i class="icon-paper-clip"></i> Add Media</a>
                              </div>
                          </div> -->


                          <div class="form-group">
                             <div class="col-lg-2">
                              <label>Town Information <span class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-8">
                              {{ form.messages('townInfo') }}
                              {% if editMode %}
                                <?php echo $form->render('townInfo', array( 'value' => $town->townInfo)); ?>
                              {% else %}
                              {{ form.render('townInfo') }}
                              {% endif %}
                              </div>
                          </div>

                      </div>
                      <div class="widget-foot">
                        {{ cancel }}
                          {{ submit_button( message , 'name':'saveTown', 'class':'btn btn-primary') }}
                        <!--   <input type="hidden" name="csrf" value="<?php // echo $this->security->getToken() ?>"/> -->
                          <div class="clearfix"></div>
                      </div>
                    </div>

                  </div>


                </div>

              </div>


            </div>
          </form>
        </div>

<!-- Matter ends -->