             <?php 
                  $token = $this->security->getToken();
             ?>

            <div id="delActConf" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="ddModalTitle">Delete Activity</h4>
                  </div>
                  <div class="modal-body">
                    Are you sure you want to <span id="ddActionSpan"></span> activity "<strong id="activityLabelConf"></strong>" ?
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel
                    </button>
                    <button type="button" class="btn btn-danger" id="delActivitybtn">Delete</button>
                  </div>
                </div>
              </div>
            </div>

            <div id="editActConf" class="modal fade modalView" tabindex="-1" role="dialog" aria-hidden="true" >
              <div class="modal-dialog">
                <div class="modal-content">
                  {{ form('admin/programpages/' ~ prog.programID, 'class': 'form-horizontal') }}
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Edit Activity</h4>
                  </div>
                  <div class="modal-body">
                    
                    {{ hidden_field('heditActivity', 'id':'heditActivity') }}
                    <textarea name="activitytextArea" id="activitytextArea" class="form-control"></textarea>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel
                    </button>
                    {{ submit_button("saveEditActivity", 'class':'btn btn-primary', 'value':'Save Changes', 'name':'saveEditActivity', 'id':'saveEditActivity' ) }}
                    
                  </div>
                </div>
                </form>
              </div>
            </div>


            <!-- Modal View-->
            <div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
              <div class="modal-dialog" style="width: 800px;">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">View Content</h4>
                  </div>
                  <div class="modal-body" style=" height: 400px; overflow:scroll;">
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
              {{ form('admin/programpages/' ~ prog.programID, 'class': 'form-horizontal') }}
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                    <input type="hidden" name="page_program_delete_id" class="page_program_delete_id" value="" >
                  </div>
                  <div class="modal-body">
                    <p class="modal-message">Are you sure you want to delete this page? All data will be lost and cannot be recovered.</p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <button type="submit" class="btn btn-primary modal-btn-yes" data-form='userform' name="program_page_delete_yes" value="Yes">Yes</button>
                    <input type="hidden" name="csrf"
        value="<?php echo $token ?>"/>
                  </div>
                </div>
                </form>
              </div>
            </div>
            {{ script }}
            <!-- Page heading -->
            <div class="page-head">
              <!-- Page heading -->
              <h2 class="pull-left"> 
                <!-- page meta -->
                <span class="page-meta">{{ prog.programName }}</span>
              </h2>


              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="/admin"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <a href="" class="bread-current">Program List</a>          
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->



            <!-- Matter -->

            <div class="matter">
              <div class="container">
                {{ content() }}
                <div class="row">
                  <div class="col-md-8">
                    <div class="widget">

                      <div class="widget-head">
                        <div class="pull-left">Pages</div>
                        <div class="widget-icons pull-right">
                        </div>  
                        <div class="clearfix"></div>
                      </div>
                      <div class="widget-content">
                        <div class="padd">

                          <ul id="myTab" class="nav nav-tabs">
                            {% for pa in pages %}
                              {% if updated %}
                            <li class="{{ pa.pageTitle == updated ? "active" : "" }}"><a href="#{{  pa.pageID }}" data-toggle="tab">{{ pa.pageTitle }}</a></li>
                              {% else %} 
                              <li class="{{ loop.first ? "active" : "" }}"><a href="#{{  pa.pageID }}" data-toggle="tab">{{ pa.pageTitle }}</a></li>
                              {% endif %}
                            {% endfor %}
                          </ul>
                          <div id="myTabContent" class="tab-content">
                          <?php
                           echo !count($pages)  ? 'Add pages using the right side "Add Page" button' : '' ?>
                            {% for pa in pages %}
                            {% if updated %}
                            <!--  -->
                             <div class="tab-pane fade in {{ pa.pageTitle == updated ? "active" : "" }}" id="{{  pa.pageID }}">
                            {% else %}
                             <div class="tab-pane fade in {% if loop.first %}active{% endif%} " id="{{  pa.pageID }}">
                            {% endif %}
                           
                              {{ form('admin/programpages/' ~ prog.programID, 'class': 'form-horizontal', 'name': pa.pageID) }}
                              <input type="hidden" name="pageID" value="{{  pa.pageID }}" >

                              <div class="form-group">
                                <div class="col-lg-12">
                                <label>Page Title</label>
                                <span class="asterisk">*</span>
                                {{ titleErrorAct }}  
                              </div>
                                <div class="col-lg-12">
                                  {{ text_field('program_page_title', 'name':'program_page_title', 'value': pa.pageTitle, 'placeholder':'Enter title', 'class':'form-control program_page_title') }}
                                  <label>Slug:</label> <span class="program_page_url">{{ pa.pageSlug }}</span>
                                        {{ hidden_field('program_page_slug', 'id':'program_page_slug','value':pa.pageSlug) }}
                                        {{ hidden_field('program_pageID', 'id':'program_pageID','value':pa.pageID) }}
                                </div>
                              </div>

                              <label>Page Keywords</label>
                              <div class="form-group">
                                <div class="col-lg-12">
                                  {{ text_field('program_page_keyword', 'class':'form-control program_page_title', 'placeholder':'Enter Keywords separated by comma. Ex. disaster, philippine climate change, philippine emergency', 'value':pa.pageKeywords) }}
                                  
                                </div>
                              </div>
                              <label>Content</label>
                              <span class="asterisk">*</span>
                              <textarea id="programPageText{{ pa.pageID }}" name="programPageText" class="programPageText">{{ pa.pageContent}}</textarea>
                              <em>Last Updated: {{ date('F j, Y h:i:s a', pa.pageLastUpdated) }}</em>
                              <br/>
                              <br/>
                              {% if pa.pageActive == 1 %}
                                {{ check_field('program_page_active', 'value': '1', 'checked':'checked') }} <label>Page Active</label>
                              {% else %}
                                {{ check_field('program_page_active', 'value': '1') }} <label>Page Active</label>
                              {% endif %}
                              <br/> <br/>
                              {{ submit_button("program_page_update", 'class':'btn btn-primary', 'value':'Update ' ~ pa.pageTitle, 'name':'program_page_update') }}
                              {{ submit_button("program_page_update", 'class':'btn btn-default', 'value':'Cancel ', 'name':'program_page_cancel' ) }}
                              <a href="#modalView" data-toggle="modal" type="button" class="btn btn-info view-page-sample" data-pageid="{{ pa.pageID }}">Info</a>
                              <a href="#modalPrompt" data-toggle="modal" class="btn btn-danger pull-right delete-program-page" data-pageid="{{ pa.pageID }}">Delete this page</a>
                              <input type="hidden" name="csrf" value="<?php echo $token ?>"/>
                              </form>

                            </div>
                            {% endfor %}
                          </div>
                        </div>
                        <div class="widget-foot"></div>
                      </div>

                    </div>  



                  </div>

                <!-- Task widget -->
                <div class="col-md-4">
                  <div class="widget">
                    <div class="widget-head">
                      <div class="pull-left">Options</div>
                      <div class="widget-icons pull-right">
                      </div>  
                      <div class="clearfix"></div>
                    </div>
                    <div class="widget-content">
                      <div class="padd">

                        {{ form('admin/programpages/' ~ prog.programID, 'class': 'form-horizontal') }}
                        <div class="form-group">
                            <div class="col-lg-12">
                              <label for="content">Page Title</label>
                              <span class="asterisk">*</span>
                              {{ titleError }}  
                            {{ text_field("program_page_title", 'id':'program_page_title','class':'form-control','placeholder':'Enter Title') }} 
                            {{ hidden_field('program_page_slug', 'id':'program_page_slug') }}
                            </div>
                            <div class="col-lg-4">
                              <br>
                              {{ submit_button("program_page_add", 'class':'form-control btn btn-primary', 'value':'Add Page', 'name':'program_page_add') }}
                            </div>
                              <input type="hidden" name="csrf" value="<?php echo $token ?>"/>
                        </div>
                        </form>
                        <hr/>
                        <h6>Update Page Sorting (Use numbers 1,2,3 and so on)</h6>
                        <hr/>

                          {{ form('admin/programpages/' ~ prog.programID, 'class': 'form-horizontal') }}
                            {% for ps in pages %}
                              <div class="form-group">
                                <div class="col-lg-3">{{ numeric_field('sort_field' ~ ps.pageID, 'placeholder':'0', 'class':'form-control', 'value':ps.pageOrder,'min':0) }}
                                </div>
                                <label> {{ ps.pageTitle }} </label>
                              </div>
                            {% endfor %}
                          <?php 
                          if (!count($pages)){
                            echo "No page to sort.";
                          }else{
                          ?>
                          {{ submit_button("updatesort", 'class':'btn btn-primary pull-right', 'name':'updatesort', 'value':'Update Sort' ) }}
                          <?php }?>
                              <input type="hidden" name="csrf" value="<?php echo $token ?>"/>
                          </form>
                          <div class="clearfix"></div>
                      </div>
                      <div class="widget-foot"></div>
                    </div>
                  </div>
                </div>

                                    <!--start program activities-->
                                        <div class="col-md-4">
                    <div class="widget">

                    <div class="widget-head">
                      <div class="pull-left">Program Activities</div>
                      <div class="widget-icons pull-right">
                      </div>  
                      <div class="clearfix"></div>
                    </div>

                      <div class="widget-content">

                        {{ form('admin/programpages/' ~ prog.programID,'id':'activityForm', 'class': 'form-horizontal') }}

                          {{ hidden_field('hdelActivity', 'id':'hdelActivity') }}
                          {{ hidden_field('ddhiddenaction', 'id':'ddhiddenaction') }}

                          
                          <table class="table table-striped table-bordered table-hover">
                            <tbody>
                              <?php if(count($activities) != 0){ ?>
                               {% for act in activities %}
                               <tr>
                                <td>
                                  {% if(act.status == 1) %}<span class="label label-success">Active</span>{% else %}<span class="label label-default">Disabled</span>{% endif %}
                                  {{ act.activity }}

                                  <span class="pull-right">
                                  <a href="#editActConf" data-activity-id ="{{ act.activityID }}" data-activity-title="{{ act.activity }}" data-toggle="modal" class="editActLink btn btn-xs btn-warning"><i class="icon-pencil"></i></a>
                                  {% if(act.status == 1) %}
                                    <a href="#delActConf" data-activity-id ="{{ act.activityID }}" data-activity-title="{{ act.activity }}" data-action="disable" data-toggle="modal" class="disableActConfLink btn btn-xs btn-default"><i class="icon-ban-circle"></i> </a>
                                  {% else %}
                                    <a href="#delActConf" data-activity-id ="{{ act.activityID }}" data-activity-title="{{ act.activity }}" data-action="activate" data-toggle="modal" class="disableActConfLink btn btn-xs btn-success"><i class="icon-ok"></i> </a>
                                  {% endif %}

                                  <!-- <a href="#delActConf" data-activity-id ="{{ act.activityID }}" data-activity-title="{{ act.activity }}" data-action="delete" data-toggle="modal" class="delActConfLink btn btn-xs btn-danger"><i class="icon-remove"></i> </a>
                                  </span> -->
                                </td>
                              </tr>
                               {% endfor %}
                               <?php }else{ ?>
                               <tr>
                                  <td>
                                    No activity found.
                                  </td>
                                </tr>
                               <?php } ?>

                              
                            </tbody>
                          </table>

                          <div class="padd">
                            <?php echo !empty($activityFormResult)?$activityFormResult:'' ?>
                            <div class="control-label">
                            <label for="content">New Activity</label>
                            <span class="asterisk">*</span>
                          </div>
                            {{ text_field('activity', 'name':'activity', 'placeholder':'Enter Title', 'class':'form-control') }}
                          </div>

                          <div class="widget-foot">
                            <input type="submit" name="saveActivity" class="btn btn-primary" value="Save Activity">
                            <input type="hidden" name="actCsrf" value="<?php echo $token ?>"/>
                          </div>
                        </form>
                      </div>
                    </div>
                    </div>
                  <!--end program activities-->
                </div>


                <div class="row">

                <div class="col-lg-8">
                    <div class="widget">
                      <div class="widget-head">
                      <div class="pull-left">Digital Assets (<?php echo count($digital_assets).' files'?>)</div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="widget-content">
                        <div class="padd">
                            <span class="btn btn-success fileinput-button">
                              <i class="glyphicon glyphicon-plus"></i>
                              <span>Add files...</span>
                              <!-- The file input field used as target for the file upload widget -->
                              <input id="digitalAssets" type="file" name="files[]" multiple accept="image/*">
                            </span>
                            <!-- The global progress bar -->
                            <div id="progress" class="progress">
                              <div class="progress-bar progress-bar-success"></div>
                            </div>
                            <div class="gallery digital-assets-gallery">
                              {% for img in digital_assets %}
                                <div class="program-digital-assets-library pull-left" style="position: relative">
                                  <a href="{{ url("img/programs/" ~ prog.programID ~ "/" ~ img) }}" class='prettyPhoto[pp_gal]'>
                                  <img src="{{ url("img/programs/" ~ prog.programID ~ "/" ~ img) }}" alt=""></a>
                                  <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="{{ url("img/programs/" ~ prog.programID ~ "/" ~ img) }}">
                                  <button type="button" class="btn btn-xs btn-danger digital-assets-delete" data-filename="{{ img }}" data-folder="{{ prog.programID }}" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
                                </div>
                              {% endfor %}
                              <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="widget-foot">
                        </div>
                      </div>
                    </div>  
                    </div>       
    

                <!-- Task widget -->
                <div class="col-md-4 pull-right">
                  <div class="widget">
                    <div class="widget-head">
                      <div class="pull-left">Program Banner</div>
                      <div class="widget-icons pull-right">
                      </div>  
                      <div class="clearfix"></div>
                    </div>
                    <div class="widget-content">
                      <div class="padd">
                          {{ form('admin/programpages/' ~ prog.programID, 'class': 'form-horizontal') }}
                              <div class="form-group">
                                <div class="col-lg-12">
                                
                                </div>
                              </div>

                               <div class="form-group">
                                <div class="col-lg-12">
                                <div id="pageBannerImgWrapper">
                                    {% if prog.programBanner %}
                                    <img src="{{prog.programBanner}}" style="width: 100%"/>
                                  {% endif %}
                                </div>
                                {{ text_field('pageBannerUrl', 'class':'form-control', 'placeholder':'Page banner URL here.', 'onclick':'this.focus();this.select()','value':prog.programBanner) }}
                              </div>
                            </div>

                              <div class="pull-right">
                          {{ submit_button("updatebanner", 'class':'btn btn-primary', 'name':'updatebanner', 'value':'Update Banner' ) }} <input type="hidden" name="csrf" value="<?php echo $token ?>"/>
                          &nbsp;
                            <button type="button" class="btn btn-default pull-right" id="removeBanner" style="display:none">Reset Banner</button>
                         </div>
                          <div class="clearfix"> </div>
                      </div>
                      <div class="widget-foot"></div>
                    </div>
                  </div>
                </div>
              </div>

        

              </div>
            </div>  


		<!-- Matter ends -->