            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- Page heading -->
            <div class="page-head">
              <!-- Page heading -->
              <h2 class="pull-left"> 
                <!-- page meta -->
                <span class="page-meta">Programs</span>
              </h2>


              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="/admin"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <a href="/admin/programs/0" class="bread-current">Programs</a>          
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->



            <!-- Matter -->

            <div class="matter">
              <div class="container">
                {{ content()}}
                <div class="row">

                  <div class="col-md-7">              
                    <div class="widget">
                      <div class="widget-head">
                        <div class="pull-left">{% if edit %} Edit {% else %} Create {% endif %} Program</div>
                        <div class="widget-icons pull-right">
                        </div>  
                        <div class="clearfix"></div>
                      </div>
                      <div class="widget-content">
                      <!-- EDIT PROGRAM-->
                      <div class="padd">
                      {% if edit %}
                        
                          <div class="form quick-post">
                            <!-- Edit profile form (not working)-->
                            <form action="{{ url('admin/programs/' ~ eprogs.programID ) }}" method="post" class="form-horizontal">
                            <!-- Title -->
                            <div class="form-group">
                              <label class="control-label col-lg-3" for="title">{{ form.label('title') }}</label>
                              <div class="col-lg-9"> 
                                <?php echo $form->render('title', array( 'value' => $eprogs->programName)); ?>
                                {{ form.messages('title') }} {{ titletaken }}
                              </div>
                            </div>  
                            <!-- Content -->
                            <div class="form-group">
                              <label class="control-label col-lg-3" for="content">{{ form.label('tagline') }}</label>
                              <div class="col-lg-9">
                                <?php echo $form->render('tagline', array( 'value' => $eprogs->programTagline)); ?>
                                {{ form.messages('tagline') }}
                              </div>
                            </div>  
                            <!-- Content -->
                            <div class="form-group">
                              <label class="control-label col-lg-3" for="tooltip">{{ form.label('tooltip') }}</label>
                              <div class="col-lg-9">
                                <?php echo $form->render('tooltip', array( 'value' => $eprogs->programTooltip)); ?>
                                {{ form.messages('tooltip') }}
                              </div>
                            </div>    
                            <!-- Content -->
                            <div class="form-group">
                              <label class="control-label col-lg-3" for="content">{{ form.label('programurl') }}</label>
                              <div class="col-lg-9">
                                <?php echo $form->render('programurl', array( 'value' => $eprogs->programPage)); ?>
                                {{ form.messages('programurl') }}
                              </div>
                            </div>  

                             
                            <div id="imageError"></div>
                            <span class="btn btn-success fileinput-button">
                              <i class="glyphicon glyphicon-plus"></i>
                              <span>Add Flipping Images</span>
                              <!-- The file input field used as target for the file upload widget -->
                              <input id="fileupload" type="file" name="files[]" multiple accept="image/*">
                            </span>
                            {{ pictureerror }}
                            <!-- The global progress bar -->
                            <div id="progress" class="progress">
                              <div class="progress-bar progress-bar-success"></div>
                            </div>
                            <!-- The container for the uploaded files -->
                            <table id="files" class="table table-striped table-bordered table-hover"><tbody>
                              {% for ep in eprogsi %}
                                <tr>
                                  <td>
                              {{ image("img/programs/thumbnail/" ~ ep.imgname, "alt": "alternative text") }}
                                   <br> {{ ep.imgname }} </td>
                                  <td><input type="hidden" name="program_picture[]" value="{{ ep.imgname }}"><a class="btn btn-xs btn-danger delete_program_picture modal-control-button" data-recorid=""><i class="icon-remove"></i> </a> </td>
                                </tr>                                
                              {% endfor %}
                            </tbody></table>

                            {#{ form.render('csrf', ['value': security.getToken()]) }}
                            {{ form.messages('csrf') }#}        
                            <br/>
                            <!-- Buttons -->
                            <div class="form-group">
                             <!-- Buttons -->
                             <div class="col-lg-offset-2 col-lg-9">
                              <a href="/admin/programs/0" class="btn btn-default">Cancel</a>
                              <button type="submit" class="btn btn-success" name="update-program" value="true">Save Changes</button>
                            </div>
                            </div>
                          </form>
                        </div>                      
                      {% else %}
                      <!-- ADD PROGRAM-->
                          <div class="form quick-post">
                            <!-- Edit profile form (not working)-->
                            <form action="{{ url('admin/programs/0' ) }}" method="post" class="form-horizontal">
                            
                            <!-- Title -->
                            <div class="form-group">
                              <div class="col-lg-2">
                              <label class="control-label" for="title">{{ form.label('title') }} <span class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-9"> 
                                {{ form.render('title') }}
                                {{ form.messages('title') }} {{ titletaken }}
                              </div>
                            </div>  
                            <!-- Content -->
                            <div class="form-group">
                              <div class="col-lg-2">
                              <label class="control-label" for="content">{{ form.label('tagline') }} <span class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-9">
                                {{ form.render('tagline') }}
                                {{ form.messages('tagline') }}
                              </div>
                            </div>  
                            <!-- Content -->
                            <div class="form-group">
                              <div class="col-lg-2">
                              <label class="control-label" for="tooltip">{{ form.label('tooltip') }} <span class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-9">
                                {{ form.render('tooltip') }}
                                {{ form.messages('tooltip') }}
                              </div>
                            </div>    
                            <!-- Content -->
                            <div class="form-group">
                              <div class="col-lg-2">
                              <label class="control-label" for="content">{{ form.label('programurl') }} <span class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-9">
                                {{ form.render('programurl') }}
                                {{ form.messages('programurl') }}
                              </div>
                            </div>

                            <div id="imageError"></div>
                            <span class="btn btn-success fileinput-button">
                              <i class="glyphicon glyphicon-plus"></i>
                              <span>Add Flipping Images...</span>
                              <!-- The file input field used as target for the file upload widget -->
                              <input id="fileupload" type="file" name="files[]" multiple accept="image/*">
                            </span>
                            {{ pictureerror }}
                            <!-- The global progress bar -->
                            <div id="progress" class="progress">
                              <div class="progress-bar progress-bar-success"></div>
                            </div>
                            <!-- The container for the uploaded files -->
                            <table id="files" class="table table-striped table-bordered table-hover"><tbody></tbody></table>

                            {{ form.render('csrf', ['value': security.getToken()]) }}
                            {{ form.messages('csrf') }}        <br/>
                            <!-- Buttons -->
                            <div class="form-group">
                             <!-- Buttons -->
                             <div class="col-lg-offset-2 col-lg-9">
                              <button type="submit" class="btn btn-success">Publish</button>
                              <button type="reset" class="btn btn-default">Reset</button>
                            </div>
                            </div>
                          </form>
                        </div>
                      {% endif %}
                      </div>
                    </div>
                    <div class="widget-foot">
                      <!-- Footer goes here -->
                    </div>
                  </div>
                  </div>                  
                  <!-- Task widget -->
                  <div class="col-md-5">
                    <div class="widget">
                      <!-- Widget title -->
                      <div class="widget-head">
                        <div class="pull-left">List of Programs</div>
                        <div class="widget-icons pull-right">
                        </div>  
                        <div class="clearfix"></div>
                      </div>
                      <div class="widget-content referrer">
                        {{ form('admin/programs', 'class': 'form-horizontal', 'id':'main-table-form') }}
                        <!-- Widget content -->
                        <input type="hidden" class="tbl-action" name="action" value=""/>
                        <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                        <input type="hidden" class="tbl-edit-url" name="editurl" value=""/>
                        <table class="table table-striped table-bordered table-hover">
                          <tbody>
                            <?php if($prog == true){ ?>
                            {% for p in prog %}
                            <tr>
                              <td class="name">{{ p.programName }} <br/> URL: {{ p.programPage }}</td>
                              <td>
                                <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="{{ p.programID }}"><i class="icon-pencil"></i> </a>
                                <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="{{ p.programID }}"><i class="icon-remove"></i> </a>
                              </td>
                            </tr> 

                            {% endfor %}      
                            <?php } else {
                                    echo "<tr><td>No Programs Yet.</td></tr>";
                                  } ?>
                          </tbody></table>
                        </form>
                        <div class="widget-foot">
                        </div>
                      </div>
                    </div>
                  </div>



              </div>

            </div>
          </div>


		<!-- Matter ends -->