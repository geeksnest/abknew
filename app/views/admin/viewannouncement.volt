<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">
        <!-- page meta -->
        <span class="page-meta">View Announcement</span>
    </h2>


    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="index.html"><i class="icon-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="#" class="bread-current">Announcements</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <span>View</span>
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->
<!-- Matter -->

<div class="matter">
    <div class="container">
        <form name="postform" method="post" action="">
            <div class="row">

                <div class="col-md-12">
                    {{ content() }}
                    <div class="widget">
                        <div class="widget-head">
                            <div class="pull-left">View Announcement</div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                            <div class="padd">
                                <span class="pull-right muted">{{ date("F j, Y", ann.annDate) }}</span>
                                <div class="form-group">
                                    <label>Enter Title</label>
                                    <div>
                                        {{ ann.annTitle }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Content</label>
                                    <div>
                                        {{ ann.annDesc }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Date Duration:</label>
                                    <div>
                                        {{ date("F j, Y", ann.annStart) }}
                                        <span> to </span>
                                        {{ date("F j, Y", ann.annEnd) }}
                                    </div>
                                </div>

                            </div>
                            <div class="widget-foot">
                                {{ link_to("admin/announcements", "Back to List", "class": "btn btn-default") }}
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Matter ends -->