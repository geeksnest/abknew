/* JS */


/* Navigation */

$(document).ready(function(){

  $(window).resize(function()
  {
    if($(window).width() >= 765){
      $(".sidebar #nav").slideDown(350);
    }
    else{
      $(".sidebar #nav").slideUp(350);
    }
  });


  $("#nav > li > a").on('click',function(e){
    if($(this).parent().hasClass("has_sub")) {
      e.preventDefault();
    }

    if(!$(this).hasClass("subdrop")) {
        // hide any open menus and remove all other classes
        $("#nav li ul").slideUp(350);
        $("#nav li a").removeClass("subdrop");

        // open our new menu and add the open class
        $(this).next("ul").slideDown(350);
        $(this).addClass("subdrop");
      }

      else if($(this).hasClass("subdrop")) {
        $(this).removeClass("subdrop");
        $(this).next("ul").slideUp(350);
      }

    });
});

$(document).ready(function(){
  $(".sidebar-dropdown a").on('click',function(e){
    e.preventDefault();

    if(!$(this).hasClass("open")) {
        // hide any open menus and remove all other classes
        $(".sidebar #nav").slideUp(350);
        $(".sidebar-dropdown a").removeClass("open");

        // open our new menu and add the open class
        $(".sidebar #nav").slideDown(350);
        $(this).addClass("open");
      }

      else if($(this).hasClass("open")) {
        $(this).removeClass("open");
        $(".sidebar #nav").slideUp(350);
      }
    });

});

/* Widget close */

$('.wclose').click(function(e){
  e.preventDefault();
  var $wbox = $(this).parent().parent().parent();
  $wbox.hide(100);
});

/* Widget minimize */

$('.wminimize').click(function(e){
  e.preventDefault();
  var $wcontent = $(this).parent().parent().next('.widget-content');
  if($wcontent.is(':visible'))
  {
    $(this).children('i').removeClass('icon-chevron-up');
    $(this).children('i').addClass('icon-chevron-down');
  }
  else
  {
    $(this).children('i').removeClass('icon-chevron-down');
    $(this).children('i').addClass('icon-chevron-up');
  }
  $wcontent.toggle(500);
});

/* Calendar */

$(document).ready(function() {

  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  $('#calendar').fullCalendar({
    header: {
      left: 'prev',
      center: 'title',
      right: 'month,agendaWeek,agendaDay,next'
    },
    editable: true,
    events: [
    {
      title: 'All Day Event',
      start: new Date(y, m, 1)
    },
    {
      title: 'Long Event',
      start: new Date(y, m, d-5),
      end: new Date(y, m, d-2)
    },
    {
      id: 999,
      title: 'Repeating Event',
      start: new Date(y, m, d-3, 16, 0),
      allDay: false
    },
    {
      id: 999,
      title: 'Repeating Event',
      start: new Date(y, m, d+4, 16, 0),
      allDay: false
    },
    {
      title: 'Meeting',
      start: new Date(y, m, d, 10, 30),
      allDay: false
    },
    {
      title: 'Lunch',
      start: new Date(y, m, d, 12, 0),
      end: new Date(y, m, d, 14, 0),
      allDay: false
    },
    {
      title: 'Birthday Party',
      start: new Date(y, m, d+1, 19, 0),
      end: new Date(y, m, d+1, 22, 30),
      allDay: false
    },
    {
      title: 'Click for Google',
      start: new Date(y, m, 28),
      end: new Date(y, m, 29),
      url: 'http://google.com/'
    }
    ]
  });

});

/* Progressbar animation */

setTimeout(function(){

  $('.progress-animated .progress-bar').each(function() {
    var me = $(this);
    var perc = me.attr("data-percentage");

            //TODO: left and right text handling

            var current_perc = 0;

            var progress = setInterval(function() {
              if (current_perc>=perc) {
                clearInterval(progress);
              } else {
                current_perc +=1;
                me.css('width', (current_perc)+'%');
              }

              me.text((current_perc)+'%');

            }, 600);

          });

},600);

/* Slider */

$(function() {
        // Horizontal slider
        $( "#master1, #master2" ).slider({
          value: 60,
          orientation: "horizontal",
          range: "min",
          animate: true
        });

        $( "#master4, #master3" ).slider({
          value: 80,
          orientation: "horizontal",
          range: "min",
          animate: true
        });

        $("#master5, #master6").slider({
          range: true,
          min: 0,
          max: 400,
          values: [ 75, 200 ],
          slide: function( event, ui ) {
            $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
          }
        });


        // Vertical slider
        $( "#eq > span" ).each(function() {
            // read initial values from markup and remove that
            var value = parseInt( $( this ).text(), 10 );
            $( this ).empty().slider({
              value: value,
              range: "min",
              animate: true,
              orientation: "vertical"
            });
          });
      });



/* Support */

$(document).ready(function(){
  $("#slist a").click(function(e){
   e.preventDefault();
   $(this).next('p').toggle(200);
 });
});

/* Scroll to Top */


$(".totop").hide();

$(function(){
  $(window).scroll(function(){
    if ($(this).scrollTop()>300)
    {
      $('.totop').slideDown();
    }
    else
    {
      $('.totop').slideUp();
    }
  });

  $('.totop a').click(function (e) {
    e.preventDefault();
    $('body,html').animate({scrollTop: 0}, 500);
  });

});

$(document).ready(function() {

  $('.noty-alert').click(function (e) {
    e.preventDefault();
    noty({text: 'Some notifications goes here...',layout:'topRight',type:'alert',timeout:2000});
  });

  $('.noty-success').click(function (e) {
    e.preventDefault();
    noty({text: 'Some notifications goes here...',layout:'top',type:'success',timeout:2000});
  });

  $('.noty-error').click(function (e) {
    e.preventDefault();
    noty({text: 'Some notifications goes here...',layout:'topRight',type:'error',timeout:2000});
  });

  $('.noty-warning').click(function (e) {
    e.preventDefault();
    noty({text: 'Some notifications goes here...',layout:'bottom',type:'warning',timeout:2000});
  });

  $('.noty-information').click(function (e) {
    e.preventDefault();
    noty({text: 'Some notifications goes here...',layout:'topRight',type:'information',timeout:2000});
  });

});


/* Date picker */

$(function() {
  $('#datetimepicker1').datetimepicker({
    pickTime: false
  });
});

$(function() {
  var date = new Date();
  $('.datetimepicker1').datetimepicker({
    pickTime: false,
    startDate: date
  });
});

$(function() {
  $('#datetimepicker2').datetimepicker({
    pickDate: false,
    pick12HourFormat: true,
  });
});

$(function() {
  $('.datetimepicker2').datetimepicker({
    pickDate: false,
    pick12HourFormat: true,
  });
});

/*
* JP - Date picker
* */
$(function () {
  $('.datePicker').datetimepicker({
    pickTime: false
  });

  $('#dateStart').datetimepicker({
    pickTime: false
  });
  $('#dateEnd').datetimepicker({
    pickTime: false
  });
});

var richtextarea = $( '.richText' ).ckeditor();




/* Modal fix */

$('.modal').appendTo($('body'));

/* Pretty Photo for Gallery*/

jQuery("a[class^='prettyPhoto']").prettyPhoto({
  overlay_gallery: false, social_tools: false
});


/* File Uploader */
$(function () {
  'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
    '//jquery-file-upload.appspot.com/' : BASE_URL + 'server/php/';
    $('#fileupload').fileupload({
      url: url,
      dataType: 'json',
      disableImageResize: /Android(?!.*Chrome)|Opera/
      .test(window.navigator.userAgent),
      previewMaxWidth: 100,
      previewMaxHeight: 100,
      previewCrop: true,
      limitMultiFileUploads: 4,
      done: function (e, data) {
        $.each(data.result.files, function (index, file) {
          $('#files tbody').append("<tr><td><img id='theImg' src='"+BASE_URL+"server/php/files/thumbnail/"+file.name+"'/> <br/> "+file.name+" </td><td><input type='hidden' name='program_picture[]' value='" + file.name + "' ><a class='btn btn-xs btn-danger delete_program_picture modal-control-button' data-recorid=''><i class='icon-remove'></i> </a> </td></tr>");
        });
      },
      progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
          'width',
          progress + '%'
          );
      }
    }).prop('disabled', !$.support.fileInput)
    .parent().addClass($.support.fileInput ? undefined : 'disabled');
  });
/* File Uploader */
$(function () {
  'use strict';
  var newArray = new Array();
  $('#digitalAssets').fileupload({
    url: BASE_URL + 'server/php/',
    dataType: 'json',
    disableImageResize: /Android(?!.*Chrome)|Opera/
    .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true,
    limitMultiFileUploads: 4,
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        if(CURRENT_FOLDER_CAT == 'program'){
          console.log();

          $.get( BASE_URL + 'admin/ajaxmoveuploadprogram/'+ file.name+'/'+CURRENT_PROGRAM_FOLDER  ).done(function( data ) {
            $('.digital-assets-gallery').prepend(data);
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            console.log(data);
          });
        }else if(CURRENT_FOLDER_CAT == 'newsletter'){
          $.get( BASE_URL + 'admin/ajaxmoveuploadnewsletter/'+ file.name  ).done(function( data ) {
            $('.digital-assets-gallery').html(data + "<div class='clearfix'></div>");
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            console.log(data);
          });
        }else if(CURRENT_FOLDER_CAT == 'post'){
          $.get( BASE_URL + 'admin/ajaxmoveuploadpost/'+ file.name  ).done(function( data ) {
            $('.digital-assets-gallery').html(data + "<div class='clearfix'></div>");
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            console.log(data);
          });
        }
      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css(
        'width',
        progress + '%'
        );
    }
  }).prop('disabled', !$.support.fileInput)
.parent().addClass($.support.fileInput ? undefined : 'disabled');
});
/* Table Functions*/
$(document).ready(function(){
  $('.tbl_select_all').on('click',function(){
    var all = $(this);
    $('.table input:checkbox').each(function() {
     $(this).prop("checked", all.prop("checked"));
   });
    if($('.tbl_select_all').is(':checked')){
      $('.tblbottomcontrol').slideDown('fast');
      $("html, body").animate({ scrollTop: $(document).height() }, 1000);
    }else{
      $('.tblbottomcontrol').slideUp('fast');
    }
  });
  $('.tbl_select_row').on('click', function(){
    if($('.table input[type=checkbox]:checked').length){
      $('.tblbottomcontrol').slideDown('fast');
    }else{
      $('.tblbottomcontrol').slideUp('fast');
    }
  });
  $('.tbl_unselect_all').on('click', function(e){
    e.preventDefault();
    $('.table input[type=checkbox]').removeAttr('checked');
    $('.tblbottomcontrol').slideUp('fast');
  });
  $('.tbl_delete_all').on('click', function(){
    var tekis="";
    $('.table input:checkbox').each(function() {
     var curr = $(this);
     if(curr.is(':checked')){
       tekis += ' ,' + curr.closest('td').siblings('.name').text();
     }

   });
    tekis = tekis.substr(2);

    $modalnames = tekis;
    $modalaction = $(this).data('action');
    $modaltitle = setModalTitle($modalaction);
    $modalmessage = setModalMessage($modalaction);
    $modalid="tbl_id";
    setModal($modaltitle, $modalaction, $modalid, $modalmessage, $modalnames);
  });
  $('.modal-control-button').on('click', function(){
    tekis = ',' + $(this).closest('td').siblings('.name').text();
    $modalname = tekis.substr(1);
    $modalaction = $(this).data('action');
    $modaltitle = setModalTitle($modalaction);
    $modalmessage = setModalMessage($modalaction);
    $modalid=$(this).data('recorid');
    console.log($modalid);
    setModal($modaltitle, $modalaction, $modalid, $modalmessage, $modalname);
  });
  $('.modal-btn-yes').on('click', function(){
    if($('.tbl-action').val()=='edit'){
      window.location.href = $('.tbl-edit-url').val() + $('.tbl-recordID').val();
    } else if($('.tbl-action').val()=='edit-slide'){
      console.log(window.location.origin + "/admin/settings/" + $('.tbl-recordID').val());
      window.location.href = window.location.origin + "/admin/settings/" + $('.tbl-recordID').val();
    } else {
      $('#main-table-form').submit();
    }
  });
  $('.modal-record-view').on('click', function(){
    href = $(this).data('href');
    $.ajax({
      url: href,
    }).done(function( data ) {
      console.log(data);
      $('.modalView .modal-body').html(data);
    });
  });
});

/* Program JS*/
$(document).ready(function(){
  $(document).on('click', '.delete_program_picture',function(){
    $(this).closest('tr').remove();
  });
  $('#program-title').keyup(function(){
    $('.program-url').val(convertToSlug($(this).val()));
  });
  $('.program_page_title').keyup(function(){
    $(this).siblings('.program_page_url').text(convertToSlug($(this).val()));
  });
  $( '.programPageText' ).ckeditor({
    filebrowserUploadUrl : '/uploader/upload.php',
    filebrowserImageUploadUrl : '/uploader/upload.php?type=Images'
  });
  $('.program_page_title').keyup(function(){
   $(this).siblings('.program_page_url').text(convertToSlug($(this).val()));
   $(this).siblings('#program_page_slug').val(convertToSlug($(this).val()));
 });
  $( '.programPageText' ).ckeditor();
  $('#program_page_title').keyup(function(){
    $(this).siblings('#program_page_slug').val(convertToSlug($(this).val()));
  });
  $(document).on('click', '.digital-assets-delete',function(){
    element = $(this);
    deleteElem(element, 'program');
  });
  $('.delete-program-page').on('click', function(){
    element = $(this);
    $('.page_program_delete_id').val(element.data('pageid'));
  });
  $('.view-page-sample').on('click', function(){
    element = $(this);
    if ( element.is("[data-page-content-id]") ) {
      var programEditorValue = CKEDITOR.instances[element.data('page-content-id')].getData();
    }else {
      var programEditorValue = CKEDITOR.instances['programPageText' + element.data('pageid')].getData();
    }
    
    $('#modalView .modal-body').html('<style>p {padding: 0px 0px 10px 0px !important; margin: 0px;}</style>'+programEditorValue + '<div class="clearfix"></div>');
  });

});
/* News Post*/
$(document).ready(function(){
  $('#post_title').keyup(function(){
    element = $(this);
    $(this).siblings('.post-slug-text').text(convertToSlug($(this).val()));
    $(this).siblings('#post_slug').val(convertToSlug($(this).val()));
  });
  $('.post-add-media').on('click',function(){
    getDigitalAssets();
  });
  $('.post-year-tab').on('click',function(){
    $('#digital-assets-year').val($(this).text());
  });
  $('.post-assets-month').change(function(){
    $('#digital-assets-month').val($(this).val());
    getDigitalAssets();
  });
  $( '.programPageText' ).ckeditor({
    filebrowserUploadUrl : '/uploader/upload.php',
    filebrowserImageUploadUrl : '/uploader/upload.php?type=Images'});
  $(document).on('click', '.digital-assets-post-delete',function(){
    element = $(this);
    deleteElem(element, 'post');
  });
  $( '.newsPostContent' ).ckeditor();
});

function deleteElem(element, type){
  if(type=='program'){
    $.get( BASE_URL + 'admin/ajaxdeleteupload/'+ element.data('filename') +'/'+element.data('folder')  ).done(function( data ) {
      element.closest('.program-digital-assets-library').remove();
    });
  }else if(type='post'){
    $.get( BASE_URL + 'admin/ajaxdeleteupload/'+ element.data('filename') +'/'+element.data('folder')+'/post'  ).done(function( data ) {
      element.closest('.program-digital-assets-library').remove();
    });
  }
}
function getDigitalAssets(){
  $.get( BASE_URL + 'admin/ajaxviewdigitalassets/'+ $('#digital-assets-year').val() +'/'+$('#digital-assets-month').val() ).done(function( data ) {
    $('.digital-assets-monthly').html(data + "<div class='clearfix'></div>");
    jQuery("a[class^='prettyPhoto']").prettyPhoto({
      overlay_gallery: false, social_tools: false
    });
  });

}
function convertToSlug(Text)
{
  return Text
  .toLowerCase()
  .replace(/ /g,'-')
  .replace(/[^\w-]+/g,'')
  ;
}
function setModal(title, action, id, message,names){
  $('#modalPrompt .modal-title').text(title);
  $('#modalPromptcont .modal-title').text(title);
  $('.tbl-action').val(action);
  $('.tbl-recordID').val(id);
  $('#modalPrompt .modal-message').text(message);
  $('#modalPrompt .modal-list-names').text(names);
  $('#modalPromptview .modal-list-names').text(names);
  $('#modalPromptview .modal-list-message').text(message);
}
function setModalMessage(action){
  if(action=="delete"){
    return "Are you sure you want to delete record?";
  }else if(action=="delete_selected"){
    return "Are you sure you want to delete records?";
  }else if(action=="edit"){3
    return "Are you sure you want to edit record?";
  }else if(action=="activate"){
    return "Are you sure you want to re-activate record?";
  }else if(action=="deactivate"){
    return "Are you sure you want to deactivate record?";
  }else if(action=="feature"){
    return "Are you sure you want to feature record?";
  }
}

function setModalTitle(action){
 if(action=="delete"){
  return "Delete Warning!";
}else if(action=="delete_selected"){
  return "Delete Warning!";
}else if(action=="edit"){
  return "Edit Warning Alert!";
}else if(action=="activate"){
  return "Activate Warning Alert!";
}else if(action=="deactivate"){
  return "Deactivate Warning Alert!";
}else if(action=="feature"){
  return "Feature Warning Alert!";
}
}
function datefunc(){
 var m = "AM";
 var gd = new Date();
 var secs = gd.getSeconds();
 var minutes = gd.getMinutes();
 var hours = gd.getHours();
 var day = gd.getDay();
 var month = gd.getMonth();
 var date = gd.getDate();
 var year = gd.getYear();

 if(year<1000){
   year += 1900;
 }
 if(hours==0){
   hours = 12;
 }
 if(hours>12){
   hours = hours - 12;
   m = "PM";
 }
 if(secs<10){
   secs = "0"+secs;
 }
 if(minutes<10){
   minutes = "0"+minutes;
 }

 var dayarray = new Array ("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
 var montharray = new Array ("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

 var fulldate = dayarray[day]+", "+montharray[month]+" "+date+", "+year+" at "+hours+":"+minutes+":"+secs+" "+m;
 $("#currentDate").html(hours+":"+minutes+":"+secs+" "+m);
 $("#currentDateDay").html(dayarray[day]+", "+montharray[month]+" "+date+", "+year);

 setTimeout("datefunc()", 1000);
}
datefunc();



//onclick partner edit caption link
$(".editCaptionLink").click(function(e){
  e.preventDefault();
  $("#modal-footer").hide();
  $("#editCaptionBody").html('<i class="icon-spinner icon-spin"></i> Loading...');
  var pictureID = $(this).data('picture-id');
  $( "#editCaptionBody" ).load("/admin/partnerAjaxEditPicture/"+pictureID, function(){
    $("#modal-footer").show();
  });
});

//when edit caption is submitted
$("#editPictureCaptionForm").submit(function(e){
  var pictureID = $("#pictureID").val();
  $.post("/admin/partnerAjaxEditPicture/"+pictureID, {pictureCaption:$("#pictureCaption").val(), editAlbumSelect:$("#editAlbumSelect").val(), coverSelected:$("#albumCover").val()}, function(data){
    console.log(data);
    //alert("asdasdsdsd");
    if(data.resultText != ''){
      if(data.result){
        $("#caption"+pictureID).text(data.newCaption);
      }
      $('#editResult').html(data.resultText);
      if(data.rename !=''){
        $("#picture"+data.rename).remove();
      }
    }
  }, 'json');

  e.preventDefault(); //STOP default action
});

//delete picture link onclick
$(".deletePictureLink").click(function(e){
  e.preventDefault();
  $("#pictureHiddenID").val($(this).data('picture-id'));
  $("#pictureAlbumHiddenID").val($(this).data('picture-album-id'));
  $("#deletePictureConfMessage").html('Are you sure you want to delete picture?');
});

//delete picture link onclick
$("#deleteAllPictureSelectedLink").click(function(e){
  e.preventDefault();
  $("#pictureHiddenID").val('');
  $("#pictureAlbumHiddenID").val($(this).data('picture-album-id'));
  $("#deletePictureConfMessage").html('Are you sure you want to delete all selected picture(s)?');
  $("#moveALbumID").val('');
});

//delete picture onclick
$("#deletePicture").click(function(){
  $("#picturesForm").submit();
});

$("#moveLink").click(function(e){
  e.preventDefault();
  $("#moveALbumID").val('');
});

$('#movePictureBtn').click(function(){
  $("#picturesForm").submit();
});

$('.tbl_unselect_all').on('click', function(e){
    e.preventDefault();
    $("#moveALbumID").val('');
  });

$("#moveAlbumSelect").change(function(){
  $("#moveALbumID").val($(this).val());
});


//jp upload image
$(function(){
  // Variable to store your files
  var files;

  // Add events
  $("#picturesFile").on('change', prepareUpload);
  $('form#uploadPictureForm').on('submit', uploadPartnerImage);

  // Grab the files and set them to our variable
  function prepareUpload(event){
    $("#progressBar").css('width', 0+'%');
    $("#startUploadBtn").prop('disabled', false);
    files = event.target.files;
  }

  // Catch the form submit and upload the files
  function uploadPartnerImage(event){
      event.stopPropagation(); // Stop stuff happening
      event.preventDefault(); // Totally stop stuff happening

      /*
      if(percentComplete === 100) {
              $("#startUploadBtn").prop('disabled', false);
              $("#partnerPreloader").html('Done');
             }else{
              $( "#startUploadBtn" ).prop( "disabled", true );
              $("#partnerPreloader").html('<i class="icon-spinner icon-spin"></i> Uploading...'+percentComplete);
             }
      */

      // Create a formdata object and add the files
      var data = new FormData();
      $.each(files, function(key, value){
        data.append(key, value);
      });
      
      //console.log(data);
      $.ajax({
        url: '/admin/partnerAjaxUploadPicture/'+$("#partnerID").val()+'/'+$("#uploadAlbumSelect").val(),
        type: 'POST',
        data: data,
        cache: false,
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        beforeSend: function(XMLHttpRequest){
          $("#picturesFile").hide();
          $(".progress").addClass('active');
          $(".progress").addClass('progress-striped');
          $("#partnerPreloader").html('<i class="icon-spinner icon-spin"></i> Uploading...<span id="progressText">0%</span>');
          $( "#startUploadBtn" ).prop( "disabled", true );
        },
        xhr: function(){
         var xhr = new window.XMLHttpRequest();

         xhr.upload.addEventListener("progress", function(evt){
           if (evt.lengthComputable) {
             var percentComplete = evt.loaded / evt.total;
             percentComplete=parseInt(percentComplete*100);
             
             $("#progressBar").html('');
            $("#progressBar").css('width', percentComplete+'%');
            $("#progressText").text(percentComplete+'%');
           }
         }, false);

         return xhr;
       },
       success: function(data){
          console.log(data.result);
          if(data.result != ''){
            $("#partnerPreloader").html(data.result);
          }
        },
       complete: function(data){
        $("#uploadPictureForm")[0].reset();
        $(".progress").removeClass('active');
        $(".progress").removeClass('progress-striped');
        $("#startUploadBtn").prop('disabled', false);
        $("#picturesFile").show();

        $("#afterUpload").click(function(){
          location.reload();
        });
        //$("#partnerPreloader").html('Done');
       },
        error: function(jqXHR, textStatus, errorThrown){
          // Handle errors here
          console.log('ERRORS: ' + textStatus);
          // STOP LOADING SPINNER
        }
      });
}


//jp create album ajax

  $("#deleteSelectedAlbumBtn").click(function(){
    $("#picturesForm").submit();
  });
  

$("button#createAlbumBtn").click(function(){
  $.post("/admin/createAlbumAjax", {partnerID: $("#partnerID").val(), 'albumName':$("#albumName").val()}, function(response){
    if(response.success == true){
      $("#albumResult").hide().html('<span class="label label-success">'+response.message+'</span>').fadeIn();
    }else{
      $("#albumResult").hide().html('<span class="label label-danger">'+response.message+'</span>').fadeIn();
    }

    if(response.albums != ''){
      $(".albums").html(response.albums);
    }

    $("#createAlbumClose").html('<a href="" class="btn btn-default">Close</a>');
    activateAlbumSelect();

    //setTimeout(function(){location.reload();}, 1000);

  }, 'json');
});

  function activateAlbumSelect(){
    $("#albumSelect").change(function () {
          location.href = $(this).val();
    })
  }

  activateAlbumSelect();

  $(".editAlbumNameLink").click(function(e){
    e.preventDefault();
    $("editAlbumContent").html('<span><i class="icon-spinner icon-spin"></i> Loading...</span>');
    $.post("/admin/editAlbumAjax/",{action:'get', albumID:$(this).data('album-id'), partnerID:$(this).data('partner-id')}, function(data){
      $("#editAlbumContent").html(data.message);
      if(data.success){
        $("#editAlbumBtn").show();
      }else{
        $("#editAlbumBtn").hide();
      }
      console.log(data);
    }, 'json');
  });

  $("#editAlbumBtn").click(function(){
    $.post("/admin/editAlbumAjax/", {
      action:'save',albumID:$("#saveHiddenAlbumID").val(),albumName:$("#saveAlbumName").val(), partnerID:$(this).data('partner-id')}, function(data){
        console.log(data);
      $("#editAlbumResult").hide().html(data.message).fadeIn();
      if(data.success){
        $("#albumLink"+$("#saveHiddenAlbumID").val()).html($("#saveAlbumName").val());
      }
      setTimeout(function(){
        //location.reload();
      }, 1000);
    }, 'json');
  });

  $('.deleteAlbumLink').click(function(e){
    e.preventDefault();
    $("#deleteHiddenAlbumID").val($(this).data('album-id'));
    if($(this).data('empty') == 1){
      $("#deleteMessage").html("Are you sure you want to delete <strong>"+$(this).data('album-name')+"</strong>?");
    }else{
      $("#deleteMessage").html("<strong>"+$(this).data('album-name')+" is not empty. </strong>Are you sure you want to delete?");
    }
  });

  $("#deleteAlbumBtn").click(function(e){
    e.preventDefault();
    $("#deleteMessage").hide().html('<span><i class="icon-spinner icon-spin"></i> Deleting...</span>');
    $.post("/admin/editAlbumAjax/", {action:'delete',albumID:$("#deleteHiddenAlbumID").val()}, function(data){
      $("#deleteMessage").hide().html(data.message).fadeIn();
      $("#album"+$("#deleteHiddenAlbumID").val()).remove();
    }, 'json');
  });

  $("#addPictures").click(function(e){
    e.preventDefault();
    $("#uploadAlbumLabel").text($(this).data('album-name'));
    $("#uploadAlbumSelect").val($(this).data('album-id'));
  });

  $(".deletePartner").click(function(e){
    e.preventDefault();
    $("#singlePartnerID").val($(this).data('recorID'));
  });

  $(".deleteEventLink").click(function(e){
    e.preventDefault();
    $("#delEventSingle").val($(this).data('event-id'));
    $("span#deleteMessage").html('Are you sure you want to delete event <strong>"'+$(this).data('eventname')+'"</strong>.');
  });

  $("#deleteEventBtn").click(function(){
    $("#eventsForm").submit();
  });

  $("#deleteAllSelectedEventLink").click(function(e){
    e.preventDefault();
    $("#delEventSingle").val('');
    $("span#deleteMessage").html('Are you sure you want to delete selected event(s)?');
  });

  $("#changePass").click(function(){
    if($(this).prop('checked') == true){
      $("#changePassWrapper").slideDown();
    }else{
      $("#changePassWrapper").slideUp();
    }
  });

});


/* File Uploader */
$(function () {
  'use strict';
  var newArray = new Array();
  $('#digitalAssets2').fileupload({
    url: BASE_URL + 'server/php/',
    dataType: 'json',
    disableImageResize: /Android(?!.*Chrome)|Opera/
    .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true,
    limitMultiFileUploads: 4,
    beforeSend: function(){
      //$('#progress .progress-bar').css('width','0%');
      $("#selectImg").remove();
    },
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {

        var partnerID = $("#partner-id").val();
        var partnerAlbum = $("#partner-album").val();
        var albumID = $("#album-id").val();
        
        $.get(BASE_URL + 'admin/ajaxmoveuploadpartnerpictures/'+ file.name+'/'+albumID, function(data){
            $('.digital-assets-gallery').prepend(data);
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            
            $(".delete-recent-upload-pic").click(function(){
              var delBtn = $(this);
              $.post('/admin/deleteRecentPicUpload/'+$(this).data('picture-id'), function(response){
                console.log(response);
                if(response.success){
                  delBtn.parent().remove();
                }
              }, 'json');
            });
        });

      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css('width',progress + '%');

      /*setTimeout(function(){
        location.reload();
      }, 1000);*/
      //console.log(data);

      $("#closeUpload").click(function(){
        location.reload();
      });

    }
  }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
});


$(".delete-recent-upload-pic").click(function(){
  var delBtn = $(this);
  $.post('/admin/deleteRecentPicUpload/'+$(this).data('picture-id'), function(response){
    console.log(response);
    if(response.success){
      delBtn.parent().remove();
    }
  }, 'json');
});

$(".delete-recent-upload-newsletter").click(function(){
  var delBtn = $(this);
  $.post('/admin/deleteRecentNewsletterUpload/'+$(this).data('picture-id'), function(response){
    console.log(response);
    if(response.success){
      delBtn.parent().remove();
    }
  }, 'json');
});

$("#partnerAlbumSelect").change(function(){
  $("#partnerGalleryWrapper").html('<div class="padd "><i class="icon-spinner icon-spin"></i> Loading...</div>');
  $.get("/admin/ajaxGetAllPictures/"+$(this).val()+"/"+$("#partnerID").val(), function(data){
    $("#partnerGalleryWrapper").html(data);
  });
});

$("#page_title").keyup(function(){
  var slug = convertToSlug($(this).val());
  $("#page-slug").text(slug);
  $("#hpage_slug").val(slug);
});

function convertToSlug(Text){
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-');
}

$("#sideBarImg").blur(function(){
  var img = $(this).val();
  if(img!=''){
    $("#sideBarImgWrapper").html('<img src="'+img+'" style="width: 100%">');
    $("#removeSideBarImg").show();
  }
  else{
    $("#sideBarImgWrapper").html('');
    $("#removeSideBarImg").hide();
  }
});

$("#removeSideBarImg").click(function(){
  $("#sideBarImgWrapper").html('');
  $("#sideBarImg").val('');
  $(this).hide();
});

$('#imageBanner').hide();
if($("#imageUrl").val()){
  $("#imagePreview").html('<img src="'+$("#imageUrl").val()+'" style="width: 100%">');
  $('#imageBanner').show();
} else {
  $("#imagePreview").html('');
  $('#imageBanner').hide();
}
$('#embedVideo').hide();
if($("#videoUrl").val()){
  $("#videoPreview").html('<iframe src="'+$("#videoUrl").val()+'" class="embed-responsive-item" style="width: 100%;" frameborder=0>');
  $('#embedVideo').show();
} else {
  $("#videoPreview").html('');
  $('#embedVideo').hide();
}

$("#imageUrl").blur(function(){
  var img = $(this).val();
  if(img){
    $("#imagePreview").html('<img src="'+img+'" style="width: 100%">');
    $('#imageBanner').show();
  }else{
    $("#imagePreview").html('');
    $('#imageBanner').hide();
  }
});
$("#videoUrl").blur(function(){
  var img = $(this).val();
  if(img){
    $("#videoPreview").html('<iframe src="'+$("#videoUrl").val()+'" class="embed-responsive-item" style="width: 100%;" frameborder=0>');
    $('#embedVideo').show();
  }else{
    $("#videoPreview").html('');
    $('#embedVideo').hide();
  }
});

function removePreview(){
  $("#imagePreview").html('');
  $('#imageBanner').hide();
  $("#videoPreview").html('');
  $('#embedVideo').hide();
}

$("#pageBannerUrl").blur(function(){
  var img = $(this).val();
  if(img!=''){
    $("#pageBannerImgWrapper").html('<img src="'+img+'" style="width: 100%">');
    $("#removeBanner").show();
  }
  else{
    $("#pageBannerImgWrapper").html('');
    $("#removeBanner").hide();
  }
});

$("#removeBanner").click(function(){
  $("#pageBannerImgWrapper").html('');
  $("#pageBannerUrl").val('');
  $(this).hide();
});

/* File Uploader */
$(function () {
  'use strict';
  var newArray = new Array();
  $('#pagespictures').fileupload({
    url: BASE_URL + 'server/php/',
    dataType: 'json',
    disableImageResize: /Android(?!.*Chrome)|Opera/
    .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true,
    limitMultiFileUploads: 1,
    beforeSend: function(){
      $('#progress .progress-bar').css('width','0');
    },
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        console.log("wtf");
        $.get(BASE_URL + 'admin/ajaxmoveuploadpages/'+ file.name +'/0'+'/pages', function(data){
            $('#pages-gallery').prepend(data);
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            var fileCount = parseInt($("#fileCount").text());
            fileCount++;
            $("#fileCount").text(fileCount);
            activateDeletePagePicture();

        });

      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css('width',progress + '%');
    }
  }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
}).on('fileuploadadd', function (e, data) {
    var currentFile = data.files[0];
    console.log(data);
    if(currentFile.size > 2000000){
        data.abort();
        $('#imageError').html("<div class='alert alert-danger'>Image file size must be less than 2 MB.</div>");
        return false;
    } else {
        $('#imageError').html("");      
    }
  });


$(function () {
  'use strict';
  var newArray = new Array();
  $('#actpictures').fileupload({
    url: BASE_URL + 'server/php/',
    dataType: 'json',
    disableImageResize: /Android(?!.*Chrome)|Opera/
    .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true,
    limitMultiFileUploads: 1,
    beforeSend: function(){
      $('#progress .progress-bar').css('width','0');
    },
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $.get(BASE_URL + 'admin/ajaxmoveuploadpages/'+ file.name +'/0'+'/activities', function(data){
            $('#pages-gallery').prepend(data);
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            var fileCount = parseInt($("#fileCount").text());
            fileCount++;
            $("#fileCount").text(fileCount);
            activateDeletePagePicture();

        });

      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css('width',progress + '%');
    }
  }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
}).on('fileuploadadd', function (e, data) {
    var currentFile = data.files[0];
    console.log(data);
    if(currentFile.size > 2000000){
        data.abort();
        $('#imageError').html("<div class='alert alert-danger'>Image file size must be less than 2 MB.</div>");
        return false;
    } else {
        $('#imageError').html("");      
    }
  });

function activateDeletePagePicture(){
  $(".delete-recent-upload-page-pic").click(function(e){
    e.preventDefault();
    var delBtn = $(this);
    $.post('/admin/deleteRecentPagesPicUpload/'+$(this).data('picture-id'), function(response){
      console.log(response);
      if(response.success){
        delBtn.parent().remove();
        var fileCount = parseInt($("#fileCount").text());
        if(fileCount > 0){
          fileCount--;
        }
        $("#fileCount").text(fileCount);
      }
    }, 'json');
  });
}
activateDeletePagePicture();

$(".delActConfLink, .disableActConfLink").click(function(e){
  e.preventDefault();
  $("#ddModalTitle").text($(this).data('action')+' Activity').css('text-transform', 'capitalize');
  $("#ddActionSpan").text($(this).data('action'));
  $("#activityLabelConf").text($(this).data('activity-title'));
  $("#hdelActivity").val($(this).data('activity-id'));
  $("#delActivitybtn").text($(this).data('action')).css('text-transform', 'capitalize');;

  if($(this).data('action')=='disable' || $(this).data('action')=='delete'){
    $("#delActivitybtn").removeClass('btn-success').addClass('btn-danger');
  }else{
    $("#delActivitybtn").removeClass('btn-danger').addClass('btn-success');
  }

  $("#ddhiddenaction").val($(this).data('action'));

  //console.log($(this).data('action'));
});

$(".editActLink").click(function(e){
  e.preventDefault();
  $("#saveEditActivity").prop('disabled', false);
  $("#activitytextArea").val($(this).data('activity-title'));
  $("#heditActivity").val($(this).data('activity-id'));
});

$('#activitytextArea').keyup(function(){
  
  if($(this).val().length > 0){
    $("#saveEditActivity").prop('disabled', false);
  }else{
    $("#saveEditActivity").prop('disabled', true);
  }
});

$("#delActivitybtn").click(function(){
  $("#activityForm").submit();
});

$("a#createTownAlbumLink").click(function(e){
  e.preventDefault();
  $("#albumName").val('');
  $("#editAlbumID").val('');
  $("#townAlbumModalTitle").text('Create Album');
});

$("a.editTownAlbumLink").click(function(e){
  e.preventDefault();
  $("#albumName").val($(this).data('album-name'));
  $("#origAlbumName").val($(this).data('album-name'));
  $("#editAlbumID").val($(this).data('album-id'));
  $("#townAlbumModalTitle").text('Edit Album');
  $("#albumResult").html('');
});

$("#createTownAlbumBtn").click(function(e){
  e.preventDefault();

  $.post(BASE_URL+'admin/townsgallery/'+$("#townID").val(), {'albumName':$("#albumName").val(), 'editAlbumID':$("#editAlbumID").val(), origAlbumName:$("#origAlbumName").val()}, function(response){
    $("#albumResult").hide().html(response.message).show();
    if(response.success){
      location.reload();
    }
  }, 'json');

});


/* File Uploader */
$(function () {
  'use strict';
  var newArray = new Array();
  $('#townPictures').fileupload({
    url: BASE_URL + 'server/php/',
    dataType: 'json',
    disableImageResize: /Android(?!.*Chrome)|Opera/
    .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true,
    limitMultiFileUploads: 4,
    beforeSend: function(){
      //$('#progress .progress-bar').css('width','0%');
      $("#selectImg").remove();
    },
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        var albumID = $("#albumID").val();
        
        $.get(BASE_URL + 'admin/townAlbum/'+$("#townID").val()+'/'+ albumID+'/'+file.name+'/0', function(data){
            $('.digital-assets-gallery').prepend(data);
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            
            $(".delete-recent-upload-townpic").click(function(){
              var delBtn = $(this);
              $.post(BASE_URL + 'admin/townAlbum/'+$("#townID").val()+'/'+ albumID, {'deleteLastTownPic':$(this).data('picture-id')}, function(response){
                console.log(response);
                if(response.success){
                  delBtn.parent().remove();
                }
              }, 'json');
            });
        });

      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css('width',progress + '%');

      /*setTimeout(function(){
        location.reload();
      }, 1000);*/
      //console.log(data);

      $("#closeUpload").click(function(){
        location.reload();
      });

    }
  }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
}).on('fileuploadadd', function (e, data) {
    var currentFile = data.files[0];
    console.log(data);
    if(currentFile.size > 2000000){
        data.abort();
        $('#imageError').html("<div class='alert alert-danger'>Image file size must be less than 2 MB.</div>");
        return false;
    } else {
        $('#imageError').html("");      
    }
  });

$(".editTownPicLink").click(function(e){
  e.preventDefault();
  $("#edit-pic-thumbnail").attr('src', $(this).data('picture-src'));
  $("#pictureCaption").val($(this).data('caption'));
  $("#hTownPic").val($(this).data('picture-id'));
  if($(this).data('picture-id')==$("#albumCoverID").val()){
    $("#albumCover").prop('checked', true);
  }else{
    $("#albumCover").prop('checked', false);
  }
});

$("#albumOptions2").change(function () {
  location.href = $(this).val();
})

$("#townAlbumSelect").change(function(){
  $("#partnerGalleryWrapper").html('<i class="icon-spinner icon-spin"></i>Loading pictures.');
  $.post(BASE_URL + 'admin/townevents/'+$("#townID").val(), {albumID:$(this).val()}, function(response){
    $("#partnerGalleryWrapper").html(response.message);
  }, 'json');
});

$('#uploadTownPicture').on('hidden.bs.modal', function () {
    location.reload();
})

$(".delete-recent-upload-town-pic").click(function(){
  var dis = $(this);
  $.post(BASE_URL + 'admin/townevents/'+$("#townID").val(), {pictureID:$(this).data('picture-id'), albumName:$(this).data('album-name')}, function(response){
    if(response.success == true){
      dis.parent().remove();
    }else{
      alert('Unable to delete file');
    }
  }, 'json');
});

$("#addpartnerlink").click(function(e){
  e.preventDefault();

  //
});

$('#addpartner').on('shown.bs.modal', function () {
  $(".chosen-select").chosen({no_results_text: "No partner found."}); 
});

$('div.modal').on('hidden.bs.modal', function () {
    $(".tbl-action, .tbl-recordID, #hdelActivity, #ddhiddenaction").val('');
})

if($('#contact')[0]){
   $("#contact").mask("(999) 999-9999");
};

function resetTextarea() {
  $('iframe').contents().find('body').empty();
}

/* File Uploader */
$(function () {
  'use strict';
  var newArray = new Array();
  $('#newsletterpictures').fileupload({
    url: BASE_URL + 'server/php/',
    dataType: 'json',
    disableImageResize: /Android(?!.*Chrome)|Opera/
    .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true,
    limitMultiFileUploads: 4,
    beforeSend: function(){
      $('#progress .progress-bar').css('width','0');
    },
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        
        $.get(BASE_URL + 'admin/ajaxmoveuploadnewsletter/'+ file.name +'/0', function(data){
            $('#newsletter-gallery').prepend(data);
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            var fileCount = parseInt($("#fileCount").text());
            fileCount++;
            $("#fileCount").text(fileCount);
            activateDeletePagePicture();

            $(".delete-recent-upload-newsletter").click(function(){
              var delBtn = $(this);
              $.post('/admin/deleteRecentNewsletterUpload/'+$(this).data('picture-id'), function(response){
                console.log(response);
                if(response.success){
                  delBtn.parent().remove();
                }
              }, 'json');
            });
        });

      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css('width',progress + '%');
    }
  }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
}).on('fileuploadadd', function (e, data) {
    var currentFile = data.files[0];
    console.log(data);
    if(currentFile.size > 2000000){
        data.abort();
        $('#imageError').html("<div class='alert alert-danger'>Image file size must be less than 2 MB.</div>");
        return false;
    } else {
        $('#imageError').html("");      
    }
  });

/* File Uploader */
$(function () {
  'use strict';
  var newArray = new Array();
  $('#postpictures').fileupload({
    url: BASE_URL + 'server/php/',
    dataType: 'json',
    disableImageResize: /Android(?!.*Chrome)|Opera/
    .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true,
    limitMultiFileUploads: 4,
    beforeSend: function(){
      $('#progress .progress-bar').css('width','0');
    },
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        
        $.get(BASE_URL + 'admin/ajaxmoveuploadprogram/'+ file.name +'/post', function(data){
            $('#pages-gallery').prepend(data);
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            var fileCount = parseInt($("#fileCount").text());
            fileCount++;
            $("#fileCount").text(fileCount);
            activateDeletePagePicture();

        });

      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css('width',progress + '%');
    }
  }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
}).on('fileuploadadd', function (e, data) {
    var currentFile = data.files[0];
    console.log(data);
    if(currentFile.size > 2000000){
        data.abort();
        $('#imageError').html("<div class='alert alert-danger'>Image file size must be less than 2 MB.</div>");
        return false;
    } else {
        $('#imageError').html("");      
    }
  });


/* CHECK PASSWORD VALIDATION */
if($("#repassword")[0]){
  validatePassword();
}

$("#repassword")
  .change(function(){
    validatePassword();
  })
  .keyup(function(){
    validatePassword();
  })
  .focus(function(){
    validatePassword();
  });

$("#password")
  .change(function(){
    validatePassword();
  })
  .keypress(function(event){
    validatePassword();
  })
  .keyup(function(){
    validatePassword();
  });

$('#password').keypress(function(event){ 
  var keycode = (event.keyCode ? event.keyCode : event.which);
  if(keycode == '32'){
    $('#spaceerror').html("Avoid using space.").fadeIn();
    return false;
  }else{
    $("#spaceerror").fadeOut();
  }
});

function validatePassword()
{
  var pass = $('#password').val(), repass = $('#repassword').val(), error = false;

  console.log(pass);

  if (!pass && !repass){
    $('#passerror').html("");
    $('#repasserror').html("");
    $("#savepassBtn").prop('disabled', true);
  }

  if (!pass || !repass){
    $("#savepassBtn").prop('disabled', true);
  }

  if (pass != repass && pass && repass){
    $('#repasserror').html("Password doesn't match confirmation");
    $("#savepassBtn").prop('disabled', true);
  }
  if(pass.length < 8 && pass) {
    $('#passerror').html("Password should be minimum of 8 characters");
    $("#savepassBtn").prop('disabled', true);
  }
  if(pass.length >= 8) {
    $('#passerror').html("");
  }
  if(pass == repass && pass && repass) {
    $('#repasserror').html("");
  }
  if(pass == repass && pass && repass && pass.length >= 8) {
    $("#savepassBtn").prop('disabled', false);
  }
}
/* END CHECK */


if($('#csrfToken')[0]) {
  $('#csrfToken').val($("#csrf").val());
}

$(function() {
  $('#fromDatepicker').datetimepicker({
    pickTime: false
  });
});

$(function() {
  $('#toDatepicker').datetimepicker({
    pickTime: false
  });
});

$(function() {
  $("#featured").click(function() { 
      if($('#featured').prop('checked')) {
        $('#actorder').attr('disabled', true);
      } else {
        $('#actorder').attr('disabled', false);
      }
  });
  if($('#featured').prop('checked')) {
    $('#actorder').attr('disabled', true);
  } else {
    $('#actorder').attr('disabled', false);
  }
});