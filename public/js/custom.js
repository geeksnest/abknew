/* JS */


/* Recent posts carousel */


$(window).load(function(){
  $('.rps-carousel').carouFredSel({
  	responsive: true,
  	width: '100%',
  	pauseOnHover : true,
  	auto : false,
  	circular	: true,
  	infinite	: false,
  	prev : {
  		button	: "#car_prev",
  		key		: "left"
  	},
  	next : {
  		button	: "#car_next",
  		key		: "right"
  	},
  	swipe: {
  		onMouse: true,
  		onTouch: true
  	},
  	items: {
  		visible: {
  			min: 1,
  			max: 4
  		}
  	}
  }); 
});

/* Parallax Slider */
$(function() {
  if($('#da-slider')[0]){
    $('#da-slider').cslider({
      autoplay  : true,
      interval : 8000
    });
  }
});


/* Flex image slider */


$('.flex-image').flexslider({
  direction: "vertical",
  controlNav: false,
  directionNav: true,
  pauseOnHover: true,
  slideshowSpeed: 10000      
});

/* Refind slider */

$('#rs-slider').refineSlide({
  transition : 'random'
});

/* header slider */


$('.header-flex').flexslider({
  direction: "horizontal",
  animation: "slide",
  controlNav: false,
  directionNav: true,
  pauseOnHover: true,
  slideshowSpeed: 8000      
});
$('.header-images').css( "background-size", "cover" );   

/* Program Tile slider */

$('.tile-flex.dm').flexslider({
  direction: "vertical",
  animation: "slide",
  controlNav: false,
  directionNav: false,
  pauseOnHover: true,
  slideshowSpeed: 15000,
  touch: false 
}); 
$('.tile-flex.pc').flexslider({
  direction: "vertical",
  animation: "slide",
  controlNav: false,
  directionNav: false,
  pauseOnHover: true,
  slideshowSpeed: 20000,
  touch: false
});      
$('.tile-flex.hc').flexslider({
  direction: "vertical",
  animation: "slide",
  controlNav: false,
  directionNav: false,
  pauseOnHover: true,
  slideshowSpeed: 18000,
  touch: false
}); 
$('.tile-flex.el').flexslider({
  direction: "vertical",
  animation: "slide",
  controlNav: false,
  directionNav: false,
  pauseOnHover: true,
  slideshowSpeed: 10000,
  touch: false
}); 
$('.tile-flex.fc').flexslider({
  direction: "vertical",
  animation: "slide",
  controlNav: false,
  directionNav: false,
  pauseOnHover: true,
  slideshowSpeed: 25000,
  touch: false
});       
$('.tile-flex.ep').flexslider({
  direction: "vertical",
  animation: "slide",
  controlNav: false,
  directionNav: false,
  pauseOnHover: true,
  slideshowSpeed: 30000,
  touch: false
});    

/* Hover Divs */
var color;
$(".programTiles").mouseover(function () {
  color = $(this).find( ".tile-title" ).css('background-color');
  $(this).find( ".tile-title" ).css( "background-color", '#000' );
}).mouseout(function () {
  $(this).find( ".tile-title" ).css( "background-color", color );
});
$(".connectTiles").mouseover(function () {
  color = $(this).find( ".connectTitle-Title" ).css('background-color');
  $(this).find( ".connectTitle-Title" ).css( "background-color", '#000' );
}).mouseout(function () {
  $(this).find( ".connectTitle-Title" ).css( "background-color", color );
});    

/* About slider */


$('.about-flex').flexslider({
  direction: "vertical",
  controlNav: true,
  directionNav: false,
  pauseOnHover: true,
  slideshowSpeed: 8000      
});


/* Support */

$("#slist a").click(function(e){
 e.preventDefault();
 $(this).next('p').toggle(200);
});

/* Tooltip */

$('#price-tip1').tooltip();

/* Scroll to Top */

$(document).ready(function(){
  $(".totop").hide();

  $(function(){
    $(window).scroll(function(){
      if ($(this).scrollTop()>600)
      {
        $('.totop').slideDown();
      } 
      else
      {
        $('.totop').slideUp();
      }
    });

    $('.totop a').click(function (e) {
      e.preventDefault();
      $('body,html').animate({scrollTop: 0}, 500);
    });

  });

});


/* Portfolio filter */

/* Isotype */

// cache container
var $container = $('#portfolio');
// initialize isotope
$container.isotope();

// filter items when filter link is clicked
$('#filters a').click(function(){
  var selector = $(this).attr('data-filter');
  $container.isotope({ filter: selector });
  return false;
});

/* Pretty Photo for Gallery*/

jQuery(".prettyphoto").prettyPhoto({
  overlay_gallery: false, social_tools: false
});

jQuery("a[class^='prettyPhoto']").prettyPhoto({
  overlay_gallery: false,
  social_tools: false
});


$(".boxInner").hover(
  function() {
    var titleBox = $(this).find(".titleBox");
    titleBox.animate({
        height: 'toggle'
        }, 100, function() {
    });
  }, function() {
    var titleBox = $(this).find(".titleBox");
    titleBox.animate({
        height: 'toggle'
        }, 100, function() {
    });
  }
);

$("#loginModalForm").submit(function(e){
  e.preventDefault();

  $("#loginModalBtn").prop('disabled', true);

  var postData = $(this).serializeArray();
  postData.push({ loginAjax: 1 });
  var formURL = $(this).attr("action");
  $.ajax(
  {
      url : formURL,
      type: "POST",
      data : postData,
      dataType: 'JSON',
      success:function(data, textStatus, jqXHR) {
          if(data.success){
            console.log(data);
            $("#loginModalBody").hide().html(data.message).fadeIn();
            $("#modal-footer").hide().html('<i class="icon-spinner icon-spin"></i> Redirecting...').fadeIn(function(e){
              setTimeout(function(){ location.reload(); }, 2000);
            });
          }else{
            $("#loginErrorMessage").hide().html('<div class="alert alert-danger">Invalid username or password</div>').fadeIn();
            $("#loginModalBtn").prop('disabled', false);
          }
      },
      error: function(jqXHR, textStatus, errorThrown) 
      {
          console.log(errorThrown);
      }
  });
});

$("#show-signin").click(function(e){
  e.preventDefault();
  $("#signup-wrapper").fadeOut("fast", function(){
    $("#signin-wrapper").fadeIn();
  });
});

$("#show-signup").click(function(e){
  e.preventDefault();
  $("#signin-wrapper").fadeOut("fast", function(){
    $("#signup-wrapper").fadeIn();
  });
});

$(".activityTitleToggle").click(function(e){
  e.preventDefault();

  $(this).parent().next('table').toggle();
  $(this).find('.icon-chevron-down').toggleClass('icon-chevron-up');
  if ($(this).find('i').hasClass('icon-chevron-down') ) {
    $(this).find('i').removeClass('icon-chevron-down').addClass('icon-chevron-up');
  }else{
    $(this).find('i').removeClass('icon-chevron-up').addClass('icon-chevron-down');
  }
});

$("#e-newsletterForm").submit(function(e){
  e.preventDefault();
  var postData = $(this).serializeArray();
  var formURL = $(this).attr("action");
    $.ajax({
        url : formURL,
        type: "POST",
        data : postData,
        dataType: 'JSON',
        success:function(data, textStatus, jqXHR) {
            if(data.message!=''){
              $("#enewsMessage").hide().html(data.message).slideDown();
              if(data.success){
                $("#name").val('');
                $("#email").val('');
              }
            }
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            console.log(errorThrown);
        }
    });
});

$(".paypalAmnt").click(function(e){
  e.preventDefault();
  var amnt = $(this).data('amnt'); 
  $("#donate-amount").val(amnt);
  $("#donate-amount").focus();
});

$("#changepassForm").submit(function(e){
  e.preventDefault();
  $("#submitChangePass").html('<i class="icon-spinner icon-spin"></i> '+$("#submitChangePass").text()).prop('disabled', true);
  var postData = $(this).serializeArray();
  var formURL = $(this).attr("action");
  $.ajax({
    url : formURL,
    type: "POST",
    data : postData,
    dataType: 'JSON',
    success:function(data, textStatus, jqXHR) {
      if(data.success){
        $("#submitChangePass").html('Change').prop('disabled', false);
        $("#changepassResult").hide().html(data.message).fadeIn();
      } else {
        $("#submitChangePass").html('Change').prop('disabled', false);
        $("#changepassResult").hide().html(data.message).fadeIn();
      }
    },
    error: function(jqXHR, textStatus, errorThrown) 
    {
        console.log(errorThrown);
    }
  });
});

$("#forgotpassForm").submit(function(e){
  e.preventDefault();
  $("#submitForgotpass").html('<i class="icon-spinner icon-spin"></i> '+$("#submitForgotpass").text()).prop('disabled', true);
  var postData = $(this).serializeArray();
  var formURL = $(this).attr("action");
  $.ajax({
    url : formURL,
    type: "POST",
    data : postData,
    dataType: 'JSON',
    success:function(data, textStatus, jqXHR) {
      if(data.success){
        $("#submitForgotpass").html('Change').prop('disabled', false);
        $("#forgotpassResult").hide().html(data.message).fadeIn();
      } else {
        $("#submitForgotpass").html('Change').prop('disabled', false);
        $("#forgotpassResult").hide().html(data.message).fadeIn();
      }
    },
    error: function(jqXHR, textStatus, errorThrown) 
    {
        console.log(errorThrown);
    }
  });
});

$("#resetpassForm").submit(function(e){
  e.preventDefault();
  $("#submitResetpass").html('<i class="icon-spinner icon-spin"></i> '+$("#submitResetpass").text()).prop('disabled', true);
  var postData = $(this).serializeArray();
  var formURL = $(this).attr("action");
  $.ajax({
    url : formURL,
    type: "POST",
    data : postData,
    dataType: 'JSON',
    success:function(data, textStatus, jqXHR) {
      if(data.success){
        $("#submitResetpass").html('Change').prop('disabled', false);
        $("#resetpassResult").hide().html(data.message).fadeIn();
      } else {
        $("#submitResetpass").html('Change').prop('disabled', false);
        $("#resetpassResult").hide().html(data.message).fadeIn();
      }
    },
    error: function(jqXHR, textStatus, errorThrown) 
    {
        console.log(errorThrown);
    }
  });
});

$("#editprofileForm").submit(function(e){
  e.preventDefault();
  $("#submitEditProfile").html('<i class="icon-spinner icon-spin"></i> '+$("#submitEditProfile").text()).prop('disabled', true);
  var postData = $(this).serializeArray();
  var formURL = $(this).attr("action");
  $.ajax({
    url : formURL,
    type: "POST",
    data : postData,
    dataType: 'JSON',
    success:function(data, textStatus, jqXHR) {
      console.log("test");
      if(data.success){
        $("#submitEditProfile").html('Update').prop('disabled', false);
        $("#editprofileResult").hide().html(data.message).fadeIn();
      } else {
        $("#submitEditProfile").html('Update').prop('disabled', false);
        $("#editprofileResult").hide().html(data.message).fadeIn();
      }
    },
    error: function(jqXHR, textStatus, errorThrown) 
    {
        console.log(errorThrown);
    }
  });
});

$("#signupForm").submit(function(e){
  e.preventDefault();
  $("#signupNext").html('<i class="icon-spinner icon-spin"></i> '+$("#signupNext").text()).prop('disabled', true);
  var postData = $(this).serializeArray();
  var formURL = $(this).attr("action");
    $.ajax({
        url : formURL,
        type: "POST",
        data : postData,
        dataType: 'JSON',
        success:function(data, textStatus, jqXHR) {
          if(data.success){
            $("#signupForm").hide();
            $("#finalForm").hide().html(data.message).show();
            $('html,body').animate({scrollTop: $("#finalSignupForm").offset().top},'slow');
            $("#signGoBack").click(function(){
              $("#signupForm").show();
              $("#finalForm").html('').hide();
              $("#signupNext").html('Next').prop('disabled', false);
            });

            $(".actSelect").click(function(){
              $("#finalFormResult").html('').hide();
              if($(this).val() == 1){
                $("#suggestAct").hide();
                $("#existingAct").show();
              }else{
                $("#suggestAct").show();
                $("#existingAct").hide();
              }
            });

            $("#finalSignupForm").submit(function(e){
                e.preventDefault();
                $("#signupCreateActBtn").html('<i class="icon-spinner icon-spin"></i> '+$("#signupCreateActBtn").text()).prop('disabled', true);
                $.ajax({
                    url : $(this).attr("action"),
                    type: "POST",
                    data : $(this).serializeArray(),
                    dataType: 'JSON',
                    success:function(data, textStatus, jqXHR) {
                        if(data.success){
                          $("#finalSignupForm").hide().html(data.message).fadeIn();
                          console.log(data.posts);
                        }else{
                          $("#finalFormResult").hide().html(data.message).fadeIn();
                          $("#signupCreateActBtn").find('.icon-spinner').remove();
                          $("#signupCreateActBtn").prop('disabled', false);
                          $('html,body').animate({scrollTop: $("#formResult").offset().top},'slow');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) 
                    {
                        console.log(errorThrown);
                    }
                });
            });

          }else{
            $("#formResult").hide().html(data.message).fadeIn();
            $("#signupNext").find('.icon-spinner').remove();
            $("#signupNext").prop('disabled', false);
            $('html,body').animate({scrollTop: $("#formResult").offset().top},'slow');
          }
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            console.log(errorThrown);
        }
    });
});

$("form#tellafriendModalForm").submit(function(e){
    e.preventDefault();
    $("#tellFriendModalBtn").html('<i class="icon-spinner icon-spin"></i> '+$("#tellFriendModalBtn").text()).prop('disabled', true);
    $.ajax({
        url : '/',
        type: "POST",
        data : $(this).serializeArray(),
        dataType: 'JSON',
        success:function(data, textStatus, jqXHR) {
            if(data.success){
              $("#tellafriendModalForm").hide().html(data.message).fadeIn();
/*              setTimeout(function(){
                $('#tellafriendModal').modal('hide');
              }, 3000);*/
            }else{
              $("#tellFriendErrorMessage").hide().html(data.message).fadeIn();
              $("#tellFriendModalBtn").prop('disabled', false).find('.icon-spinner').remove();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            console.log(errorThrown);
        }
    });
});

if($('.limitChar')[0]){
  $(".limitChar").maxlength();
}

if($('.phone')[0]){
   $(".phone").mask("(999) 999-9999");
};

$(function(){
    $('#scroll-menu').slimScroll({
        height: '250px'
    });
});

