$("#joinForm").on('submit', function(){
  $("div.error-class").remove();
  var $inputs = $('#joinForm :input');
  $inputs.each(function (index)
  {
    if($(this).attr('id') != undefined){
     var id = "#" + $(this).attr('id');
     if($(id).val() == ""){
       switch($(this).attr('id')) {
         case 'lname':
         fieldName = "Lastname";
         break;
         case 'fname':
         fieldName = "Firstname";
         break;
         case 'email':
         fieldName = "Email address";
         break;
         case 'address':
         fieldName = "Address";
         break;
         case 'city':
         fieldName = "City";
         break;
         case 'country':
         fieldName = "Country";
         break;
         case 'state':
         fieldName = "State";
         break;
         case 'zip':
         fieldName = "Zip";
         break;
         case 'phone-number':
         fieldName = "Phone number";
         break;
         default:
          fieldName = $(this).attr('id');
       }
       $(id).parent().append("<div class='error-class'><div class='label label-danger'>"+ fieldName  +" is required</div></div>");
       $(id).addClass("box-error");
     }
    }
  });

  var email = $('#email').val();
  var re = new RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
  var tes = re.test(email);

  if (tes === false && email) {
    $('#email').parent().append("<div class='error-class'><div class='label label-danger'> Email is Invalid</div></div>");
  }

  if($("div.error-class").length > 0){
    $('html,body').animate({scrollTop:$('div.error-class:first').offset().top-300 }, 'slow');
    return false;
  } else {
    return true;
  }
});
